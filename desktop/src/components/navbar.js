var allListNews = document.querySelectorAll("li.item-sub-menu.all-list > .list-news-sub-menu");
var allListNewsOne = document.querySelectorAll("li.item-sub-menu.all-list");

document.addEventListener("mouseover", function( event ) {

    if(event.target.classList.value === "item-sub-menu"
        || event.target.classList.value === "link-sub-menu"
        || event.target.closest(".list-news-sub-menu") !== null) {

        if(event.target.classList.value === "wrap-sub-menu"
            || event.target.classList.value === "sub-menu-ul"
            || event.target.classList.value === "box"
            || event.target.classList.value === "items"
            || event.target.classList.value === "img-thumb"
            || event.target.classList.value === "info"
            || event.target.classList.value === "list-news-sub-menu"
            || event.target.classList.value === "") {
                
            [].forEach.call(allListNewsOne, function(el) {
                el.style.visibility = 'visible';
            });

        } else {

            [].forEach.call(allListNews, function (el) {
                el.style.visibility = 'hidden';
            });

        }

        // console.log("hide");
    } else {

        [].forEach.call(allListNews, function (el) {
            el.style.visibility = 'visible';
        });

    }
    // console.log(event.target.classList)
  
}, false);



/* Open Menu Regional */
document.querySelector('#open-regional-menu').addEventListener("click", (e) => {
    
    var menuRegional = document.querySelector('.nav-menu-regional');
    var arrowDown = document.querySelector('.arrow-down-regional');
    
    if(menuRegional.hasAttribute('style')) {
        menuRegional.removeAttribute('style');
        arrowDown.removeAttribute('style');
    } else {
        menuRegional.style.display = 'flex';
        arrowDown.style.transform = 'rotate(180deg)';
    }

});
/* End Open Menu Regional */

const axios = require('axios');
async function getNews(url) {

    var data = await axios.get(url);

    $('.list-news-sub-menu .wrap-list').append("<span class='loading'>Loading..</span>");

    var newListNews = '';
    
    if(data.status === 200) {
        $('.list-news-sub-menu .wrap-list .loading').remove();

        var listNews = $('.list-news-sub-menu .wrap-list .items');
    
        listNews.remove();
        
        data.data.results.forEach(e => {
    
            var title = e.title.length > 55 ? e.title.substring(0, 55)+"..." : e.title;
    
            newListNews += '<div class="items">'+
                                '<div class="box">'+
                                    '<div class="img-thumb">'+
                                        '<a href="'+e.url+'" aria-label="'+e.title+'">'+
                                            '<img width="70px" height="70px" alt="'+e.title+'"'+
                                            'src="'+e.images[0].url_90x90+'" />'+
                                        '</a>'+
                                    '</div>'+
                                    '<div class="info">'+
                                        '<h3>'+
                                            '<a href="'+e.url+'">'+
                                                title+
                                            '</a>'+
                                        '</h3>'+
                                        '<span>'+convertTime(e.publish_date)+' WIB</span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';
        });
    
        $('.list-news-sub-menu .wrap-list').append(newListNews);
    }

}

var parent = '';
var child = '';
var keyword = '';
$('.nav-menu-top ul li a').mouseover(function(e) {

    var slice = $(this).text().replace(/\s+/g, '-').replace(/ /g,'').replace(/\r?\n|\r/g, '');
    var checkSlice = slice.charAt(0) === "-" || slice.charAt(slice.length) === "-" ? true: false;
    var q_parent = checkSlice ? slice.substring(1, slice.length-1) : slice;

    if($(this).hasClass('link-sub-menu')){
        child = q_parent;
        keyword = parent+","+child;
    } else {
        parent = q_parent;
        keyword = parent;
    }

    // console.log(keyword)

    var url = conf.mode === "dev" ? conf.url : conf.url+keyword+"&limit=6";
    getNews(url)

})

function convertTime(time) {

    var date = new Date(time);
    
    var hours = date.getHours();
    var minutes = date.getMinutes();
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = addZero(hours) + ':' + addZero(minutes);

    return strTime;
}

function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}