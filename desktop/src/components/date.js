var datenow = new Date();

if(document.getElementById('date_now_')) {
    document.getElementById('date_now_').innerHTML = formatDate(datenow);
}

if(document.getElementById('date_time_now_')) {
    document.getElementById('date_time_now_').innerHTML = formatDateTime(datenow);
}

/* Without Time */
function formatDate(date) {
    var monthNames = [
        "Januari", "Februari", "Maret",
        "April", "Mei", "Juni", "Juli",
        "Augustus", "September", "Oktober",
        "November", "Desember"
    ];

    var hari = [
        "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"
    ];

    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = addZero(hours) + ':' + addZero(minutes) + ' ' + ampm;

    var hariIndex = date.getDay();
    var tanggal = addZero(date.getDate());
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return hari[hariIndex]+ ', '+ tanggal + ' ' + monthNames[monthIndex] + ' ' + year;
}

/* With Time */
function formatDateTime(date) {
    var monthNames = [
        "Januari", "Februari", "Maret",
        "April", "Mei", "Juni", "Juli",
        "Augustus", "September", "Oktober",
        "November", "Desember"
    ];

    var hari = [
        "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"
    ];

    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12
    minutes = minutes < 10 ? '0'+minutes : minutes;
    // var strTime = addZero(hours) + ':' + addZero(minutes) + ' ' + ampm;
    var strTime = addZero(hours) + ':' +addZero(minutes);

    var hariIndex = date.getDay();
    var tanggal = addZero(date.getDate());
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return hari[hariIndex]+ ', '+ tanggal + ' ' + monthNames[monthIndex] + ' ' + year + ' | '+strTime;
}

function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}
			