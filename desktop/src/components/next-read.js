
if(document.getElementById("next-article")){
    var next_read = document.getElementById("next-article");
    var element_position = document.getElementById("tag-detail").offsetTop;
    var nav = document.querySelector(".navbar").clientHeight;
    
    window.addEventListener("scroll", (event) => {
        
        var y_scroll_pos = window.pageYOffset;

        if(y_scroll_pos > (element_position + nav + 350)) {
            next_read.style.display = 'block';
        }

    });

    
    document.querySelector('.close-next').addEventListener("click", (e) => {
        next_read.style.display = 'none';
        next_read.remove();
    })
}