// document.addEventListener('DOMContentLoaded', function(){ 
$(document).ready(function() {

    setTimeout(function() {

        var sidebar = 0;
        var height_side = 0;
        var limit, c_Side, nav, foot;
        var body = document.body,
            html = document.documentElement;

        var height = Math.max(body.scrollHeight, body.offsetHeight,
            html.clientHeight, html.scrollHeight, html.offsetHeight);


        if (document.getElementById("sticky-sidebar")) {
            c_Side = document.getElementById("sticky-sidebar");
            footer = document.querySelector('footer').clientHeight;
            height_side = document.getElementById("sticky-sidebar").clientHeight;
            sidebar = document.getElementById("sticky-sidebar").offsetTop;
            nav = document.querySelector(".navbar").clientHeight;
            // limit = document.querySelector('footer').offsetTop;
            limit = height - (footer + height_side + nav);
            var offsetTop = 0;

            var stickyStyle = `
                position: fixed;
                background: #fff;
                padding-top: 10px;
                top: ${nav}px;
            `;
            var unStickyStyle = `
                position: relative;
            `;
            var styleFooter = `
                z-index: 6;
                position: relative;
            `;
            footer.style = `${styleFooter}`;

            window.addEventListener("scroll", (event) => {
                // let scroll = this.scrollY;
                offsetTop = Math.abs(document.body.getBoundingClientRect().top);

                if (offsetTop > sidebar + 50 && offsetTop < limit) {
                    c_Side.style = `${stickyStyle}`;
                } else {
                    c_Side.style = `${unStickyStyle}`;
                }
            });
        }

    }, 1500)

})

// }, false);