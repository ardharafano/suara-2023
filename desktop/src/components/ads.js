/* Close Ads sticky Bottom */
document.querySelector('#close-ads-bottom').addEventListener("click", (e) => {
    var adsBottom = document.querySelector('.wrap-ads-bottom-sticky');

    adsBottom.remove();

});

/* Sticky Ads Side */ 
// document.addEventListener('DOMContentLoaded', function(){ 
$(document).ready(function() {

    setTimeout(function(){

        var body = document.body,
            html = document.documentElement;

        var height = Math.max( body.scrollHeight, body.offsetHeight, 
            html.clientHeight, html.scrollHeight, html.offsetHeight );
            footer = document.querySelector('footer').clientHeight;
            sidebar = document.getElementById("sticky-sidebar").offsetTop;
            nav = document.querySelector(".navbar").clientHeight;
            sticky_ads_l = document.querySelector('.sticky-ads.left');
            sticky_ads_r = document.querySelector('.sticky-ads.right');
            limit_ads = height - (footer + nav + sticky_ads_l.clientHeight);
            offsetTop = 0;

        window.addEventListener("scroll", (event) => {
            // let scroll = this.scrollY;
            offsetTop = Math.abs(document.body.getBoundingClientRect().top);

            if(offsetTop < limit_ads){
                sticky_ads_l.style.display = 'block';
                sticky_ads_r.style.display = 'block';
            } else {
                sticky_ads_l.style.display = 'none';
                sticky_ads_r.style.display = 'none';
            }
        });
        console.log(limit_ads)

    }, 1500)
});
// }, false);