const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const Webpack = require('webpack');

/* WebFont Generator */
// const webfontsGenerator = require('webfonts-generator');
// webfontsGenerator({
//     files: [
//         'src/icons/bullseye.svg',
//         'src/icons/down.svg',
//         'src/icons/fb.svg',
//         'src/icons/google.svg',
//         'src/icons/ig.svg',
//         'src/icons/line.svg',
//         'src/icons/link.svg',
//         'src/icons/mail.svg',
//         'src/icons/play.svg',
//         'src/icons/right.svg',
//         'src/icons/rss.svg',
//         'src/icons/search.svg',
//         'src/icons/telegram.svg',
//         'src/icons/tw.svg',
//         'src/icons/left-arrow.svg',
//         'src/icons/right-arrow.svg',
//         'src/icons/wa.svg',
//         'src/icons/yt.svg',
//     ],
//     dest: 'assets/webfonts/',
//     fontName: 'my-icon',
//     cssFontsUrl: '../webfonts/',
//     templateOptions: {
//         baseClass: 'icon',
//         classPrefix: 'icon-'
//     }
// }, function(error) {
//     if (error) {
//         console.log('Fail!', error);
//     } else {
//         console.log('Done!');
//     }
// });  
/* End WebFont Generator */


/* Css And JS Generate */
module.exports = {
    entry: ['./src/index.js', './assets/webfonts/my-icon.css', './src/sass/main.sass'],
    // entry: {
    //     index: {
    //         import: ['./src/index.js', './assets/webfonts/my-icon.css', './src/sass/main.sass'],
    //     },
    //     static: {
    //         import: './src/bootstrap'
    //     },
    //     headline: {
    //         import: './src/components/headline',
    //     },
    // },
    output: {
        // filename: '[name].build.js',
        filename: 'main.js',
        path: path.resolve(__dirname, 'assets/js/'),
    },
    mode: 'production',
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader, 
                    'css-loader?url=false',
                ],
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader?url=false",
                    'sass-loader'
                ],
            },
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-react', '@babel/preset-env'],
                        plugins: ['@babel/plugin-transform-runtime']
                    }
                }
            },
            {
                test   : /\.(png|jpg|ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
                loader : 'file-loader',
            },
        ]
    },
    plugins: [
        new TerserPlugin(),
        new MiniCssExtractPlugin({
            filename: "../css/main.css",
        }),
        new Webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        }),
    //    new Webpack.DefinePlugin({
    //         'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    //    })
    ],
    optimization: {
        minimize: true,
        minimizer: [
            new CssMinimizerPlugin(),
        ],
    },
}
/* End Css And JS Generate */