<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Suara.com - Berita Hari ini, Berita Terbaru dan Terkini</title>

    <!-- Meta -->
    <meta name="description"
        content="Portal berita yang menyajikan informasi terhangat baik peristiwa politik, entertainment dan lain lain">
    <meta name="keywords"
        content="Berita, Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga">
    <!-- End Meta  -->

    <!-- Favicon -->
    <link rel="Shortcut icon" href="https://assets.suara.com/desktop/images/new-images/favicon.png">
    <link rel="apple-touch-icon" href="https://assets.suara.com/desktop/images/new-images/suara-icon.png">
    <link rel="apple-touch-icon" sizes="72x72"
        href="https://assets.suara.com/desktop/images/new-images/suara-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114"
        href="https://assets.suara.com/desktop/images/new-images/suara-icon-114x114.png">
    <link rel="icon" type="image/png" href="https://assets.suara.com/desktop/images/new-images/suara-icon-192x192.png"
        sizes="192x192">
    <link rel="icon" type="image/png" href="https://assets.suara.com/desktop/images/new-images/suara-icon-512x512.png"
        sizes="512x512">
    <!-- End Favicon -->

    <!-- Font external -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Poppins:400,600,700&amp;font-display=swap" />
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&display=swap">
    <!-- End Font external -->

    <!-- My Style -->
    <link rel="stylesheet" href="assets/css/main.css?<?= time() ?>" />
    <link rel="stylesheet" href="assets/css/splide.min.css?<?= time() ?>">
    <!-- End My Style -->

    <!-- <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
    <script src="https://kit.fontawesome.com/d6e78495c8.js" crossorigin="anonymous"></script> -->

</head>

<body>

    <!-- Navbar -->
    <?php include('include/blocks/navbar.php'); ?>
    <!-- End Navbar -->

    <div class="distance-top"></div>

    <div class="wrap">

        <!-- Ads side sticky -->
        <?php include('include/components/ads/left-ads.php'); ?>
        <?php include('include/components/ads/right-ads.php'); ?>
        <!-- End Ads side sticky -->

        <!-- Nav Menu Bottom  -->
        <?php include('include/components/navbar/nav-menu-bottom.php'); ?>
        <!-- End Nav Menu Bottom  -->

        <!-- Nav Menu Regional  -->
        <?php include('include/components/navbar/nav-menu-regional.php'); ?>
        <!-- End Nav Menu Regional  -->

        <!-- Ads Leaderboard -->
        <?php include('include/components/ads/leaderboard-ads.php'); ?>
        <!-- End Ads Leaderboard -->

        <?php
            if(isset($_GET['page'])){ 
                $url = 'index.php?';
                include("include/pages/".$_GET['page'].".php");
            }else{
                $url = 'index.php?';
                include("include/pages/home.php");
            }
        ?>

    </div>

    <!-- Footer -->
    <?php include('include/blocks/footer.php'); ?>
    <!-- End Footer -->

    <!-- Ads Sticky Bottom -->
    <?php include('include/components/ads/sticky-bottom-ads.php'); ?>
    <!-- End Ads Sticky Bottom -->

    <script>
    // Development conf
    // var conf = {
    //     "url": "news.json",
    //     "mode": "dev",
    // };

    // Production conf
    // var conf = {
    //     "url": "https://suara.com/demo/data/service/headlinecat?category=",
    //     "mode": "prod",
    // };
    </script>

    <!-- Custom Js -->
    <script src="assets/js/main.js?<?= time() ?>"></script>
    <!-- <script src="assets/js/static.build.js"></script>
    <script src="assets/js/index.build.js"></script>
    <script src="assets/js/headline.build.js"></script> -->
    <!-- End Custom Js -->

</body>

</html>