<nav class="navbar">

    <!-- List Other Portal -->
    <?php include('include/components/navbar/nav-list-other-portal.php'); ?>
    <!-- End List Other Portal -->

    <!-- Nav Middle  -->
    <?php include('include/components/navbar/nav-middle.php'); ?>
    <!-- End Nav Middle  -->

    <!-- Nav Menu Top  -->
    <?php include('include/components/navbar/nav-menu-top.php'); ?>
    <!-- End Nav Menu Top  -->
</nav>