<footer>
    <!-- Footer Info -->
    <?php include('include/components/footer/foot-info.php'); ?>
    <!-- End Footer Info -->

    <!-- Footer copyright -->
    <?php include('include/components/footer/foot-copyright.php'); ?>
    <!-- End Footer copyright -->

</footer>