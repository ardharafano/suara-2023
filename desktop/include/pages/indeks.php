
<!-- Wrap -->
<div class="wrap">

    <!-- Base Content  -->
    <div class="base-content">

        <!-- Content -->
        <div class="content static">
            
            <!-- Head content static -->
            <div class="head-content-static">
                <h1>
                    Indek
                </h1>
                <span id="date_time_now_"></span>
            </div>
            <!-- End Head content static -->

            <!-- Search Form Index -->
            <div class="wrap-search-indeks">
                <form action="indeks">
                    <select name="category">
                        <option value="terkini">
                            All
                        </option>
                        <option value="news">
                            News
                        </option>
                        <option value="bisnis">
                            Bisnis
                        </option>
                    </select>
                    <select name="years">
                        <option value="2021">
                            2021
                        </option>
                        <option value="2020">
                            2020
                        </option>
                        <option value="2019">
                            2019
                        </option>
                    </select>
                    <button type="submit">
                        Lihat
                    </button>
                </form>
            </div>
            <!-- End Search Form Index -->

            <!-- List item y img retangle -->
            <?php include('include/components/list-news/list-item-y-for-indeks.php'); ?>
            <!-- End list item y img retangle -->
            
            <?php include('include/components/pagination.php'); ?>


        </div>
        <!-- End Content -->

        <!-- Sidebar -->
        <?php include('include/blocks/sidebar/sidebar-page-statik.php'); ?>
        <!-- End Sidebar -->

    </div>
    <!-- End Base Content  -->

</div>
<!-- Wrap -->
