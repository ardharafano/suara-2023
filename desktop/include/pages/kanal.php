
<!-- Wrap -->
<div class="wrap">

    <!-- Base Content  -->
    <div class="base-content">

        <!-- Content -->
        <div class="content">
            
            <!-- Headline One  -->
            <?php include('include/components/headline/headline-one.php'); ?>
            <!-- End Headline One  -->

            <!-- List Item X Without Date  -->
            <?php include('include/components/list-news/list-item-x-without-date-submenu.php'); ?>
            <!-- List Item X Without Date -->

            <!-- Head Title  -->
        <span class="c-default">
            <a href="index.php?page=kanal" class="c-default c-entertainment">
                <p class="text-head">NASIONAL<span class="border-judul"></span></p>
            </a>
        </span>
            <!-- End Head Title  -->
            <!-- Hedline middle content x -->
            <?php include('include/components/headline/headline-middle-content-x.php'); ?>
            <!-- End Hedline middle content x -->
            
            <!-- Head Title  -->
        <span class="c-default">
            <a href="index.php?page=kanal" class="c-default c-entertainment">
                <p class="text-head">METROPOLITAN<span class="border-judul"></span></p>
            </a>
        </span>
            <!-- End Head Title  -->
            <!-- Hedline middle content y -->
            <?php include('include/components/headline/headline-middle-content-y.php'); ?>
            <!-- End Hedline middle content y -->

            <!-- Head Title  -->
        <span class="c-default">
            <a href="index.php?page=kanal" class="c-default c-entertainment">
                <p class="text-head">INTERNASIONAL<span class="border-judul"></span></p>
            </a>
        </span>
            <!-- End Head Title  -->
            <!-- Hedline middle content x -->
            <?php include('include/components/headline/headline-middle-content-x.php'); ?>
            <!-- End Hedline middle content x -->

            <!-- Head Title  -->
        <span class="c-default">
            <a href="index.php?page=kanal" class="c-default c-entertainment">
                <p class="text-head">VIDEO<span class="border-judul"></span></p>
            </a>
        </span>
            <!-- End Head Title  -->
            <!-- List Video X -->
            <?php include('include/components/list-news/list-video-x.php'); ?>
            <!-- End List Video X -->

            <!-- Head Title  -->
        <span class="c-default">
            <a href="index.php?page=kanal" class="c-default c-entertainment">
                <p class="text-head">TERKINI<span class="border-judul"></span></p>
            </a>
        </span>
            <!-- End Head Title  -->
            <!-- List item y img retangle -->
            <?php include('include/components/list-news/list-item-y-img-retangle.php'); ?>
            <!-- End list item y img retangle -->


        </div>
        <!-- End Content -->

        <!-- Sidebar -->
        <?php include('include/blocks/sidebar/sidebar-home.php'); ?>
        <!-- End Sidebar -->

    </div>
    <!-- End Base Content  -->

</div>
<!-- Wrap -->
