
<!-- Wrap -->
<div class="wrap">

    <!-- Base Content  -->
    <div class="base-content">

        <!-- Content -->
        <div class="content static">
            
            <!-- Head content static -->
            <div class="head-content-static">
                <h1>
                    tentang kami
                </h1>
                <span id="date_time_now_"></span>
            </div>
            <!-- End Head content static -->
    
            <img src="assets/images/tentang-kami.png" alt="tentang suara.com" class="img-responsive" />
    
            <br/>
            <p>
                Suara.com adalah portal berita yang menyajikan informasi terhangat baik peristiwa politik, bisnis, hukum, sepakbola, entertainment, 
                gaya hidup, otomotif, sains teknologi hingga jurnalisme warga. Dikemas dengan bahasa ringan, lugas dan tanpa prasangka. Informasi tersaji 24 jam,
                dapat dinikmati melalui desktop, laptop hingga beragam gadget atau perangkat mobile lainnya.
            </p>
            <p>
                Pertama kali terbit pada 11 Maret 2014, menjelang berlangsung pesta demokrasi pemilihan umum legislatif 
                maupun pemilihan presiden 2014. Meski terbilang anyar, kami yakin mampu berkembang cepat karena dikelola secara profesional 
                dengan melibatkan jurnalis-jurnalis muda yang sudah berpengalaman bekerja di beragam media multipatform, baik online, radio, televisi, 
                maupun cetak.
            </p>

            <p>
                Pemberitaan yang jujur, berimbang dan independen menjadi keniscayaan di tengah berkembangan 
                media partisan karena kepentingan politik maupun bisnis. Jujur adalah menyampaikan fakta apa 
                adanya, tanpa dikurangi atau ditambahi. Berimbang adalah memberikan porsi yang sama bagi pihak yang terkait, 
                tidak berat sebelah dan memberikan asas keadilan. Sedangkan independen maksudnya pengelolaan redaksi 
                bebas dari tekanan atau intervensi manapun.
            </p>

            <p>
                Di era arus informasi yang datang bak air bah, kepercayaan publik atas informasi yang 
                akurat dan dapat dipercaya menjadi acuan. Era digital yang membuat warga berperan 
                memproduksi informasi melalui akun-akun personal di media sosial, menyebabkan perilaku 
                masyarakat dalam mengakses informasi berubah. Tidak lagi mengandalkan media sebagai sumber 
                utama, namun langsung berinteraksi dengan pelaku-pelaku langsung. Pada posisi seperti ini, 
                peran media yang kredibel sangat dibutuhkan, mengingat semakin banyak simpang siur informasi 
                langsung dari masyakat. Media akan berperan mengakurasi informasi, dan melakukan kroscek atau 
                apa yang berkembang di sosial media.
            </p>

            <p>
                Hadir belakangan di tengah belantara media online, Suara.com bertekad memberikan warna 
                baru bagi peta media online di Indonesia. Selain keunggulan berita-berita penting, juga 
                menyajikan berita menarik, unik, dari berbagai belahan dunia. Diharapkan konten beragam 
                ini menjadi pencerah, dan inspirasi bagi pembaca. Harapan kami, Suara.com akan membantu 
                publik untuk mendapatkan informasi secara lengkap, jernih, dan jelas. Seperti motto kami, 
                tanpa Suara beda artinya. 
            </p>



        </div>
        <!-- End Content -->

        <!-- Sidebar -->
        <?php include('include/blocks/sidebar/sidebar-page-statik.php'); ?>
        <!-- End Sidebar -->

    </div>
    <!-- End Base Content  -->

</div>
<!-- Wrap -->
