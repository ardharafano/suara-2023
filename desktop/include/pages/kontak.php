
<!-- Wrap -->
<div class="wrap">

    <!-- Base Content  -->
    <div class="base-content">

        <!-- Content -->
        <div class="content static">
            
            <!-- Head content static -->
            <div class="head-content-static">
                <h1>
                    kontak
                </h1>
                <span id="date_time_now_"></span>
            </div>
            <!-- End Head content static -->
    
            
            <div class="mapouter">
                <div class="gmap_canvas">
                    <iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=arkadia%20digital%20media&t=&z=15&ie=UTF8&iwloc=&output=embed" 
                        frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                    </iframe>
                </div>
            </div>

            <hr/>
            <br/>
            <h2>PT. Arkadia Media Nusantara</h2>
            <ul>
                <li>Jl. Sisingamangaraja No. 21</li>
                <li>Kebayoran Baru, </li>
                <li>Jakarta Selatan 12120, Indonesia</li>
                <li>Tel &nbsp;: 021 - 724 1888 / 021 - 720 8374</li>
                <li>Fax &nbsp;: 021 - 724 1887</li>
                <li>Email &nbsp;: redaksi@suara.com</li>
                <li>Iklan &nbsp;: sales@suara.com </li>
                <li>Info Kirim Tulisan&nbsp;&nbsp;: yoursay@suara.com</li>
            </ul>



        </div>
        <!-- End Content -->

        <!-- Sidebar -->
        <?php include('include/blocks/sidebar/sidebar-page-statik.php'); ?>
        <!-- End Sidebar -->

    </div>
    <!-- End Base Content  -->

</div>
<!-- Wrap -->
