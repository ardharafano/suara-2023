<!-- Wrap -->
<div class="wrap">

    <!-- Base Content  -->
    <div class="base-content">

        <!-- Content -->
        <div class="content">

            <!-- Headline One  -->
            <?php include('include/components/headline/headline-one.php'); ?>
            <!-- End Headline One  -->

            <!-- List Item X Without Date  -->
            <?php include('include/components/list-news/list-item-x-without-date.php'); ?>
            <!-- List Item X Without Date -->

            <!-- Head Title  -->
            <!-- <img class="lingkaran" src="assets/images/icons/lingkaran.svg"> -->
            <!-- <div class="garis-hor"></div> -->
            <!-- <div class="text-head">
                <span class="c-default">
                    <a href="index.php?page=kanal" class="c-default c-entertainment">
                    News                    
                    </a>
                    <span class="border-judul"></span>
                    <span class="garis-hor"></span>
                </span>
            </div> -->

            <span class="c-default">
                <a href="index.php?page=kanal" class="c-default c-entertainment">
                    <p class="text-head">NEWS<span class="border-judul"></span></p>
                </a>
            </span>

            <!-- <div class="text-head">
                <span class="c-default">
                    <a href="index.php?page=kanal" class="c-default c-entertainment">
                    <div class="border-judul2">News</div>                    
                    </a>
                </span>
            </div> -->
            <!-- <hr class="home-hr"> -->
            <!-- End Head Title  -->
            <!-- Hedline middle content y -->
            <?php include('include/components/headline/headline-middle-content-y.php'); ?>
            <!-- End Hedline middle content y -->

            <span class="c-default">
                <a href="index.php?page=kanal" class="c-default c-entertainment">
                    <p class="text-head">TERKINI<span class="border-judul"></span></p>
                </a>
            </span>
            <!-- <hr class="home-hr"> -->

            <!-- End Head Title  -->
            <!-- List Video X -->
            <?php include('include/components/list-news/list-item-y-img-retangle.php'); ?>

            <!-- Head Title  -->
            <span class="c-default">
                <a href="index.php?page=kanal" class="c-default c-entertainment">
                    <p class="text-head">LIFESTYLES<span class="border-judul"></span></p>
                </a>
            </span>
            <!-- <hr class="home-hr"> -->

            <!-- End Head Title  -->
            <!-- Hedline middle content y -->
            <?php include('include/components/headline/headline-middle-content-y.php'); ?>
            <!-- End List Video X -->

            <!-- Head Title  -->
            <!-- <div class="head-title">
                <span class="c-default">
                    <a href="index.php?page=kanal" class="c-default c-regional">
                        Terkini
                    </a>
                </span>
            </div> -->
            <!-- End Head Title  -->
            <!-- List item y img retangle -->
            <?php include('include/components/list-news/list-item-y-img-retangle.php'); ?>
            <!-- End list item y img retangle -->

        </div>
        <!-- End Content -->

        <!-- Sidebar -->
        <?php include('include/blocks/sidebar/sidebar-home.php'); ?>
        <!-- End Sidebar -->

    </div>
    <!-- End Base Content  -->

</div>
<!-- Wrap -->