<!-- Wrap -->
<div class="wrap">

    <!-- Base Content  -->
    <div class="base-content">

        <!-- Content -->
        <div class="content">

            <!-- Top  -->
            <div class="top-detail">
                <ul>
                    <li>
                        <a href="index.php?page=kanal">
                            News
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=kanal" class="active">
                            Nasional
                        </a>
                    </li>
                </ul>
                <!-- <span id="date-article">Senin, 25 Januari 2021 | 11:51</span> -->
            </div>
            <!-- End Top  -->

            <!-- Info  -->
            <div class="info">
                <h1>
                    Telak! P2G Kritik Menteri Nadiem, Soroti Kasus Jilbab Siswi karena Viral
                </h1>
                <h2>
                    Kemunculan Rommy Sulastyo sebagai ayah Aldebaran di sinetron Ikatan Cinta mampu menarik perhatian
                    publik.
                </h2>
                <div class="writer">
                    <span>Agung Sandy Lesmana </span>
                    <span>Ria Rizki Nirmala Sari </span>
                </div>

                <div class="date-article">
                    <span>Senin, 25 Januari 2021 | 11:51 WIB</span>
                </div>
            </div>
            <!-- End Info  -->

            <div class="share-baru-header">
                <a href="#">
                    <img src="assets/images/share/fb.svg" alt="img">
                </a>

                <a href="#">
                    <img src="assets/images/share/twitter.svg" alt="img">
                </a>

                <a href="#">
                    <img src="assets/images/share/line.svg" alt="img">
                </a>

                <a href="#">
                    <img src="assets/images/share/tele.svg" alt="img">
                </a>

                <a href="#">
                    <img src="assets/images/share/wa.svg" alt="img">
                </a>

                <a href="#">
                    <img src="assets/images/share/link.svg" alt="img">
                </a>
            </div>

            <!-- Image Cover  -->
            <div class="img-cover">
                <!-- <img src="assets/images/examples/headline.jpg" class="img-responsive" alt="cover berita" /> -->
                <!-- <picture>
                    <source srcset="https://media.suara.com/pictures/970x544/2021/02/15/83678-ilustrasi-cara-membuat-masker-kain-suaracommichelle-illona.webp" type="image/webp">
                    <source srcset="https://media.suara.com/pictures/970x544/2021/02/15/83678-ilustrasi-cara-membuat-masker-kain-suaracommichelle-illona.jpg" type="image/jpeg">
                    <img src="https://media.suara.com/pictures/970x544/2021/02/15/83678-ilustrasi-cara-membuat-masker-kain-suaracommichelle-illona.jpg" width="653" height="366" alt="title image" class="">
                </picture> -->

                <!-- Slide Photo -->
                <!-- <div class="wrap-slide">

                    <a href="index.php?page=detail-foto" aria-label="Preview">
                        <i class="icon icon-left-arrow"></i>
                    </a>

                    <div class="slide-foto-thumb">
                        <div class="item-foto active">
                            <a href="index.php?page=detail-foto" aria-label="detail slide foto">
                                <img src="assets/images/examples/video-side.jpg" width="300px" height="250px" alt="ads" />
                            </a>
                        </div>
                        <div class="item-foto">
                            <a href="index.php?page=detail-foto" aria-label="detail slide foto">
                                <img src="assets/images/examples/video-side.jpg" width="300px" height="250px" alt="ads" />
                            </a>
                        </div>
                        <div class="item-foto">
                            <a href="index.php?page=detail-foto" aria-label="detail slide foto">
                                <img src="assets/images/examples/video-side.jpg" width="300px" height="250px" alt="ads" />
                            </a>
                        </div>
                        <div class="item-foto">
                            <a href="index.php?page=detail-foto" aria-label="detail slide foto">
                                <img src="assets/images/examples/video-side.jpg" width="300px" height="250px" alt="ads" />
                            </a>
                        </div>
                    </div>

                    <a href="index.php?page=detail-foto" aria-label="Next">
                        <i class="icon icon-right-arrow"></i>
                    </a>
                    
                </div> -->
                <!-- End Slide Photo -->

                <!-- slide new foto -->
                <!-- SLIDE JS -->
                <div id="detail-foto">
                    <div id="main-slider" class="splide">
                        <div class="splide__track">
                            <ul class="splide__list">
                                <li class="splide__slide">
                                    <img src="assets/images/examples/headline-big.jpg" alt="img" width="653"
                                        height="366" />
                                </li>
                                <li class="splide__slide">
                                    <img src="assets/images/examples/detail.png" alt="img" width="653" height="366" />
                                </li>
                                <li class="splide__slide">
                                    <img src="assets/images/examples/headline-big.jpg" alt="img" width="653"
                                        height="366" />
                                </li>
                                <li class="splide__slide">
                                    <img src="assets/images/examples/detail.png" alt="img" width="653" height="366" />
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div id="thumbnails" class="splide">
                        <div class="splide__track">
                            <ul class="splide__list">
                                <li class="splide__slide">
                                    <img src="assets/images/examples/headline-big.jpg" alt="img" width="160"
                                        height="92" />
                                </li>
                                <li class="splide__slide">
                                    <img src="assets/images/examples/detail.png" alt="img" width="160" height="92" />
                                </li>
                                <li class="splide__slide">
                                    <img src="assets/images/examples/headline-big.jpg" alt="img" width="160"
                                        height="92" />
                                </li>
                                <li class="splide__slide">
                                    <img src="assets/images/examples/detail.png" alt="img" width="160" height="92" />
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>

                <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3/dist/js/splide.min.js"></script>
                <script>
                document.addEventListener('DOMContentLoaded', function() {
                    var main = new Splide('#main-slider', {
                        type: 'fade',
                        rewind: true,
                        pagination: false,
                        arrows: false,
                    });


                    var thumbnails = new Splide('#thumbnails', {
                        fixedWidth: 160,
                        fixedHeight: 92,
                        gap: 5,
                        rewind: true,
                        pagination: false,
                        cover: true,
                        isNavigation: true,
                    });


                    main.sync(thumbnails);
                    main.mount();
                    thumbnails.mount();
                });
                </script>

                <!-- END SLIDE JS -->

                <div class="caption">
                    Nadiem Makarim (Instagram/Kemdikbud.RI)
                </div>

            </div>
            <!-- End Image Cover  -->


            <!-- Detail content -->
            <div class="detail-content">
                <p>
                    <strong>Suara.com -</strong> Perhimpunan Pendidikan dan Guru (P2G) mengapresiasi atas reaksi cepat
                    Menteri Pendidikan dan Kebudayaan (Mendikbud) Nadiem Makarim terkait aturan di SMK Negeri 2 Padang
                    yang mewajibkan siswi non muslim memakai jilbab. Namun di sisi lain, P2G juga menyayangkan kalau
                    Nadiem hanya merespons kasus yang kebetulan tengah menjadi obrolan hangat di tengah masyarakat.
                </p>
                <p>
                    Kabid Advokasi P2G, Iman Zanatul Haeri menilai kalau Nadiem tidak mengakui secara terbuka kalau
                    fenomena intoleransi seperti yang terjadi di SMK Negeri 2 Padang itu juga dialami banyak siswa di
                    daerah lain.
                </p>
                <h2>
                    Kabid Advokasi P2G
                </h2>
                <p>
                    "Kasus <a href="#">intoleransi</a> di sekolah yang dilakukan secara terstruktur bukanlah kasus
                    baru," kata Iman dalam keterangan tertulisnya
                </p>
                <iframe width="560" height="315" title="iframe title" src="https://www.youtube.com/embed/5iuvNQRWTH0"
                    frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
                <p>
                    Menurut Iman, Nadiem seharusnya mengungkap persoalan intoleransi di lingkungan sekolah.
                    Persoalan intoleransi di sekolah itu dikatakannya mengandung problematika dari aspek regulasi
                    struktural, sistematik dan birokratis.
                </p>
                <div class="wrap-ads-r">
                    <a href="#" aria-label="ads">
                        <img src="assets/images/examples/ads/ads-squere-side.png" width="300px" height="250px"
                            alt="ads" />
                    </a>
                </div>
                <ul>
                    <li>Jangan mengonsumsi makanan yang sudah tercemar air banjir</li>
                    <li>Jangan mengonsumsi makanan dalam kemasan plastik, karton, atau kemasan lain yang sudah rusak
                        karena basah</li>
                    <li>Lindungi makanan yang masih tersegel dengan baik. Bisa ditambah perlindungan dengan memasang
                        perekat yang lebih kuat agar tertutup rapat</li>
                    <li>Makanan kaleng yang tidak rusak bisa dikonsumsi. Kaleng bekas yang bersih juga bisa digunakan
                        lagi, tapi perlu dicuci dan dilabeli dengan keterangan tanggal kedaluwarsanya</li>
                </ul>
                <p class="baca-juga-new">
                    <span>Baca Juga:</span>
                    <a href="#">Infeksi Virus Corona Biasa Bisa Bentuk Antibodi Covid-19, Mitos atau Fakta</a>
                </p>
                <h3>
                    Kabid Advokasi P2G
                </h3>
                <p>
                    Di samping itu, P2G melihat adanya Peraturan Daerah (Perda) yang menjadi penyebab utama dari
                    lahirnya intoleransi di sekolah. Sebab, peristiwa pemaksaan jilbab di SMKN 2
                    Padang merujuk pada Instruksi Walikota Padang No 451.442/BINSOS-iii/2005.
                </p>
                <figure class="image">
                    <img src="https://media.suara.com/pictures/653x366/2021/02/18/46786-pep-guardiola-manchester-city.jpg"
                        alt="Simpang Susun Pemalang di Jalan Tol Brebes - Pemalang, Jawa Tengah. [Dok Djoko Setijowarno]"
                        width="653" height="366" class="js_detail_img">
                    <figcaption>Setya Novanto [suara.com/Kurniawan Mas'ud]</figcaption>
                </figure>
                <h4>
                    Kabid Advokasi P2G
                </h4>
                <p>
                    "Artinya ada peran pemerintah pusat, seperti Kemendagri dan Kemendikbud yang
                    mendiamkan dan melakukan pembiaran terhadap adanya regulasi daerah bermuatan intoleransi di sekolah
                    selama ini," tuturnya.
                </p>
            </div>
            <!-- End Detail content -->

            <!-- Detail Pagination -->
            <div class="wrap-pagination">
                <ul class="pagination-custom">
                    <li><a href="#">«</a></li>
                    <li><a href="#">1</a></li>
                    <li class="active"><span>2</span></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">»</a></li>
                    <li><a href="#">Tampilkan Semua</a></li>
                </ul>
            </div>
            <!-- End Detail Pagination -->

            <!-- Head Title  -->
            <div class="tag-header">
                <div class="text-tag" id="tag-detail">
                    <span class="text-tag">
                        <a href="index.php?page=kanal" class="text-tag">
                            Tag
                        </a>
                    </span>
                </div>

                <!-- End Head Title  -->
                <!-- List Tag -->
                <?php include('include/components/list-news/list-tag.php'); ?>
            </div>
            <!-- End List Tag -->

            <!-- Share Button  -->
            <div class="share-link">
                <ul>
                    <li>
                        <span>
                            Share link:
                        </span>
                    </li>
                </ul>

                <div class="share-baru-bottom">
                    <a href="#">
                        <img src="assets/images/share/fb.svg" alt="img">
                    </a>

                    <a href="#">
                        <img src="assets/images/share/twitter.svg" alt="img">
                    </a>

                    <a href="#">
                        <img src="assets/images/share/line.svg" alt="img">
                    </a>

                    <a href="#">
                        <img src="assets/images/share/tele.svg" alt="img">
                    </a>

                    <a href="#">
                        <img src="assets/images/share/wa.svg" alt="img">
                    </a>

                    <a href="#">
                        <img src="assets/images/share/link.svg" alt="img">
                    </a>
                </div>

            </div>
            <!-- End Share Button  -->

            <!-- Head Title  -->
            <!-- <div class="text-head">
                <span class="c-default">
                    <a href="index.php?page=kanal" class="c-default c-regional">
                    News
                    </a>
                </span>
            </div> -->
            <span class="c-default">
                <a href="index.php?page=kanal" class="c-default c-entertainment">
                    <p class="text-head">NEWS<span class="border-judul"></span></p>
                </a>
            </span>
            <!-- End Head Title  -->
            <!-- Hedline Regional middle content y -->
            <div class="headline-middle-content-y">
                <div class="headline-x mb-30">
                    <div class="img-thumb">
                        <a href="index.php?page=detail" aria-label="News suara.com">
                            <img src="assets/images/examples/headline-small.jpg" alt="headline content news"
                                width="330px" height="186px" />
                        </a>
                    </div>
                    <div class="description">
                        <h3>
                            <a href="index.php?page=detail">
                                Punya Badan Ramping, Adele Dikabarkan Habiskan Uang Hampir Rp70 Miliar!
                            </a>
                        </h3>
                        <span>12:27 WIB</span>
                    </div>
                </div>
            </div>
            <?php include('include/components/list-news/list-item-x2.php'); ?>
            <!-- End Hedline Regional middle content y  -->

            <!-- Head Title  -->
            <!-- <div class="text-head">
                <span class="c-default">
                    <a href="index.php?page=kanal" class="c-default c-regional">
                        Terkini
                    </a>
                </span>
            </div> -->
            <span class="c-default">
                <a href="index.php?page=kanal" class="c-default c-entertainment">
                    <p class="text-head">TERKINI<span class="border-judul"></span></p>
                </a>
            </span>
            <!-- End Head Title  -->

            <!-- List item y img retangle -->
            <?php include('include/components/list-news/list-item-y-img-retangle.php'); ?>

            <a href="index.php?page=kanal" class="btn bg-darkslateblue-2">
                Tampilkan lebih banyak
            </a>
            <!-- End list item y img retangle -->

            <!-- List item y img retangle -->
            <?php include('include/components/baca-selanjutnya-detail-page.php'); ?>
            <!-- End list item y img retangle -->

        </div>
        <!-- End Content -->

        <!-- Sidebar -->
        <?php include('include/blocks/sidebar/sidebar-home.php'); ?>
        <!-- End Sidebar -->

    </div>
    <!-- End Base Content  -->

</div>
<!-- Wrap -->