
<!-- Wrap -->
<div class="wrap">

    <!-- Base Content  -->
    <div class="base-content">

        <!-- Content -->
        <div class="content">
            
            <!-- Headline One  -->
            <?php include('include/components/headline/headline-one.php'); ?>
            <!-- End Headline One  -->

            <!-- List Item X Without Date  -->
            <?php include('include/components/list-news/list-item-x-without-date-submenu.php'); ?>
            <!-- List Item X Without Date -->

            <!-- Head Title  -->
            <div class="head-title">
                <span class="c-default">
                    <a href="index.php?page=kanal" class="c-default c-news">
                        Nasional
                    </a>
                </span>
            </div>
            <!-- End Head Title  -->
            <!-- Hedline middle content x -->
            <?php include('include/components/headline/headline-middle-content-x.php'); ?>
            <!-- End Hedline middle content x -->
            
            <!-- Head Title  -->
            <div class="head-title">
                <span class="c-default">
                    <a href="index.php?page=kanal" class="c-default c-news">
                        METROPOLITAN
                    </a>
                </span>
            </div>
            <!-- End Head Title  -->
            <!-- Hedline middle content y -->
            <?php include('include/components/headline/headline-middle-content-y.php'); ?>
            <!-- End Hedline middle content y -->

            <!-- Head Title  -->
            <div class="head-title">
                <span class="c-default">
                    <a href="index.php?page=kanal" class="c-default c-news">
                        INTERNATIONAL
                    </a>
                </span>
            </div>
            <!-- End Head Title  -->
            <!-- Hedline middle content x -->
            <?php include('include/components/headline/headline-middle-content-x.php'); ?>
            <!-- End Hedline middle content x -->

            <!-- Head Title  -->
            <div class="head-title">
                <span class="c-default">
                    <a href="index.php?page=kanal" class="c-default c-video">
                        Video
                    </a>
                </span>
            </div>
            <!-- End Head Title  -->
            <!-- List Video X -->
            <?php include('include/components/list-news/list-video-x.php'); ?>
            <!-- End List Video X -->

            <!-- Head Title  -->
            <div class="head-title">
                <span class="c-default">
                    <a href="index.php?page=kanal" class="c-default c-regional">
                        Terkini
                    </a>
                </span>
            </div>
            <!-- End Head Title  -->
            <!-- List item y img retangle -->
            <?php include('include/components/list-news/list-item-y-img-retangle.php'); ?>
            <!-- End list item y img retangle -->


        </div>
        <!-- End Content -->

        <!-- Sidebar -->
        <?php include('include/blocks/sidebar/sidebar-kanal.php'); ?>
        <!-- End Sidebar -->

    </div>
    <!-- End Base Content  -->

</div>
<!-- Wrap -->
