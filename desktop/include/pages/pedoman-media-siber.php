
<!-- Wrap -->
<div class="wrap">

    <!-- Base Content  -->
    <div class="base-content">

        <!-- Content -->
        <div class="content static">
            
            <!-- Head content static -->
            <div class="head-content-static">
                <h1>
                    Pedoman Pemberitaan Media Siber
                </h1>
                <span id="date_time_now_"></span>
            </div>
            <!-- End Head content static -->

            <p>
                Kemerdekaan berpendapat, kemerdekaan berekspresi, dan kemerdekaan pers adalah hak asasi manusia yang dilindungi Pancasila, Undang-Undang Dasar 1945, dan Deklarasi Universal Hak Asasi Manusia PBB. Keberadaan media siber di Indonesia juga merupakan bagian dari kemerdekaan berpendapat, kemerdekaan berekspresi, dan kemerdekaan pers.
            </p>

            <p>
                Media siber memiliki karakter khusus sehingga memerlukan pedoman agar pengelolaannya dapat dilaksanakan secara profesional, memenuhi fungsi, hak, dan kewajibannya sesuai Undang-Undang Nomor 40 Tahun 1999 tentang Pers dan Kode Etik Jurnalistik. Untuk itu Dewan Pers bersama organisasi pers, pengelola media siber, dan masyarakat menyusun Pedoman Pemberitaan Media Siber sebagai berikut:
            </p>

            <p>
                <b>1. Ruang Lingkup</b><br/>
                Media Siber adalah segala bentuk media yang menggunakan wahana internet dan melaksanakan kegiatan jurnalistik, serta memenuhi persyaratan Undang-Undang Pers dan Standar Perusahaan Pers yang ditetapkan Dewan Pers.
            </p>
            <p>
                Isi Buatan Pengguna (User Generated Content) adalah segala isi yang dibuat dan atau dipublikasikan oleh pengguna media siber, antara lain, artikel, gambar, komentar, suara, video dan berbagai bentuk unggahan yang melekat pada media siber, seperti blog, forum, komentar pembaca atau pemirsa, dan bentuk lain.
            </p>

            <p>
                <b>2. Verifikasi dan keberimbangan berita</b><br/>
                Pada prinsipnya setiap berita harus melalui verifikasi.
            </p>
            <p>
                Berita yang dapat merugikan pihak lain memerlukan verifikasi pada berita yang sama untuk memenuhi prinsip akurasi dan keberimbangan.
            </p>
            <p>
                Ketentuan dalam butir (a) di atas dikecualikan, dengan syarat:
            </p>
            <p>Berita benar-benar mengandung kepentingan publik yang bersifat mendesak</p>
            <p>Sumber berita yang pertama adalah sumber yang jelas disebutkan identitasnya, kredibel dan kompeten</p>
            <p>Sumber berita yang pertama adalah sumber yang jelas disebutkan identitasnya, kredibel dan kompeten</p>
            <p>Subyek berita yang harus dikonfirmasi tidak diketahui keberadaannya dan atau tidak dapat diwawancarai</p>
            <p>Media memberikan penjelasan kepada pembaca bahwa berita tersebut masih memerlukan verifikasi lebih lanjut yang diupayakan dalam waktu secepatnya. Penjelasan dimuat pada bagian akhir dari berita yang sama, di dalam kurung dan menggunakan huruf miring.</p>
            <p>Setelah memuat berita sesuai dengan butir (c), media wajib meneruskan upaya verifikasi, dan setelah verifikasi didapatkan, hasil verifikasi dicantumkan pada berita pemutakhiran (update) dengan tautan pada berita yang belum terverifikasi.</p>

            <p>
                <b>3. Isi Buatan Pengguna (User Generated Content)</b></br>
            </p>
            <p>Media siber wajib mencantumkan syarat dan ketentuan mengenai Isi Buatan Pengguna yang tidak bertentangan dengan Undang-Undang No. 40 tahun 1999 tentang Pers dan Kode Etik Jurnalistik, yang ditempatkan secara terang dan jelas.</p>
            <p>Media siber mewajibkan setiap pengguna untuk melakukan registrasi keanggotaan dan melakukan proses log-in terlebih dahulu untuk dapat mempublikasikan semua bentuk Isi Buatan Pengguna. Ketentuan mengenai log-in akan diatur lebih lanjut.</p>
            <p>Dalam registrasi tersebut, media siber mewajibkan pengguna memberi persetujuan tertulis bahwa Isi Buatan Pengguna yang dipublikasikan:</p>
            <p>Tidak memuat isi bohong, fitnah, sadis dan cabul</p>
            <p>Tidak memuat isi yang mengandung prasangka dan kebencian terkait dengan suku, agama, ras, dan antargolongan (SARA), serta menganjurkan tindakan kekerasan</p>
            <p>Tidak memuat isi diskriminatif atas dasar perbedaan jenis kelamin dan bahasa, serta tidak merendahkan martabat orang lemah, miskin, sakit, cacat jiwa, atau cacat jasmani.</p>
            <p>Media siber memiliki kewenangan mutlak untuk mengedit atau menghapus Isi Buatan Pengguna yang bertentangan dengan butir (c).</p>
            <p>Media siber wajib menyediakan mekanisme pengaduan Isi Buatan Pengguna yang dinilai melanggar ketentuan pada butir (c). Mekanisme tersebut harus disediakan di tempat yang dengan mudah dapat diakses pengguna.</p>
            <p>Media siber wajib menyunting, menghapus, dan melakukan tindakan koreksi setiap Isi Buatan Pengguna yang dilaporkan dan melanggar ketentuan butir (c), sesegera mungkin secara proporsional selambat-lambatnya 2 x 24 jam setelah pengaduan diterima.</p>
            <p>Media siber yang telah memenuhi ketentuan pada butir (a), (b), (c), dan (f) tidak dibebani tanggung jawab atas masalah yang ditimbulkan akibat pemuatan isi yang melanggar ketentuan pada butir (c).</p>
            <p>Media siber bertanggung jawab atas Isi Buatan Pengguna yang dilaporkan bila tidak mengambil tindakan koreksi setelah batas waktu sebagaimana tersebut pada butir (f).</p>


            <p>
                <b>4. Ralat, Koreksi, dan Hak Jawab</b></br/>
                Ralat, koreksi, dan hak jawab mengacu pada Undang-Undang Pers, Kode Etik Jurnalistik, dan Pedoman Hak Jawab yang ditetapkan Dewan Pers.
            </p>
            <p>Ralat, koreksi dan atau hak jawab wajib ditautkan pada berita yang diralat, dikoreksi atau yang diberi hak jawab.</p>
            <p>Di setiap berita ralat, koreksi, dan hak jawab wajib dicantumkan waktu pemuatan ralat, koreksi, dan atau hak jawab tersebut.</p>
            <p>Bila suatu berita media siber tertentu disebarluaskan media siber lain, maka:</p>
            <p>Tanggung jawab media siber pembuat berita terbatas pada berita yang dipublikasikan di media siber tersebut atau media siber yang berada di bawah otoritas teknisnya</p>
            <p>Koreksi berita yang dilakukan oleh sebuah media siber, juga harus dilakukan oleh media siber lain yang mengutip berita dari media siber yang dikoreksi itu</p>
            <p>Media yang menyebarluaskan berita dari sebuah media siber dan tidak melakukan koreksi atas berita sesuai yang dilakukan oleh media siber pemilik dan atau pembuat berita tersebut, bertanggung jawab penuh atas semua akibat hukum dari berita yang tidak dikoreksinya itu.</p>
            <p>Sesuai dengan Undang-Undang Pers, media siber yang tidak melayani hak jawab dapat dijatuhi sanksi hukum pidana denda paling banyak Rp500.000.000 (Lima ratus juta rupiah).</p>

            <p>
                <b>5. Pencabutan Berita</b><br/>
                Berita yang sudah dipublikasikan tidak dapat dicabut karena alasan penyensoran dari pihak luar redaksi, kecuali terkait masalah SARA, kesusilaan, masa depan anak, pengalaman traumatik korban atau berdasarkan pertimbangan khusus lain yang ditetapkan Dewan Pers.
            </p>
            <p>Media siber lain wajib mengikuti pencabutan kutipan berita dari media asal yang telah dicabut.</p>
            <p>Pencabutan berita wajib disertai dengan alasan pencabutan dan diumumkan kepada publik.</p>

            <p>
                <b>6. Iklan</b><br/>
                Media siber wajib membedakan dengan tegas antara produk berita dan iklan.
            </p>
            <p>
                Setiap berita/artikel/isi yang merupakan iklan dan atau isi berbayar wajib mencantumkan keterangan "advertorial", " iklan" , " ads" , " sponsored", atau kata lain yang menjelaskan bahwa berita/artikel/isi tersebut adalah iklan.
            </p>

            <p>
                <b>7. Hak Cipta</b></br>
                Media siber wajib menghormati hak cipta sebagaimana diatur dalam peraturan perundang-undangan yang berlaku.
            </p>

            <p>
                <b>8. Pencantuman Pedoman</b></br>
                Media siber wajib mencantumkan Pedoman Pemberitaan Media Siber ini di medianya secara terang dan jelas.
            </p>

            <p>
                <b>9. Sengketa</b></br>
                Penilaian akhir atas sengketa mengenai pelaksanaan Pedoman Pemberitaan Media Siber ini diselesaikan oleh Dewan Pers.
            </p>

            <p>
                Jakarta, 3 Februari 2012</br>
                (Pedoman ini ditandatangani oleh Dewan Pers dan komunitas pers di Jakarta,<br/>
                3 Februari 2012).
            </p>


        </div>
        <!-- End Content -->

        <!-- Sidebar -->
        <?php include('include/blocks/sidebar/sidebar-page-statik.php'); ?>
        <!-- End Sidebar -->

    </div>
    <!-- End Base Content  -->

</div>
<!-- Wrap -->
