
<!-- Wrap -->
<div class="wrap">

    <!-- Base Content  -->
    <div class="base-content">

        <!-- Content -->
        <div class="content static">
            
            <!-- Head content static -->
            <div class="head-content-static">
                <h1>
                    site map
                </h1>
                <span id="date_time_now_"></span>
            </div>
            <!-- End Head content static -->


            <!-- Row Static -->
            <div class="row-static">

                <!-- Col Statik -->
                <div class="col-static">

                    <h2><a href="https://www.suara.com/news">News</a></h2>
                    <ul>
                        <li><a href="https://www.suara.com/news/news-category/nasional">Nasional</a></li>
                        <li><a href="https://www.suara.com/news/news-category/metropolitan">Metropolitan</a></li>
                        <li><a href="https://www.suara.com/news/news-category/internasional">Internasional</a></li>
                        <li><a href="https://www.suara.com/news/news-category/sport">Sport</a></li>
                    </ul>
                    <hr/>

                    <h2><a href="https://www.suara.com/bisnis">Bisnis</a></h2>
                    <ul>
                        <li><a href="https://www.suara.com/bisnis/bisnis-category/makro">Makro</a></li>
                        <li><a href="https://www.suara.com/bisnis/bisnis-category/keuangan">Keuangan</a></li>
                        <li><a href="https://www.suara.com/bisnis/bisnis-category/properti">Properti</a></li>
                        <li><a href="https://www.suara.com/bisnis/bisnis-category/inspiratif">Inspiratif</a></li>
                    </ul>
                    <hr/>

                    <h2><a href="https://www.suara.com/bola">Bola</a></h2>
                    <ul>
                        <li><a href="https://www.suara.com/bola/bola-category/liga-inggris">Liga Inggris</a></li>
                        <li><a href="https://www.suara.com/bola/bola-category/liga-spanyol">Liga Spanyol</a></li>
                        <li><a href="https://www.suara.com/bola/bola-category/bola-dunia">Bola Dunia</a></li>
                        <li><a href="https://www.suara.com/bola/bola-category/bola-indonesia">Bola Indonesia</a></li>
                    </ul>
                    <hr/>

                    <h2><a href="https://www.suara.com/lifestyle">Lifestyle</a></h2>
                    <ul>
                        <li><a href="https://www.suara.com/lifestyle/lifestyle-category/female">Female</a></li>
                        <li><a href="https://www.suara.com/lifestyle/lifestyle-category/male">Male</a></li>
                        <li><a href="https://www.suara.com/lifestyle/lifestyle-category/relationship">Relationship</a></li>
                        <li><a href="https://www.suara.com/lifestyle/lifestyle-category/food-travel">Food &amp; Travel</a></li>
                        <li><a href="https://www.suara.com/lifestyle/lifestyle-category/komunitas">Komunitas</a></li>
                    </ul>
                    <hr/>
                    
                    <h2><a href="https://www.suara.com/entertainment">Entertainment</a></h2>
                    <ul>
                        <li><a href="https://www.suara.com/entertainment/entertainment-category/gosip">Gosip</a></li>
                        <li><a href="https://www.suara.com/entertainment/entertainment-category/music">Music</a></li>
                        <li><a href="https://www.suara.com/entertainment/entertainment-category/film">Film</a></li>
                    </ul>
                    <hr/>
                    
                    <h2><a href="https://www.suara.com/otomotif">Otomotif</a></h2>
                    <ul>
                        <li><a href="https://www.suara.com/otomotif/otomotif-category/mobil">Mobil</a></li>
                        <li><a href="https://www.suara.com/otomotif/otomotif-category/motor">Motor</a></li>
                        <li><a href="https://www.suara.com/otomotif/otomotif-category/autoseleb">AutoSeleb</a>
                        </li>
                    </ul>
                    <hr/>
                    
                    <h2><a href="https://www.suara.com/tekno">Tekno</a></h2>
                    <ul>
                        <li><a href="https://www.suara.com/tekno/tekno-category/internet">Internet</a></li>
                        <li><a href="https://www.suara.com/tekno/tekno-category/gadget">Gadget</a></li>
                        <li><a href="https://www.suara.com/tekno/tekno-category/tekno">Tekno</a></li>
                        <li><a href="https://www.suara.com/tekno/tekno-category/sains">Sains</a></li>
                    </ul>
                    <hr/>
                </div>

                <div class="col-static">

                    <h2><a href="https://www.suara.com/health">Health</a></h2>
                    <ul>
                        <li><a href="https://www.suara.com/health/health-category/women">Women</a></li>
                        <li><a href="https://www.suara.com/health/health-category/men">Men</a></li>
                        <li><a href="https://www.suara.com/health/health-category/parenting">Parenting</a></li>
                        <li><a href="https://www.suara.com/health/health-category/konsultasi">Konsultasi</a></li>
                    </ul>
                    <hr/>
                    
                    <h2><a href="https://www.suara.com/foto">Foto</a></h2>
                    <ul>
                        <li><a href="https://www.suara.com/foto/foto-category/news">News</a></li>
                        <li><a href="https://www.suara.com/foto/foto-category/bola">Bola</a></li>
                        <li><a href="https://www.suara.com/foto/foto-category/lifestyle">Lifestyle</a></li>
                        <li><a href="https://www.suara.com/foto/foto-category/entertainment">Entertainment</a></li>
                        <li><a href="https://www.suara.com/foto/foto-category/otomotif">Otomotif</a></li>
                        <li><a href="https://www.suara.com/foto/foto-category/tekno">Tekno</a></li>
                    </ul>
                    <hr/>
                    
                    <h2><a href="https://www.suara.com/video">Video</a></h2>
                    <ul>
                        <li><a href="https://www.suara.com/video/video-category/news">News</a></li>
                        <li><a href="https://www.suara.com/video/video-category/bola">Bola</a></li>
                        <li><a href="https://www.suara.com/video/video-category/lifestyle">Lifestyle</a></li>
                        <li><a href="https://www.suara.com/video/video-category/entertainment">Entertainment</a></li>
                        <li><a href="https://www.suara.com/video/video-category/otomotif">Otomotif</a></li>
                        <li><a href="https://www.suara.com/video/video-category/tekno">Tekno</a></li>
                        <li><a href="https://www.suara.com/video/video-category/infografis">Infografis</a></li>
                    </ul>
                    <hr/>
                    
                    <h2><a href="https://yoursay.suara.com/">Your Say</a></h2>
                    <ul>
                        <li><a href="https://yoursay.suara.com//yoursay-category/news">News</a></li>
                        <li><a href="https://yoursay.suara.com//yoursay-category/lifestyle">Lifestyle</a></li>
                        <li><a href="https://yoursay.suara.com//yoursay-category/foto">Foto</a></li>
                    </ul>
                    <hr/>
                    
                    <h2><a href="https://www.suara.com/pressrelease">Press Release</a></h2>
                    <hr/>
                    <h2><a href="https://www.suara.com/wawancara">Wawancara</a></h2>
                    <hr/>
                    <h2><a href="https://www.suara.com/suratpembaca">Surat Pembaca</a></h2>
                    <hr/>
                    <h2><a href="https://www.suara.com/indeks">Indeks</a></h2>
                    <hr/>
                </div>
                <!-- End Col Statik -->

            </div>
            <!-- End Row Statik -->


        </div>
        <!-- End Content -->

        <!-- Sidebar -->
        <?php include('include/blocks/sidebar/sidebar-page-statik.php'); ?>
        <!-- End Sidebar -->

    </div>
    <!-- End Base Content  -->

</div>
<!-- Wrap -->
