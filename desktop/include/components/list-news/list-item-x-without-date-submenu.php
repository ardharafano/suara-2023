<div class="list-item-x">

    <div class="item">
        <div class="box">
            <div class="img-thumb-1">
                <a href="index.php?page=detail" aria-label="Insiden Perahu Tenggelam di Sungai Mentaya, Satu Anggota Polisi Meninggal">
                    <img src="assets/images/examples/list-square1.jpg" alt="News suara.com"
                    width="88px" height="84px" />
                </a>
            </div>
            <div class="description">
                <span>
                    <a href="index.php?page=kanal" class="c-default c-news">
                        NASIONAL
                    </a>
                </span>
                <h2>
                    <a href="index.php?page=detail">
                        Insiden Perahu Tenggelam di Sungai Mentaya, Satu Anggota Polisi Meninggal
                    </a>
                </h2>
                <span class="date">
                    12:27 WIB   
                </span>
            </div>
        </div>
    </div>

    <div class="item">
        <div class="box">
            <div class="img-thumb-1">
                <a href="index.php?page=detail" aria-label="Insiden Perahu Tenggelam di Sungai Mentaya, Satu Anggota Polisi Meninggal">
                    <img src="assets/images/examples/list-square2.jpg" alt="News suara.com"
                    width="88px" height="84px" />
                </a>
            </div>
            <div class="description">
                <span>
                    <a href="index.php?page=kanal" class="c-default c-news">
                        METROPOLITAN
                    </a>
                </span>
                <h2>
                    <a href="index.php?page=detail">
                        Insiden Perahu Tenggelam di Sungai Mentaya, Satu Anggota Polisi Meninggal
                    </a>
                </h2>
                <span class="date">
                    12:27 WIB   
                </span>
            </div>
        </div>
    </div>

    <div class="item">
        <div class="box">
            <div class="img-thumb-1">
                <a href="index.php?page=detail" aria-label="Insiden Perahu Tenggelam di Sungai Mentaya, Satu Anggota Polisi Meninggal">
                    <img src="assets/images/examples/list-square2.jpg" alt="News suara.com"
                    width="88px" height="84px" />
                </a>
            </div>
            <div class="description">
                <span>
                    <a href="index.php?page=kanal" class="c-default c-news">
                        INTERNASIONAL
                    </a>
                </span>
                <h2>
                    <a href="index.php?page=detail">
                        Hari Ini, Polda Sumut Jadwalkan Periksa Guru Besar USU
                    </a>
                </h2>
                <span class="date">
                    12:27 WIB   
                </span>
            </div>
        </div>
    </div>

    <div class="item">
        <div class="box">
            <div class="img-thumb-1">
                <a href="index.php?page=detail" aria-label="Insiden Perahu Tenggelam di Sungai Mentaya, Satu Anggota Polisi Meninggal">
                    <img src="assets/images/examples/list-square1.jpg" alt="News suara.com"
                    width="88px" height="84px" />
                </a>
            </div>
            <div class="description">
                <span>
                    <a href="index.php?page=kanal" class="c-default c-news">
                        NASIONAL
                    </a>
                </span>
                <h2>
                    <a href="index.php?page=detail">
                        Insiden Perahu Tenggelam di Sungai Mentaya, Satu Anggota Polisi Meninggal
                    </a>
                </h2>
                <span class="date">
                    12:27 WIB   
                </span>
            </div>
        </div>
    </div>

</div>