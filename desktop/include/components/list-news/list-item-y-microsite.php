<div class="list-item-y microsite">

    <div class="item">
        <div class="box">
            <div class="img-thumb">
                <a href="index.php?page=detail" aria-label="Insiden Perahu Tenggelam di Sungai Mentaya, Satu Anggota Polisi Meninggal">
                    <img src="assets/images/examples/list-square1.jpg" alt="News suara.com"
                    width="88px" height="84px" />
                </a>
            </div>
            <div class="description">
                <h2>
                    <a href="index.php?page=detail">
                        Insiden Perahu Tenggelam di Sungai Mentaya, Satu Anggota Polisi Meninggal
                    </a>
                </h2>
                <span class="date">
                    29 July, 2020 | 12:27 WIB   
                </span>
            </div>
        </div>
    </div>

    <div class="item">
        <div class="box">
            <div class="img-thumb">
                <a href="index.php?page=detail" aria-label="Insiden Perahu Tenggelam di Sungai Mentaya, Satu Anggota Polisi Meninggal">
                    <img src="assets/images/examples/list-square2.jpg" alt="News suara.com"
                    width="88px" height="84px" />
                </a>
            </div>
            <div class="description">
                <h2>
                    <a href="index.php?page=detail">
                        Insiden Perahu Tenggelam di Sungai Mentaya, Satu Anggota Polisi Meninggal
                    </a>
                </h2>
                <span class="date">
                    29 July, 2020 | 12:27 WIB   
                </span>
            </div>
        </div>
    </div>

    <div class="item">
        <div class="box">
            <div class="img-thumb">
                <a href="index.php?page=detail" aria-label="Insiden Perahu Tenggelam di Sungai Mentaya, Satu Anggota Polisi Meninggal">
                    <img src="assets/images/examples/list-square2.jpg" alt="News suara.com"
                    width="88px" height="84px" />
                </a>
            </div>
            <div class="description">
                <h2>
                    <a href="index.php?page=detail">
                        Hari Ini, Polda Sumut Jadwalkan Periksa Guru Besar USU
                    </a>
                </h2>
                <span class="date">
                    29 July, 2020 | 12:27 WIB   
                </span>
            </div>
        </div>
    </div>

    <div class="item">
        <div class="box">
            <div class="img-thumb">
                <a href="index.php?page=detail" aria-label="Insiden Perahu Tenggelam di Sungai Mentaya, Satu Anggota Polisi Meninggal">
                    <img src="assets/images/examples/list-square2.jpg" alt="News suara.com"
                    width="88px" height="84px" />
                </a>
            </div>
            <div class="description">
                <h2>
                    <a href="index.php?page=detail">
                        Hari Ini, Polda Sumut Jadwalkan Periksa Guru Besar USU
                    </a>
                </h2>
                <span class="date">
                    29 July, 2020 | 12:27 WIB   
                </span>
            </div>
        </div>
    </div>
    
</div>