<div class="nav-menu-top">
    <div class="wrap">
        <ul>
            <li>
                <div class="dropdown">
                    <a href="index.php?page=kanal">
                        <button class="dropbtn">INDEKS</button>
                    </a>
                </div>
            </li>
            <li>
                <div class="dropdown">
                    <a href="index.php?page=kanal">
                        <button class="dropbtn">NEWS <i class="icon icon-down"></i></button>
                    </a>
                    <div class="dropdown-content">
                        <a href="?page=kanal">Nasional</a>
                        <a href="?page=kanal">Metropolitan</a>
                        <a href="?page=kanal">Internasional</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="dropdown">
                    <a href="index.php?page=kanal">
                        <button class="dropbtn">BISNIS <i class="icon icon-down"></i></button>
                    </a>
                    <div class="dropdown-content">
                        <a href="?page=kanal">Makro</a>
                        <a href="?page=kanal">Keuangan</a>
                        <a href="?page=kanal">Properti</a>
                        <a href="?page=kanal">Inspiratif</a>
                        <a href="?page=kanal">Ekopol</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="dropdown">
                    <a href="index.php?page=kanal">
                        <button class="dropbtn">BOLA <i class="icon icon-down"></i></button>
                    </a>
                    <div class="dropdown-content">
                        <a href="?page=kanal">Liga inggris</a>
                        <a href="?page=kanal">Liga Spanyol</a>
                        <a href="?page=kanal">Bola Dunia</a>
                        <a href="?page=kanal">Bola Indonesia</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="dropdown">
                    <a href="index.php?page=kanal">
                        <button class="dropbtn">SPORT <i class="icon icon-down"></i></button>
                    </a>
                    <div class="dropdown-content">
                        <a href="?page=kanal">Raket</a>
                        <a href="?page=kanal">Balap</a>
                        <a href="?page=kanal">Arena</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="dropdown">
                    <a href="index.php?page=kanal">
                        <button class="dropbtn">lIFESTYLE <i class="icon icon-down"></i></button>
                    </a>
                    <div class="dropdown-content">
                        <a href="?page=kanal">Raket</a>
                        <a href="?page=kanal">Balap</a>
                        <a href="?page=kanal">Arena</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="dropdown">
                    <a href="index.php?page=kanal">
                        <button class="dropbtn">ENTERTAINMENT <i class="icon icon-down"></i></button>
                    </a>
                    <div class="dropdown-content">
                        <a href="?page=kanal">Gosip</a>
                        <a href="?page=kanal">Musik</a>
                        <a href="?page=kanal">Film</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="dropdown">
                    <a href="index.php?page=kanal">
                        <button class="dropbtn">OTOMOTIF <i class="icon icon-down"></i></button>
                    </a>
                    <div class="dropdown-content">
                        <a href="?page=kanal">Mobil</a>
                        <a href="?page=kanal">Motor</a>
                        <a href="?page=kanal">Autoseleb</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="dropdown">
                    <a href="index.php?page=kanal">
                        <button class="dropbtn">TEKNO <i class="icon icon-down"></i></button>
                    </a>
                    <div class="dropdown-content">
                        <a href="?page=kanal">Internet</a>
                        <a href="?page=kanal">Gadget</a>
                        <a href="?page=kanal">Tekno</a>
                        <a href="?page=kanal">Sains</a>
                        <a href="?page=kanal">Game</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="dropdown">
                    <a href="index.php?page=kanal">
                        <button class="dropbtn">HEALTH <i class="icon icon-down"></i></button>
                    </a>
                    <div class="dropdown-content">
                        <a href="?page=kanal">Women</a>
                        <a href="?page=kanal">Men</a>
                        <a href="?page=kanal">Parenting</a>
                        <a href="?page=kanal">Konsultasi</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="dropdown">
                    <a href="index.php?page=kanal">
                        <button class="dropbtn">FOTO <i class="icon icon-down"></i></button>
                    </a>
                    <div class="dropdown-content">
                        <a href="?page=kanal">News</a>
                        <a href="?page=kanal">Bola</a>
                        <a href="?page=kanal">Lifestyle</a>
                        <a href="?page=kanal">Entertainment</a>
                        <a href="?page=kanal">Otomotif</a>
                        <a href="?page=kanal">Tekno</a>
                        <a href="?page=kanal">Essay</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="dropdown">
                    <a href="index.php?page=kanal">
                        <button class="dropbtn">VIDEO <i class="icon icon-down"></i></button>
                    </a>
                    <div class="dropdown-content">
                        <a href="?page=kanal">News</a>
                        <a href="?page=kanal">Entertainment</a>
                        <a href="?page=kanal">Bola</a>
                        <a href="?page=kanal">Lifestyle</a>
                        <a href="?page=kanal">Otomotif</a>
                        <a href="?page=kanal">Tekno</a>
                        <a href="?page=kanal">Infografis</a>
                        <a href="?page=kanal">Sport</a>
                    </div>
                </div>
            </li>
            <!-- <li>
                <a href="index.php?page=kanal">
                    Network
                </a>
            </li> -->
        </ul>
    </div>
</div>