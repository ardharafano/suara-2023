<div class="nav-menu-regional">
    <div class="wrap">
        <ul>
            <!-- <li>
                <a href="https://suarajakarta.id" title="Suara Jakarta">Jakarta</a>
            </li>
            <li>
                <a href="https://suarabogor.id" title="Suara Bogor">bogor</a>
            </li>
            <li>
                <a href="https://suarabekaci.id" title="Suara Bekaci">bekaci</a>
            </li>
            <li>
                <a href="https://suarajabar.id" title="Suara Jabar">jabar</a>
            </li>
            <li>
                <a href="https://suarajogja.id" title="Suara Jogja">jogja</a> 
            </li>
            <li>
                <a href="https://suarajawatengah.id" title="Suara Jateng">jateng</a>
            </li> -->
            <li>
                <a href="https://suaramalang.id" title="Suara Malang">malang</a>
            </li>
            <li> 
                <a href="https://suarajatim.id" title="Suara Jatim">jatim</a>
            </li>
            <li>
                <a href="https://suarabali.id" title="Suara Bali">bali</a>
            </li>
            <li>
                <a href="https://suaralampung.id" title="Suara Lampung">lampung</a> 
            </li>
        </ul>
        <ul> 
            <li>
                <a href="https://suarabanten.id" title="Suara Banten">banten</a>
            </li>
            <li>
                <a href="https://suarasurakarta.id" title="Suara Surakarta">surakarta</a>
            </li>
            <li>
                <a href="https://suarakaltim.id" title="Suara Kaltim">kaltim</a> 
            </li>
            <li>
                <a href="https://suarakalbar.id" title="Suara Kalbar">kalbar</a> 
            </li>
            <li>
                <a href="https://suarasulsel.id" title="Suara Sulsel">sulsel</a> 
            </li>
            <li>
                <a href="https://suarasumut.id" title="Suara Sumut">sumut</a> 
            </li>
            <li>
                <a href="https://suarasumbar.id" title="Suara Sumbar">sumbar</a> 
            </li>
            <li>
                <a href="https://suarasumsel.id" title="Suara Sumsel">sumsel</a> 
            </li>
            <li>
                <a href="https://suarabatam.id" title="Suara Batam">batam</a> 
            </li>
            <li>
                <a href="https://suarariau.id" title="Suara Riau">riau</a> 
            </li>
        </ul>
    </div>
</div>