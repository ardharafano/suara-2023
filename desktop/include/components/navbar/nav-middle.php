<div class="nav-middle">
    <div class="wrap">
        <ul>
            <li>
                <div class="logo">
                    <a href="index.php?page=home" aria-label="suara.com">
                        <img src="assets/images/suara.svg" alt="logo suara" width="229px" height="26px" />
                    </a>
                    <a href="#" aria-label="cek fakta suara.com">
                        <img src="assets/images/cek-fakta.svg" alt="logo cek fakta" width="35px" height="35px" />
                    </a>
                </div>
            </li>
            <li>
                <div class="search">
                    <form action="index.php" method="get">
                        <div class="wrap-search">
                            <input type="text" name="search" placeholder="Search" />
                            <i class="icon icon-search"></i>
                        </div>
                    </form>
                </div>
                <div class="auth">
                    <a href="#" aria-label="user login">
                        <i class="icon-svg icon-user"></i>
                    </a>
                    <a href="#" aria-label="user write">
                        <i class="icon-svg icon-pencil"></i>
                    </a>
                    <a href="#" class="btn-signup">
                        Daftar
                    </a>
                </div>
            </li>
            <li>
                <div
                    class="share-sosmed">
                    <a href="#" class="social-icon">
                        <img src="assets/images/icons/fb.svg" alt="logo suara" width="18" height="18" />
                    </a>

                    <a href="#" class="social-icon">
                        <img src="assets/images/icons/twt.svg" alt="logo suara" width="18" height="18" />
                    </a>

                    <a href="#" class="social-icon">
                        <img src="assets/images/icons/yt.svg" alt="logo suara" width="18" height="18" />
                    </a>

                    <a href="#" class="social-icon">
                        <img src="assets/images/icons/ig.svg" alt="logo suara" width="18" height="18" />
                    </a>

                    <a href="#" class="social-icon">
                        <img src="assets/images/icons/tiktok.svg" alt="logo suara" width="18" height="18" />
                    </a>
                </div>
            </li>
        </ul>
    </div>
</div>