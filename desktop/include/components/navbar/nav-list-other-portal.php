<div class="list-other-portal">
    <div class="wrap">
        <ul class="other-portal">
            <li>
                <a href="http://arkadiacorp.com/" target="_blank">
                    <img src="assets/images/icons-other-portal/arkadia.svg" alt="Arkadia Group"
                    width="65px" height="23px"/>
                </a>
            </li>
            <li>
                <a href="https://www.matamata.com" target="_blank">
                    <img src="assets/images/icons-other-portal/suara.svg" alt="Arkadia Group"
                    width="25px" height="25px" />
                    <span>
                        SUARA.COM
                    </span>
                </a>
            </li>
            <li>
                <a href="https://www.matamata.com" target="_blank">
                    <img src="assets/images/icons-other-portal/mata-mata.svg" alt="Arkadia Group"
                    width="25px" height="25px" />
                    <span>
                        MATAMATA.COM
                    </span>
                </a>
            </li>
            <li>
                <a href="https://www.bolatimes.com" target="_blank">
                    <img src="assets/images/icons-other-portal/bolatimes.svg" alt="Arkadia Group"
                    width="25px" height="25px" />
                    <span>
                        BOLATIMES.COM
                    </span>
                </a>
            </li>
            <li>
                <a href="https://www.hitekno.com" target="_blank">
                    <img src="assets/images/icons-other-portal/hitekno.svg" alt="Arkadia Group"
                    width="25px" height="25px" />
                    <span>
                        HITEKNO.COM
                    </span>
                </a>
            </li>
            <li>
                <a href="https://www.dewiku.com" target="_blank">
                    <img src="assets/images/icons-other-portal/dewiku.svg" alt="Arkadia Group"
                    width="25px" height="25px" />
                    <span>
                        DEWIKU.COM
                    </span>
                </a>
            </li>
            <li>
                <a href="https://www.mobimoto.com" target="_blank">
                    <img src="assets/images/icons-other-portal/mobimoto.svg" alt="Arkadia Group"
                    width="25px" height="25px" />
                    <span>
                        MOBIMOTO.COM
                    </span>
                </a>
            </li>
            <li>
                <a href="https://www.guideku.com/" target="_blank">
                    <img src="assets/images/icons-other-portal/guideku.svg" alt="Arkadia Group"
                    width="25px" height="25px" />
                    <span>
                        GUIDEKU.COM
                    </span>
                </a>
            </li>
            <li>
                <a href="https://www.himedik.com/" target="_blank">
                    <img src="assets/images/icons-other-portal/himedik.svg" alt="Arkadia Group"
                    width="25px" height="25px" />
                    <span>
                        HIMEDIK.COM
                    </span>
                </a>
            </li>
            <li>
                <a href="https://www.iklandisini.com/" target="_blank">
                    <img src="assets/images/icons-other-portal/iklandisini.svg" alt="Arkadia Group"
                    width="25px" height="25px" />
                    <span>
                        IKLANDISINI.COM
                    </span>
                </a>
            </li>
            <li>
                <a href="https://www.serbada.com/" target="_blank">
                    <img src="assets/images/icons-other-portal/serbada.svg" alt="Arkadia Group"
                    width="25px" height="25px" />
                    <span>
                        SERBADA.COM
                    </span>
                </a>
            </li>
        </ul>
    </div>
</div>