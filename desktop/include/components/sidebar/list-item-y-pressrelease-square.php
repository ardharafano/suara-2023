<div class="side-list-item-y-square">

    <div class="item">
        <div class="box">
            <div class="img-thumb">
                <a href="index.php?page=detail" aria-label="Insiden Perahu Tenggelam di Sungai Mentaya, Satu Anggota Polisi Meninggal">
                    <img src="assets/images/examples/list-square1.jpg" alt="News suara.com"
                    width="88px" height="84px" />
                </a>
            </div>
            <div class="description">
                <span>
                    <a href="index.php?page=kanal"  class="c-default">
                        press release
                    </a>
                    <span class="date">
                        12:27 WIB   
                    </span>
                </span>
                <h2>
                    <a href="index.php?page=detail">
                        Insiden Perahu Tenggelam di Sungai Mentaya, Satu Anggota Polisi Meninggal
                    </a>
                </h2>
            </div>
        </div>
    </div>

    <div class="item">
        <div class="box">
            <div class="img-thumb">
                <a href="index.php?page=detail" aria-label="Insiden Perahu Tenggelam di Sungai Mentaya, Satu Anggota Polisi Meninggal">
                    <img src="assets/images/examples/list-square2.jpg" alt="News suara.com"
                    width="88px" height="84px" />
                </a>
            </div>
            <div class="description">
                <span>
                    <a href="index.php?page=kanal"  class="c-default">
                        press release
                    </a>
                    <span class="date">
                        12:27 WIB   
                    </span>
                </span>
                <h2>
                    <a href="index.php?page=detail">
                        Sekolah Ditutup Lagi Gegara Guru Positif Corona, 100 Siswa Tes Swab Massal
                    </a>
                </h2>
            </div>
        </div>
    </div>

    <div class="item">
        <div class="box">
            <div class="img-thumb">
                <a href="index.php?page=detail" aria-label="Insiden Perahu Tenggelam di Sungai Mentaya, Satu Anggota Polisi Meninggal">
                    <img src="assets/images/examples/list-square1.jpg" alt="News suara.com"
                    width="88px" height="84px" />
                </a>
            </div>
            <div class="description">
                <span>
                    <a href="index.php?page=kanal"  class="c-default">
                        press release
                    </a>
                    <span class="date">
                        12:27 WIB   
                    </span>
                </span>
                <h2>
                    <a href="index.php?page=detail">
                        Sekolah Ditutup Lagi Gegara Guru Positif Corona, 100 Siswa Tes Swab Massal
                    </a>
                </h2>
            </div>
        </div>
    </div>
    
    <a href="index.php?page=kanal" class="btn bg-darkslateblue">
        Tampilkan lebih banyak
    </a>

</div>