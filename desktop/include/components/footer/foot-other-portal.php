<div class="foot-other-portal">
    <div class="wrap">

        <!-- Arkadia Portal -->
        <div class="item">
            <ul>
                <li>
                    <img src="assets/images/arkadia.svg" width="119px" height="40px" 
                    class="logo-arkadia" alt="Logo arkadia" />
                </li>
            </ul>
            <ul>
                <li>
                    <a href="#">
                        Suara.com
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        matamata.com
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        bolatimes.com
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        hitekno.com
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        dewiku.com
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        mobimoto.com
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        guideku.com
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        himedik.com
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        serbada.com
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        iklandisini.com
                    </a>
                </li>
            </ul>
        </div>
        <!-- End Arkadia Portal -->

        <!-- Suara Regional -->
        <div class="item">
            <ul>
                <li>
                    <a href="#">
                        <b>
                            Suara Regional
                        </b>
                    </a>
                </li>
            </ul>
            <ul>
                <li>
                    <a href="#" target="_blank">
                        suarajakarta.id
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        suarabogor.id
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        suarabekaci.id
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        suarajabar.id
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        suarajogja.id
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        suarajawatengah.id
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        suaramalang.id
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        suarajatim.id
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        suarabali.id
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        suaralampung.id
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        suarabanten.id
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        suarasurakarta.id
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        suarakaltim.id
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        suarakalbar.id
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        suarasulsel.id
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        suarasumut.id
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        suarasumbar.id
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        suarasumsel.id
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        suarabatam.id
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        suarariau.id
                    </a>
                </li>
            </ul>
        </div>
        <!-- End Suara Regional -->

        <!-- Network -->
        <div class="item">
            <ul>
                <li>
                    <a href="#">
                        <b>
                            Network
                        </b>
                    </a>
                </li>
            </ul>
            <ul>
                <li>
                    <a href="#" target="_blank">
                        bbc.com
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        abc.net.au
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        dw.com
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        fajarsatu
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        blokbojonegoro
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        bloktuban
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        telisik
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        berita manado
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        sumselupdate
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        inibalikpapan
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        digtara
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        abc.net.au
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        dw.com
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        fajarsatu
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        blokbojonegoro
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        bloktuban
                    </a>
                </li>
                <li>
                    <a href="#" target="_blank">
                        telisik
                    </a>
                </li>
            </ul>
        </div>
        <!-- End Network -->
    
    </div>
</div>