<div class="wrap-list-canal">
    <!-- List Canal -->
    <ul class="list-canal">
        <li>
            <div class="box">
                <a href="index.php?page=kanal">
                    News
                </a>
                <i class="icon icon-down open-sub"></i>
                <ul class="sub-menu">
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Nasional
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Metropolitan
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Internasional
                        </a>
                    </li>
                </ul>
            </div>
        </li>

        <li>
            <div class="box">
                <a href="index.php?page=kanal">
                    Lifestyle
                </a>
                <i class="icon icon-down open-sub"></i>
                <ul class="sub-menu">
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Female
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Male
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Relationship
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Food & Travel
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Komunitas
                        </a>
                    </li>
                </ul>
            </div>
        </li>

        <li>
            <div class="box">
                <a href="index.php?page=kanal">
                    Indeks
                </a>
            </div>
        </li>

        <li>
            <div class="box">
                <a href="index.php?page=kanal">
                    Pilihan
                </a>
            </div>
        </li>


        <li>
            <div class="box">
                <a href="index.php?page=kanal">
                    Terpopuler
                </a>
            </div>
        </li>
    </ul>

    <ul class="list-canal">
        <li>
            <div class="box">
                <a href="index.php?page=kanal">
                    Olahraga
                </a>
            </div>
        </li>

        <li>
            <div class="box">
                <a href="index.php?page=kanal">
                    Gaya Hidup
                </a>
            </div>
        </li>

        <li>
            <div class="box">
                <a href="index.php?page=kanal">
                    Foto
                </a>
            </div>
        </li>

        <li>
            <div class="box">
                <a href="index.php?page=kanal">
                    Video
                </a>
            </div>
        </li>

    </ul>
    <!-- <ul class="list-canal">
        <li>
            <div class="box">
                <a href="index.php?page=kanal">
                    News 
                </a>
                <i class="icon icon-down open-sub"></i>
                <ul class="sub-menu">
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Nasional
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Metropolitan
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Internasional
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li>
            <div class="box">
                <a href="index.php?page=kanal">
                    Bisnis
                </a>
                <i class="icon icon-down open-sub"></i>
                <ul class="sub-menu">
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Makro
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Keuangan
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Properti
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Inspiratif
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li>
            <div class="box">
                <a href="index.php?page=kanal">
                    Bola
                </a>
                <i class="icon icon-down open-sub"></i>
                <ul class="sub-menu">
                    <li>
                        <a href="index.php?page=sub-kanal">
                            liga inggris
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            liga spanyol
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            bola dunia
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            bola indonesia
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li>
            <div class="box">
                <a href="index.php?page=kanal">
                    Sport
                </a>
                <i class="icon icon-down open-sub"></i>
                <ul class="sub-menu">
                    <li>
                        <a href="index.php?page=sub-kanal">
                            raket
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            balap
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            arena
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li>
            <div class="box">
                <a href="index.php?page=kanal">
                    Lifestyle
                </a>
                <i class="icon icon-down open-sub"></i>
                <ul class="sub-menu">
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Female
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            male
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            relationship
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            food & travel
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            komunitas
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li>
            <div class="box">
                <a href="index.php?page=kanal">
                    Entertainment
                </a>
                <i class="icon icon-down open-sub"></i>
                <ul class="sub-menu">
                    <li>
                        <a href="index.php?page=sub-kanal">
                            gosip
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            music
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            film
                        </a>
                    </li>
                </ul>
            </div>
        </li>
    </ul>

    <ul class="list-canal">
        <li>
            <div class="box">
                <a href="index.php?page=kanal">
                    Otomotif
                </a>
                <i class="icon icon-down open-sub"></i>
                <ul class="sub-menu">
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Mobil
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Motor
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Autoseleb
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li>
            <div class="box">
                <a href="index.php?page=kanal">
                    Tekno
                </a>
                <i class="icon icon-down open-sub"></i>
                <ul class="sub-menu">
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Internet
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Gadget
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            tekno
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            sains
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li>
            <div class="box">
                <a href="index.php?page=kanal">
                    Health
                </a>
                <i class="icon icon-down open-sub"></i>
                <ul class="sub-menu">
                    <li>
                        <a href="index.php?page=sub-kanal">
                            Women
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            men
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            parenting
                        </a>
                    </li>
                    <li>
                        <a href="index.php?page=sub-kanal">
                            konsultasi
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li>
            <div class="box">
                <a href="index.php?page=kanal">
                    Foto
                </a>
            </div>
        </li>
        <li>
            <div class="box">
                <a href="index.php?page=kanal">
                    Video
                </a>
            </div>
        </li>
        <li>
            <div class="box">
                <a href="index.php?page=kanal">
                    Network
                </a>
            </div>
        </li>
    </ul> -->
    <!-- End List Canal  -->
</div>