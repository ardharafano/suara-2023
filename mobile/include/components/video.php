<div class="headline-one video">
    <div class="thumb-img">
        <a href="<?php echo $url.'page=detail'; ?>">
            <img src="assets/images/examples/headline.jpg" width="330px" height="185px" class="img-responsive" alt="berita terkini" />
        </a>
        <i class="icon-svg icon-play-white"></i>
    </div>
    <h2>
        <a href="<?php echo $url.'page=detail'; ?>">
            Perancis Larang Perempuan Berhijab Antar 
        </a>
    </h2>
    <span>
        <a href="<?php echo $url.'page=kanal'; ?>">
            video
        </a>
    </span>
</div>
<div class="list-item-x">
    <div class="item">
        <div class="box">
            <div class="thumb-img">
                <a href="<?php echo $url.'page=detail'; ?>">
                    <img src="assets/images/examples/list1.jpg" width="160px" height="88px" class="img-responsive" alt="berita terkini" />
                </a>
            </div>
            <h2>
                <a href="<?php echo $url.'page=detail'; ?>">
                    Kisruh Rumah Tangga, Celine Evangelista Berharap Tak Bercerai
                </a>
            </h2>
            <span>
                <a href="<?php echo $url.'page=kanal'; ?>">
                    video
                </a>
            </span>
        </div>
    </div>
    <div class="item">
        <div class="box">
            <div class="thumb-img">
                <a href="<?php echo $url.'page=detail'; ?>">
                    <img src="assets/images/examples/list2.jpg" width="160px" height="88px" class="img-responsive" alt="berita terkini" />
                </a>
            </div>
            <h2>
                <a href="<?php echo $url.'page=detail'; ?>">
                    5 Jenis Dokumen Perjalanan Luar Negeri yang Perlu Anda Ketahui
                </a>
            </h2>
            <span>
                <a href="<?php echo $url.'page=kanal'; ?>">
                    video
                </a>
            </span>
        </div>
    </div>
</div>