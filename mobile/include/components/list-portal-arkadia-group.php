<div class="item">
    <ul>
        <li>
            <img src="assets/images/arkadia.svg" width="119px" height="40px" class="logo-arkadia" alt="Logo arkadia" />
        </li>
    </ul>
    <ul>
        <li>
            <a href="#">
                Suara.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                matamata.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                bolatimes.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                hitekno.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                dewiku.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                mobimoto.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                guideku.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                himedik.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                serbada.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                iklandisini.com
            </a>
        </li>
    </ul>
</div>

<div class="item">
    <ul>
        <li>
            <a href="#">
                <b>
                    Suararegional
                </b>
            </a>
        </li>
    </ul>
    <ul>
        <li>
            <a href="#" target="_blank">
                suarajakarta.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suarabogor.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suarabekaci.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suarajabar.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suarajogja.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suarajawatengah.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suaramalang.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suarajatim.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suarabali.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suaralampung.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suarabanten.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suarasurakarta.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suarakaltim.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suarakalbar.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suarasulsel.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suarasumut.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suarasumbar.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suarasumsel.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suarabatam.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suarariau.id
            </a>
        </li>
    </ul>
</div>

<div class="item">
    <ul>
        <li>
            <a href="#">
                <b>
                    Network
                </b>
            </a>
        </li>
    </ul>
    <ul>
        <li>
            <a href="#" target="_blank">
                bbc.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
            abc.net.au
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                dw.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                fajarsatu
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                blogbojonegoro
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                bloktuban
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                telisik
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                berita manado
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                sumselupdate
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                inibalikpapan
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                digtara
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
            fornews
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                itulah
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                the asian parent
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                seuramoeaceh.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                idpost.co.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                fajarsumbar.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                wartamataram.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                digtraksi.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                hops.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                ligo.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                gopos.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                memulai.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                pantau.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                gosulut.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                lifepal.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                obormotindok.co.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                makassar terkini
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                ungkap.co.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                zona utara
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                jatim.net
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                satelitpost.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suarakalbar.co.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                pojokcelebes.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                sukabumiupdate.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suarajatimpost.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                suaraindonesia.co.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                beritabali.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                kalbarupdates.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                barta1.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                medanheadlines.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                thesonet.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                minagkabaunews.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                gamebrott
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                kabarmakassar.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                yukepo.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                keepo.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                feedburner.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                klikbabel.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                bantenhits.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                bantennews.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                klikpositif.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                portalsatu.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                nttonlinenow.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                solopos.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                covesia.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                lombokita.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                timesindonesia.co.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                batamnews.co.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                riauonline.co.id
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                ampera.co
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                kabarnusa.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                saibumi.com
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                kabarpapua.co
            </a>
        </li>
        <li>
            <a href="#" target="_blank">
                kabar medan
            </a>
        </li>
    </ul>
</div>