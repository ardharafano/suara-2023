<div class="next-article" id="next-article">
    <label class="close-next" for="close-next">
        <i class="icon-svg icon-close"></i>
    </label>
    <input type="checkbox" hidden="" id="close-next">
    <div class="baca-juga-1">
        <div class="item-baca-juga"> 
            <div class="item--baca-juga-inner"> 
                <div class="post-thumb"> 
                    <a href="https://www.suara.com/sport/2019/01/09/152500/pensiun-peraih-emas-asian-games-ini-banting-stir-jadi-pelatih"> 
                        <img alt="Pensiun, Peraih Emas Asian Games Ini Banting Stir Jadi Pelatih" gambar="baru" height="80" 
                        src="assets/images/examples/list-square1.jpg" 
                        title="Pensiun, Peraih Emas Asian Games Ini Banting Stir Jadi Pelatih" width="80">
                    </a> 
                </div> 
                <div class="item-content"> 
                    <h4>Baca Selengkapnya</h4>
                    <h2 class="post-title"> 
                        <a class="ellipsis2" href="https://www.suara.com/sport/2019/01/09/152500/pensiun-peraih-emas-asian-games-ini-banting-stir-jadi-pelatih">
                            Pensiun, Peraih Emas Asian Games Ini Banting Stir Jadi Pelatih
                        </a> 
                    </h2> 
                </div> 
            </div>       
        </div>       
    </div> 
</div>