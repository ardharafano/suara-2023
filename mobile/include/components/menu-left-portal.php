<div class="bg-suararegional">
    <p>SUARA REGIONAL</p>

    <div class="side-suararegional">
        <a href="#">
            jakarta
        </a>
        <a href="#">
            bogor
        </a>
        <a href="#">
            bekaci
        </a>
        <a href="#">
            jabar
        </a>
        <a href="#">
            jogja
        </a>
        <a href="#">
            jateng
        </a>
        <a href="#">
            malang
        </a>
        <a href="#">
            jatim
        </a>
        <a href="#">
            bali
        </a>
        <a href="#">
            lampung
        </a>
        <a href="#">
            banten
        </a>
        <a href="#">
            surakarta
        </a>
        <a href="#">
            kaltim
        </a>
        <a href="#">
            kalbar
        </a>
        <a href="#">
            sulsel
        </a>
        <a href="#">
            sumut
        </a>
        <a href="#">
            sumbar
        </a>
        <a href="#">
            sumsel
        </a>
        <a href="#">
            batam
        </a>
        <a href="#">
            riau
        </a>
    </div>
</div>

<style>

</style>


<div class="bg-menu-left-portal">
    <div class="item">
        <ul>
            <li>
                <a href="https://arkadiacorp.com/" target="_blank">
                    <img src="assets/images/arkadia.svg" width="119" height="40" class="logo-arkadia"
                        alt="Logo arkadia" />
                </a>
            </li>
        </ul>
    </div>

    <div class="side-logo-partner">
        <a href="https://www.suara.com/" target="_blank">
            <img src="assets/images/partner/suara.svg" width="110" height="25" class="logo-partner"
                alt="Logo arkadia" />
        </a>

        <a href="https://www.matamata.com/" target="_blank">
            <img src="assets/images/partner/matamata.svg" width="110" height="25" class="logo-partner"
                alt="Logo arkadia" />
        </a>

        <a href="https://www.bolatimes.com/" target="_blank">
            <img src="assets/images/partner/bolatimes.svg" width="110" height="25" class="logo-partner"
                alt="Logo arkadia" />
        </a>

        <a href="https://www.hitekno.com/" target="_blank">
            <img src="assets/images/partner/hitekno.svg" width="110" height="25" class="logo-partner"
                alt="Logo arkadia" />
        </a>

        <a href="https://www.dewiku.com/" target="_blank">
            <img src="assets/images/partner/dewiku.svg" width="110" height="25" class="logo-partner"
                alt="Logo arkadia" />
        </a>

        <a href="https://www.mobimoto.com/" target="_blank">
            <img src="assets/images/partner/mobimoto.svg" width="110" height="25" class="logo-partner"
                alt="Logo arkadia" />
        </a>

        <a href="https://www.guideku.com/" target="_blank">
            <img src="assets/images/partner/guideku.svg" width="110" height="25" class="logo-partner"
                alt="Logo arkadia" />
        </a>

        <a href="https://www.himedik.com/" target="_blank">
            <img src="assets/images/partner/himedik.svg" width="110" height="25" class="logo-partner"
                alt="Logo arkadia" />
        </a>

        <a href="https://regional.suara.com/" target="_blank">
            <img src="assets/images/partner/regional.svg" width="110" height="25" class="logo-partner"
                alt="Logo arkadia" />
        </a>

        <a href="https://clickmov.suara.com/" target="_blank">
            <img src="assets/images/partner/clickmov.svg" width="110" height="25" class="logo-partner"
                alt="Logo arkadia" />
        </a>

        <a href="https://yoursay.id/" target="_blank">
            <img src="assets/images/partner/yoursay.svg" width="110" height="25" class="logo-partner"
                alt="Logo arkadia" />
        </a>

        <a href="https://www.serbada.com/" target="_blank">
            <img src="assets/images/partner/serbada.svg" width="110" height="25" class="logo-partner"
                alt="Logo arkadia" />
        </a>

        <a href="https://www.theindonesia.id/" target="_blank">
            <img src="assets/images/partner/theind.svg" width="110" height="25" class="logo-partner"
                alt="Logo arkadia" />
        </a>

        <a href="https://hits.suara.com/" target="_blank">
            <img src="assets/images/partner/beritahits.svg" width="110" height="25" class="logo-partner"
                alt="Logo arkadia" />
        </a>

        <a href="https://www.iklandisini.com/" target="_blank">
            <img src="assets/images/partner/iklandisini.svg" width="110" height="25" class="logo-partner"
                alt="Logo arkadia" />
        </a>

        <a href="https://nexuscreatorhub.com/" target="_blank">
            <img src="https://assets.suara.com/mobile/images/partner/nexus.svg" width="110" height="25"
                class="logo-partner" alt="Logo arkadia" />
        </a>

        <a href="https://www.suara.com/kotaksuara" target="_blank">
            <img src="https://assets.suara.com/mobile/images/partner/kotaksuara.svg" width="110" height="25"
                class="logo-partner" alt="Logo arkadia" />
        </a>

        <a href="https://liks.suara.com/" target="_blank">
            <img src="https://assets.suara.com/mobile/images/partner/liks.svg" width="110" height="25"
                class="logo-partner" alt="Logo arkadia" />
        </a>
    </div>

    <div class="side-hr"></div>
</div>