<a href="#">
    <div id="share-sosmed">
        <div>Bagikan ke Whatsapp</div>
        <img src="assets/images/icons/wa-white.svg" alt="" width="35" height="35">
    </div>
</a>

<style>
#share-sosmed {
    background-color: #5BB354;
    position: fixed;
    top: -50px;
    width: 100%;
    transition: top 0.3s;
    z-index: 999999;
    color: #fff;
    font-size: 18px;
    font-family: Poppins, sans-serif;
    padding: 5px;
    display: flex;
    justify-content: center;
    align-items: center;
}

#share-sosmed img {
    margin: 0 10px
}
</style>

<script>
window.onscroll = function() {
    scrollFunction()
};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 300) {
        document.getElementById("share-sosmed").style.top = "0";
    } else {
        document.getElementById("share-sosmed").style.top = "-50px";
    }
}

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 3500) {
        document.getElementById("share-sosmed").style.top = "-50px";
    } else {
        document.getElementById("share-sosmed").style.top = "0";
    }
}
</script>