<!-- Sosmed -->
<div class="sosmed">
    <span>
        <b>
            Ikuti Kami
        </b>
    </span>
    <ul class="list-sosmed">
        <li>
            <a href="#" target="_blank" aria-label="share facebook">
                <i class="icon icon-fb c-fb"></i>
            </a>
        </li>
        <li>
            <a href="#" target="_blank" aria-label="share twitter">
                <i class="icon icon-tw c-tw"></i>
            </a>
        </li>
        <li>
            <a href="#" target="_blank" aria-label="share youtube">
                <!-- <i class="icon icon-line c-line"></i> -->
                <i class="icon icon-yt c-yt"></i>
            </a>
        </li>
        <li>
            <a href="#" target="_blank" aria-label="share instagram">
                <!-- <i class="icon icon-wa c-wa"></i> -->
                <i class="icon icon-ig c-ig"></i>
            </a>
        </li>
        <li>
            <a href="#" target="_blank" aria-label="share rss">
                <!-- <i class="icon icon-link c-link"></i> -->
                <i class="icon icon-rss c-rss"></i>
            </a>
        </li>
        <li>
            <a href="#" target="_blank" aria-label="share twitter">
                <img src="assets/images/partner/tiktok.svg" alt="img">
            </a>
        </li>
    </ul>
    <p>
        Dapatkan informasi terkini dan terbaru yang dikirimkan langsung ke Inbox anda
    </p>
</div>


<!-- End Sosmed  -->
<div class="other-info">
    <div class="bg-gr-foot"></div>
    <ul>
        <li>
            <a href="index.php?page=redaksi">
                redaksi
            </a>
        </li>
        <li>
            <a href="index.php?page=kontak">
                kontak
            </a>
        </li>
        <li>
            <a href="index.php?page=tentang-kami">
                tentang kami
            </a>
        </li>
        <li>
            <a href="index.php?page=karir">
                karir
            </a>
        </li>
        <li>
            <a href="index.php?page=pedoman-media-siber">
                pedoman media siber
            </a>
        </li>
        <li>
            <a href="index.php?page=site-map">
                site map
            </a>
        </li>
    </ul>
</div>