<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
<script>
window.googletag = window.googletag || {
    cmd: []
};
googletag.cmd.push(function() {
    var slot_ADdfpParallax = googletag.defineSlot('/148558260/SuaraCom', [
        [320, 480],
        [300, 250],
        [336, 280]
    ], 'div-ad-parallax-top');
    slot_ADdfpParallax.setTargeting('passback', ['none']);
    slot_ADdfpParallax.setTargeting('page', ['demo']);
    slot_ADdfpParallax.setTargeting('pos', ['parallax']);
    slot_ADdfpParallax.addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
});
</script>

<div id="adds-top" class="add adds-top">
    <style>
    .wrap {
        background: #fff;
        z-index: 2;
    }

    .m-foot {
        z-index: 2;
        position: relative;
        background: #fff;
    }

    .menu-top-canal {
        margin-top: 0px;
        padding: 15px 15px 0px 15px
    }

    .adds-top {
        position: relative;
        /* z-index: -2; */
        z-index: 1;
        top: 0px;
        width: 100%;
        height: 0px;
        max-height: 480px;
        display: flex;
        align-items: center;
        background: #000;
    }

    .wrap-adds iframe {
        max-height: 480px;
        display: block;
        position: fixed;
        z-index: -1;
        top: 0px;
        margin: 0 auto;
    }

    .wrap-adds {
        width: 320px;
        margin: 0 auto;
        display: block;
    }

    .wrap-close {
        position: fixed;
        z-index: 1;
        top: 15px;
        right: 15px;
        width: 25px;
        height: 25px;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
        content: '';
        background: #fff;
    }

    #close-adds-top {
        color: #fff;
        display: none;
        cursor: pointer;
    }
    </style>

    <div class="wrap-adds">
        <div id='div-ad-parallax-top'>
            <script>
            googletag.cmd.push(function() {
                googletag.display('div-ad-parallax-top');
            });
            </script>
        </div>
    </div>
    <div class="wrap-close">
        <i class="icon-svg icon-close" id="close-adds-top"></i>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script>
    var height_b = 0;
    var width_b = 0;

    $(document).ready(function() {

        setTimeout(function() {
            console.log($("#div-ad-parallax-top").find("iframe").attr("height"));

            if ($("#div-ad-parallax-top").find("iframe").attr("height") !== undefined) {
                height_b = $("#div-ad-parallax-top").find("iframe").attr("height");
                width_b = $("#div-ad-parallax-top").find("iframe").attr("width");

                $('#adds-top').css({
                    "height": height_b + "px"
                });
                $('.distance-from-top').css({
                    "height": "0px"
                });
                $('.navbar').css({
                    "position": "relative"
                });
                $('.wrap-search').css({
                    "top": parseInt(height_b) + 40 + "px"
                });
                $('#close-adds-top').css({
                    'display': 'block'
                });
            }

        }, 1500)

    });
    </script>

    <script>
    var status_ads_parallax_top = true;
    $(document).ready(function() {
        $("#close-adds-top").click(function() {
            $('.navbar').css({
                "position": "fixed",
                "top": "0px"
            });

            $('.wrap-search').css({
                "top": "40px",
                "position": "fixed"
            });

            $('#adds-top').remove()
            $('#close-adds-top').css({
                "display": "none"
            });
            $('.distance-from-top').css({
                "height": "40px"
            });
            status_ads_parallax_top = false;
        });
    });
    </script>

    <script>
    setTimeout(function() {

        $(window).scroll(function() {

            y_scroll_pos_data = window.pageYOffset;
            top_current = parseInt(height_b) - y_scroll_pos_data;

            // console.log(top_current);
            // console.log(y_scroll_pos_data);

            if (top_current < 6 || status_ads_parallax_top == false) {

                $('.navbar').css({
                    "position": "fixed",
                    "top": "0px",
                });

                $('#close-adds-top').css({
                    'visibility': 'hidden'
                });
                $('#adds-top').css({
                    "opacity": "0",
                    "z-index": "-2",
                    "visibility": 'hidden'
                });
            }

            if (top_current > 0 && status_ads_parallax_top == true) {

                $('.navbar').css({
                    "position": "relative"
                });

                $('#adds-top').css({
                    "opacity": "1",
                    "z-index": "1",
                    "visibility": 'visible'
                });
                $('#close-adds-top').css({
                    'visibility': 'visible'
                });
                $('.distance-from-top').css({
                    "height": "0"
                });
            }

        });

    }, 1500)
    </script>

</div>