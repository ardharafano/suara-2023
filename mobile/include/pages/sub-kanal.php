<!-- Menu Top -->
<?php include('include/blocks/menu-top-canal.php'); ?>
<!-- End Menu Top -->

<div class="content">

    <!-- Headline -->
    <?php include('include/components/headline-canal.php'); ?>
    <!-- End Headline -->

    <!-- List Item Y -->
    <?php include('include/components/list-item-y.php'); ?>
    <!-- End List Item Y -->

    <!-- Video -->
    <h3 class="head-title">
        <a href="#" class="c-red">
            Video
        </a>
    </h3>
    <?php include('include/components/video.php'); ?>
    <!-- End Video -->

    <!-- Foto -->
    <?php include('include/components/foto.php'); ?>
    <!-- End Foto -->

    <!-- Terpopuler -->
    <h3 class="head-title">
        <a href="#" class="c-red">
            Terpopuler
        </a>
    </h3>
    <?php include('include/components/list-item-y-without-img.php'); ?>
    <?php include('include/components/list-item-y.php'); ?>
    <!-- End Terpopuler -->

    <!-- Load more  -->
    <a href="#" class="btn bg-darkslateblue">
        load more
    </a>
    <!--End load more  -->
    
</div>