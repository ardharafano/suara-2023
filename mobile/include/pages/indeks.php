
<?php include('include/blocks/menu-top.php'); ?>

<?php include('include/blocks/menu-top-regional.php'); ?>

<!-- Menu Top -->
<div class="menu-top-canal">

    <!-- Top  -->
    <div class="top">
        <span>
            <a href="index.php?page=indeks">
                indeks
            </a>
        </span>
        <span id="date_now_"></span>
    </div>
    <!-- End Top  -->

</div>
<!-- End Menu Top -->


<div class="content index">

    <!-- Search Form Index -->
    <hr/>
    <div class="wrap-search-indeks">
        <form action="indeks">
            <select name="category">
                <option value="terkini">
                    All
                </option>
                <option value="news">
                    News
                </option>
                <option value="bisnis">
                    Bisnis
                </option>
            </select>
            <select name="years">
                <option value="2021">
                    2021
                </option>
                <option value="2020">
                    2020
                </option>
                <option value="2019">
                    2019
                </option>
            </select>
            <button type="submit">
                Lihat
            </button>
        </form>
    </div>
    <!-- End Search Form Index -->

    <!-- Headline -->
    <?php include('include/components/headline-one.php'); ?>
    <br/>
    <!-- End Headline -->

    <!-- List Item  -->
    <?php include('include/components/list-item-y.php'); ?>
    <!-- List Item -->
    
    <!-- Pagination -->
    <?php include('include/components/pagination.php'); ?>
    <!-- End Pagination -->


</div>