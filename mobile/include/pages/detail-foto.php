<div class="content">

    <!-- Top  -->
    <div class="top-detail">
        <ul>
            <li>
                <a href="index.php?page=kanal">
                    Lokal
                </a>
            </li>
            <!-- <li>
                <a href="index.php?page=kanal" class="active">
                    Entertainment
                </a>
            </li> -->
        </ul>
        <span>Selasa, 02 Maret 2021 | 08:54 WIB</span>
    </div>
    <!-- End Top  -->

    <!-- Info  -->
    <div class="info">
        <h1>
            Telak! P2G Kritik Menteri Nadiem, Soroti Kasus Jilbab Siswi karena Viral
        </h1>
        <div class="writer">
            <span>Agung Sandy Lesmana </span>
            <span>Ria Rizki Nirmala Sari </span>
        </div>

        <div class="share-baru-header">
            <a href="#">
                <img src="assets/images/share/fb.svg" alt="">
            </a>

            <a href="#">
                <img src="assets/images/share/twitter.svg" alt="">
            </a>

            <a href="#">
                <img src="assets/images/share/line.svg" alt="">
            </a>

            <a href="#">
                <img src="assets/images/share/tele.svg" alt="">
            </a>

            <a href="#">
                <img src="assets/images/share/wa.svg" alt="">
            </a>

            <a href="#">
                <img src="assets/images/share/link.svg" alt="">
            </a>
        </div>

    </div>
    <!-- End Info  -->

    <!-- Image Cover  -->
    <div class="img-cover">
        <!-- <img src="assets/images/examples/headline.jpg" class="img-responsive" alt="cover berita" /> -->
        <!-- <picture>
            <source
                srcset="https://media.suara.com/pictures/970x544/2021/02/15/83678-ilustrasi-cara-membuat-masker-kain-suaracommichelle-illona.webp"
                type="image/webp">
            <source
                srcset="https://media.suara.com/pictures/970x544/2021/02/15/83678-ilustrasi-cara-membuat-masker-kain-suaracommichelle-illona.jpg"
                type="image/jpeg">
            <img src="https://media.suara.com/pictures/970x544/2021/02/15/83678-ilustrasi-cara-membuat-masker-kain-suaracommichelle-illona.jpg"
                width="653" height="366" alt="title image" class="">
        </picture> -->

        <!-- Image Cover  -->
        <div class="img-cover">
            <!-- <img src="assets/images/examples/headline.jpg" class="img-responsive" alt="cover berita" /> -->
            <!-- <picture>
            <source
                srcset="https://media.suara.com/pictures/970x544/2021/02/15/83678-ilustrasi-cara-membuat-masker-kain-suaracommichelle-illona.webp"
                type="image/webp">
            <source
                srcset="https://media.suara.com/pictures/970x544/2021/02/15/83678-ilustrasi-cara-membuat-masker-kain-suaracommichelle-illona.jpg"
                type="image/jpeg">
            <img src="https://media.suara.com/pictures/970x544/2021/02/15/83678-ilustrasi-cara-membuat-masker-kain-suaracommichelle-illona.jpg"
                width="653" height="366" alt="title image" class="">
        </picture>

        <div class="slide-foto-thumb">
            <div class="item-foto active">
                <a href="index.php?page=detail-foto" aria-label="detail slide foto">
                    <img src="assets/images/examples/list1.jpg" width="300px" height="250px" alt="ads" />
                </a>
            </div>
            <div class="item-foto">
                <a href="index.php?page=detail-foto" aria-label="detail slide foto">
                    <img src="assets/images/examples/list2.jpg" width="300px" height="250px" alt="ads" />
                </a>
            </div>
            <div class="item-foto">
                <a href="index.php?page=detail-foto" aria-label="detail slide foto">
                    <img src="assets/images/examples/list1.jpg" width="300px" height="250px" alt="ads" />
                </a>
            </div>
            <div class="item-foto">
                <a href="index.php?page=detail-foto" aria-label="detail slide foto">
                    <img src="assets/images/examples/list2.jpg" width="300px" height="250px" alt="ads" />
                </a>
            </div>
        </div>

        <div class="caption">
            Nadiem Makarim (Instagram/Kemdikbud.RI)
        </div>

        <hr class="hr-bottom-caption" />
    </div> -->

            <!-- SLIDE JS -->
            <div id="detail-foto">
                <div id="main-slider" class="splide">
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide">
                                <img src="assets/images/examples/headline.jpg" class="img-responsive" alt="img" width="330"
                                    height="185" />
                            </li>
                            <li class="splide__slide">
                                <img src="assets/images/examples/detail.png" class="img-responsive" alt="img" width="330"
                                    height="185" />
                            </li>
                            <li class="splide__slide">
                                <img src="assets/images/examples/headline.jpg" class="img-responsive" alt="img" width="330"
                                    height="185" />
                            </li>
                            <li class="splide__slide">
                                <img src="assets/images/examples/detail.png" class="img-responsive" alt="img" width="330"
                                    height="185" />
                            </li>
                        </ul>
                    </div>
                </div>

                <div id="thumbnails" class="splide">
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide">
                                <img src="assets/images/examples/headline.jpg" class="img-responsive" alt="img"
                                    width="160" height="92" />
                            </li>
                            <li class="splide__slide">
                                <img src="assets/images/examples/detail.png" class="img-responsive" alt="img" width="160"
                                    height="92" />
                            </li>
                            <li class="splide__slide">
                                <img src="assets/images/examples/headline.jpg" class="img-responsive" alt="img"
                                    width="160" height="92" />
                            </li>
                            <li class="splide__slide">
                                <img src="assets/images/examples/detail.png" class="img-responsive" alt="img" width="160"
                                    height="92" />
                            </li>
                        </ul>
                    </div>
                </div>

            </div>

            <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3/dist/js/splide.min.js"></script>
            <script>
            document.addEventListener('DOMContentLoaded', function() {
                var main = new Splide('#main-slider', {
                    type: 'fade',
                    rewind: true,
                    pagination: false,
                    arrows: false,
                });


                var thumbnails = new Splide('#thumbnails', {
                    fixedWidth: 160,
                    fixedHeight: 92,
                    gap: 5,
                    rewind: true,
                    pagination: false,
                    cover: true,
                    isNavigation: true,
                });


                main.sync(thumbnails);
                main.mount();
                thumbnails.mount();
            });
            </script>

            <!-- END SLIDE JS -->

            <div class="caption">
                Nadiem Makarim (Instagram/Kemdikbud.RI)
            </div>
        </div>
        <!-- End Image Cover  -->

        <a href="#!" rel="">
            <div class="banner-ads--big">
                <img src="assets/images/ads_baru/lead.svg" alt="" width="320px" height="100px">
            </div>
        </a>

        <!-- Detail content -->
        <div class="detail-content">
            <p>
                <strong>Suara.com -</strong> Perhimpunan Pendidikan dan Guru (P2G) mengapresiasi atas reaksi cepat
                Menteri
                Pendidikan dan Kebudayaan (Mendikbud) Nadiem Makarim terkait aturan di SMK Negeri 2 Padang yang
                mewajibkan
                siswi non muslim memakai jilbab. Namun di sisi lain, P2G juga menyayangkan kalau Nadiem hanya merespons
                kasus yang kebetulan tengah menjadi obrolan hangat di tengah masyarakat.
            </p>
            <p>
                Kabid Advokasi P2G, Iman Zanatul Haeri menilai kalau Nadiem tidak mengakui secara terbuka kalau fenomena
                intoleransi seperti yang terjadi di SMK Negeri 2 Padang itu juga dialami banyak siswa di daerah lain.
            </p>
            <h2>
                Kabid Advokasi P2G
            </h2>
            <p>
                "Kasus <a href="#">intoleransi</a> di sekolah yang dilakukan secara terstruktur bukanlah kasus baru,"
                kata
                Iman dalam keterangan tertulisnya
            </p>
            <iframe width="560" height="315" title="iframe title" src="https://www.youtube.com/embed/5iuvNQRWTH0"
                frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen></iframe>
            <p>
                Menurut Iman, Nadiem seharusnya mengungkap persoalan intoleransi di lingkungan sekolah.
                Persoalan intoleransi di sekolah itu dikatakannya mengandung problematika dari aspek regulasi
                struktural,
                sistematik dan birokratis.
            </p>
            <!-- <div class="wrap-ads-r">
            <a href="#" aria-label="ads">
                <img src="assets/images/ads/on-article.png" width="300px" height="250px" alt="ads" />
            </a>
        </div> -->
            <a href="#!" rel="">
                <div class="banner-ads--big">
                    <img src="assets/images/ads_baru/mr1.svg" alt="" width="336px" height="280px">
                </div>
            </a>
            <ul>
                <li>Jangan mengonsumsi makanan yang sudah tercemar air banjir</li>
                <li>Jangan mengonsumsi makanan dalam kemasan plastik, karton, atau kemasan lain yang sudah rusak karena
                    basah</li>
                <li>Lindungi makanan yang masih tersegel dengan baik. Bisa ditambah perlindungan dengan memasang perekat
                    yang lebih kuat agar tertutup rapat</li>
                <li>Makanan kaleng yang tidak rusak bisa dikonsumsi. Kaleng bekas yang bersih juga bisa digunakan lagi,
                    tapi
                    perlu dicuci dan dilabeli dengan keterangan tanggal kedaluwarsanya</li>
            </ul>
            <p class="baca-juga-new">
                <span>Baca Juga:</span>
                <a href="#">Infeksi Virus Corona Biasa Bisa Bentuk Antibodi Covid-19, Mitos atau Fakta</a>
            </p>
            <h3>
                Kabid Advokasi P2G
            </h3>
            <p>
                Di samping itu, P2G melihat adanya Peraturan Daerah (Perda) yang menjadi penyebab utama dari lahirnya
                intoleransi di sekolah. Sebab, peristiwa pemaksaan jilbab di SMKN 2
                Padang merujuk pada Instruksi Walikota Padang No 451.442/BINSOS-iii/2005.
            </p>
            <figure class="image">
                <img src="assets/images/examples/headline.jpg"
                    alt="Simpang Susun Pemalang di Jalan Tol Brebes - Pemalang, Jawa Tengah. [Dok Djoko Setijowarno]"
                    width="253" class="js_detail_img">
                <figcaption>Setya Novanto [suara.com/Kurniawan Mas'ud]</figcaption>
            </figure>
            <h4>
                Kabid Advokasi P2G
            </h4>
            <p>
                "Artinya ada peran pemerintah pusat, seperti Kemendagri dan Kemendikbud yang
                mendiamkan dan melakukan pembiaran terhadap adanya regulasi daerah bermuatan intoleransi di sekolah
                selama
                ini," tuturnya.
            </p>
        </div>
        <!-- End Detail content -->

        <!-- Pagination -->
        <a href="#" class="next-page-detail">
            Lanjut ke halaman berikutnya
        </a>
        <?php include('include/components/pagination-detail.php'); ?>
        <!-- End Pagination -->

        <!-- Ads  -->
        <!-- <div class="wrap-ads-r">
        <img src="assets/images/ads/r2.png" width="300px" height="250px" alt="ads" />
    </div> -->

        <!-- End Ads  -->

        <!-- Share Link  -->
        <!-- <div class="share-link">
        <ul>
            <li>
                <span>
                    Share link:
                </span>
            </li>
        </ul> -->

        <div class="share-baru-bottom">
            <a href="#">
                <img src="assets/images/share/fb.svg" alt="">
            </a>

            <a href="#">
                <img src="assets/images/share/twitter.svg" alt="">
            </a>

            <a href="#">
                <img src="assets/images/share/line.svg" alt="">
            </a>

            <a href="#">
                <img src="assets/images/share/tele.svg" alt="">
            </a>

            <a href="#">
                <img src="assets/images/share/wa.svg" alt="">
            </a>

            <a href="#">
                <img src="assets/images/share/link.svg" alt="">
            </a>
        </div>
        <!-- </div> -->
        <!-- End Share Link  -->

        <a href="#!" rel="">
            <div class="banner-ads--big">
                <img src="assets/images/ads_baru/mr2.svg" alt="" width="336px" height="280px">
            </div>
        </a>

        <!-- Baca Juga -->
        <h2 class="head-title mt20">
            <a href="#" class="c-red">
                Berita Terkait
            </a>
        </h2>
        <?php include('include/components/list-item-y.php'); ?>
        <!-- End Juga -->

        <!-- Tag -->
        <h3 class="head-title" id="tag-detail">
            <a href="#" class="c-red">
                Tag
            </a>
        </h3>
        <?php include('include/components/tag.php'); ?>
        <!-- End Tag -->

        <!-- Komentar  -->
        <h3 class="head-title">
            <a href="#" class="c-red">
                Komentar
            </a>
        </h3>
        <!-- ================== Give Plugin in here ==================== -->
        <div class="wrap-comment">
            <div id="fb-root"></div>
            <script async defer crossorigin="anonymous"
                src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v8.0&appId=453030488369496&autoLogAppEvents=1"
                nonce="6NUqNhea"></script>
            <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator"
                data-numposts="2" data-width=""></div>
        </div>
        <!-- ================== End Give Plugin in here ==================== -->
        <!-- End Komentar  -->


        <h3 class="head-title">
            <a href="#" class="c-red">
                Terpopuler
            </a>
        </h3>
        <?php include('include/components/list-item-y-without-img.php'); ?>

        <a href="#!" rel="">
            <div class="banner-ads--big">
                <img src="assets/images/ads_baru/mr3.svg" alt="" width="336px" height="280px">
            </div>
        </a>

        <h3 class="head-title mt20">
            <a href="#" class="c-red">
                News
            </a>
        </h3>
        <?php include('include/components/headline.php'); ?>

        <!-- Video -->
        <!-- <?php include('include/components/video-x-scroll.php'); ?> -->


        <!-- <h3 class="head-title">
        <a href="#" class="c-red">
            Video
        </a>
    </h3>
    <?php include('include/components/video.php'); ?> -->
        <!-- End Video  -->
        <!-- <?php include('include/components/foto.php'); ?> -->

        <h3 class="head-title">
            <a href="#" class="c-red">
                Pilihan
            </a>
        </h3>
        <?php include('include/components/list-item-y.php'); ?>

        <a href="#!" rel="">
            <div class="banner-ads--big">
                <img src="assets/images/ads_baru/mr4.svg" alt="" width="336px" height="280px">
            </div>
        </a>

        <!-- Terpopuler -->
        <h3 class="head-title mt20">
            <a href="#" class="c-red">
                Terkini
            </a>
        </h3>
        <div class="headline-one">
            <div class="thumb-img">
                <a href="<?php echo $url.'page=detail'; ?>">
                    <img src="assets/images/examples/headline.jpg" width="330px" height="185px" class="img-responsive"
                        alt="berita terkini" />
                </a>
            </div>
            <h2>
                <a href="<?php echo $url.'page=detail'; ?>">
                    Rektor USU Terpilih Muryanto Amin Akan Dilantik Besok
                </a>
            </h2>
            <span>
                <a href="<?php echo $url.'page=kanal'; ?>">
                    news
                </a>
            </span>
        </div>
        <?php include('include/components/list-item-y.php'); ?>

        <a href="#!" rel="">
            <div class="banner-ads--big">
                <img src="assets/images/ads_baru/mr5.svg" alt="" width="336px" height="280px">
            </div>
        </a>

        <?php include('include/components/list-item-y.php'); ?>

        <a href="#!" rel="">
            <div class="banner-ads--big">
                <img src="assets/images/ads_baru/mr6.svg" alt="" width="336px" height="280px">
            </div>
        </a>

        <!-- Load more  -->
        <a href="#" class="btn bg-darkslateblue">
            Load More
        </a>
        <!--End load more  -->

    </div>