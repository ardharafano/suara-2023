
<?php include('include/blocks/menu-top.php'); ?>

<?php include('include/blocks/menu-top-regional.php'); ?>

<!-- Menu Top -->
<div class="menu-top-canal">

    <!-- Top  -->
    <div class="top">
        <span>
            <a href="index.php?page=redaksi">
                redaksi
            </a>
        </span>
        <span id="date_now_"></span>
    </div>
    <!-- End Top  -->

</div>
<!-- End Menu Top -->

<div class="content statik">
    
    <h2>
        Pemimpin Redaksi
    </h2>
    <ul>
        <li>Suwarjono</li>
    </ul>
    <hr/>

    <h2>
        Redaktur Pelaksana
    </h2>
    <ul>
        <li>Arsito Hidayatullah</li>
        <li>Madinah</li>
    </ul>
    <hr/>

    <h2>
        Asisten Redaktur Pelaksana
    </h2>
    <ul>
        <li>Reza Gunadha</li>
        <li>Ririn Indriani</li>
    </ul>
    <hr/>

    <h2>
        Tim Editor
    </h2>
    <ul>
        <li>Reky Kalumata</li>
        <li>Syaiful Rachman</li>
        <li>Liberty Jemadu</li>
        <li>Yazir Farouk</li>
        <li>Rizki Nurmansyah</li>
        <li>Rully Fauzi</li>
        <li>Dythia Novianty</li>
        <li>Ferry Noviandi</li>
        <li>Vania Rossa</li>
        <li>RR Ukirsari Manggalani</li>
        <li>Iwan Supriyatna</li>
        <li>Bangun Santoso</li>
        <li>Muhammad Reza Sulaiman</li>
        <li>Dwi Bowo Raharjo</li>
        <li>Chandra Iswinarno</li>
    </ul>
    <hr/>

    <h2>
        Head of Products
    </h2>
    <ul>
        <li>Abdurrahman Rauf</li>
    </ul>
    <hr/>

    <h2>
        Tim Video Kreatif
    </h2>
    <ul>
        <li>Iramdani (Produser)</li>
        <li>Dendi Afriyan (Asisten Produser)</li>
        <li>Rinaldi Aban (Asisten Produser)</li>
        <li>Ade Dianti</li>
        <li>Heriyanto</li>
        <li>Peter Johannes Rotti</li>
        <li>Suciati</li>
        <li>Andika Bagus</li>
        <li>Adit Rianto Saputro</li>
        <li>Moch Iqbal Maulana Syarief</li>
    </ul>
    <hr/>

    <h2>
        Tim Foto
    </h2>
    <ul>
        <li>Oke Atmaja (Asisten Redaktur)</li>
        <li>Angga Budianto</li>
        <li>Alfian Winanto</li>
    </ul>
    <hr/>

    <h2>
        Koordinator Liputan Daerah/Partners
    </h2>
    <ul>
        <li>Pebriansyah Ariefana</li>
    </ul>
    <hr/>

    <h2>
        Koordinator Liputan Jakarta
    </h2>
    <ul>
        <li>Agung Sandy Lesmana</li>
    </ul>
    <hr/>

    <h2>
        Reporter
    </h2>
    <ul> 
        <li>Adie Prasetyo Nugraha</li>
        <li>Agung Sandy Lesmana</li>
        <li>Arief Apriadi</li>
        <li>Dinda Rachmawati</li>
        <li>Erick Tanjung</li>
        <li>Ismail</li>
        <li>Manuel Jeghesta</li>
        <li>Ria Rizki Nirmala Sari</li>
        <li>Risna Halidi</li>
        <li>Sumarni</li>
        <li>Ummy Hadyah Saleh</li>
        <li>Welly Hidayat</li>
        <li>Yosea Arga Pramudita</li>
        <li>Muhamad Yasir</li>
        <li>Achmad Fauzi</li>
        <li>Tivan Rahmat</li>
        <li>Stephanus Aranditio</li>
        <li>Dini Afrianti Efendi</li>
        <li>Fakhri Fuadi Muflih</li>
        <li>Mohammad Fadil</li>
        <li>Novian Ardiansyah</li>
        <li>Evi Ariska</li>
        <li>Yuliani</li>
        <li>Herwanto</li>
    </ul>
    <hr/>

    <h2>
        Content Writer
    </h2>
    <ul>
        <li>Fabiola Febrinastri (Copy Writer)</li>
        <li>Chyntia Sami Bhayangkara</li>
        <li>Lintang Siltya Utami</li>
        <li>Raden Roro Dian Kusuma Hapsari</li>
    </ul>

</div>