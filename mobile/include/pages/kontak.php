
<?php include('include/blocks/menu-top.php'); ?>

<?php include('include/blocks/menu-top-regional.php'); ?>

<!-- Menu Top -->
<div class="menu-top-canal">

    <!-- Top  -->
    <div class="top">
        <span>
            <a href="index.php?page=kontak">
                kontak
            </a>
        </span>
        <span id="date_now_"></span>
    </div>
    <!-- End Top  -->

</div>
<!-- End Menu Top -->

<div class="content statik">

    <div class="mapouter">
        <div class="gmap_canvas">
            <iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=arkadia%20digital%20media&t=&z=15&ie=UTF8&iwloc=&output=embed" 
                frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
            </iframe>
        </div>
    </div>

    <hr/>
    <br/>
    <h2>PT. Arkadia Media Nusantara</h2>
    <ul>
        <li>Jln Mega Kuningan Timur Blok C6 Kav.9</li>
        <li>Kawasan Mega Kuningan,</li>
        <li>Jakarta 12950, Indonesia</li>
        <li>Tel: 021 - 50101239</li>
        <li>Fax &nbsp;: 021 - 724 1887</li>
        <li>Email &nbsp;: redaksi@suara.com</li>
        <li>Iklan &nbsp;: sales@suara.com </li>
        <li>Info Kirim Tulisan&nbsp;&nbsp;: yoursay@suara.com</li>
    </ul>

</div>