<div class="menu-left">
    <!-- Head -->
    <div class="m-head">
        <ul>
            <li>
                <a href="index.php" aria-label="logo suara">
                    <img src="assets/images/logo.svg" width="150px" height="18px" class="img-logo" alt="Logo suara.com" />
                </a>
                <i class="icon-svg icon-close" id="close-menu"></i>
            </li>
        </ul>
        <ul class="account">
            <li>
                <a href="#" aria-label="user">
                    <i class="icon-svg icon-user"></i>
                </a>
            </li>
            <li>
                <a href="http://arkadia.me" class="open-login">
                    Login
                </a>
            </li>
            <li>
                <a href="http://arkadia.me" class="open-regis">
                    Daftar
                </a>
            </li>
        </ul>
    </div>
    <!-- End Head -->

    <!-- Body -->
    <div class="m-body">
        <?php include('include/components/list-kanal-menu.php'); ?>

        <!-- <div class="other-canal">
            <ul>
                <li>
                    <a href="#">
                        Cek fakta
                    </a>
                </li>
                <li>
                    <a href="#">
                        Infografis
                    </a>
                </li>
                <li>
                    <a href="#">
                        Yoursay
                    </a>
                </li>
                <li>
                    <a href="index.php?page=indeks">
                        indeks
                    </a>
                </li>
            </ul>
        </div> -->
    </div>
    <!-- End Body -->

    <!-- Foot -->
    <div class="m-foot">

        <div class="other-media">

            <!-- Arkadia group -->
            <!-- <?php //include('include/components/list-portal-arkadia-group.php'); ?> -->
            <?php include('include/components/menu-left-portal.php'); ?> 
            <!-- End Arkadia group -->

            <div class="item">
                <ul>
                    <li>
                        <a href="#">
                            <b>
                                Network
                            </b>
                        </a>
                    </li>
                </ul>
                <ul>
                    <li>
                        <a href="#" target="_blank">
                            bbc.com
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            abc.net.au
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            dw.com
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            fajarsatu
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            blokbojonegoro
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            bloktuban
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            telisik
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            berita manado
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            sumselupdate
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            inibalikpapan
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            digtara
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            fornews
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            itulah
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            the asian parent
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            seuramoeaceh.com
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            idpost.co.id
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            blokbojonegoro
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            bloktuban
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            telisik
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            berita manado
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            sumselupdate
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            inibalikpapan
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            digtara
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            blokbojonegoro
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            bloktuban
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            telisik
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            berita manado
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            sumselupdate
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            inibalikpapan
                        </a>
                    </li>
                    <li>
                        <a href="#" target="_blank">
                            digtara
                        </a>
                    </li>
                </ul>
            </div>

        </div>

        <!-- Footer Bottom -->
        <?php include('include/components/footer-bottom.php'); ?>
        <!-- End Footer Bottom -->

    </div>
    <!-- End Foot -->

</div>


<!-- Menu Auth -->
<?php include('include/blocks/menu-auth.php'); ?>
<!-- End Menu Auth -->