<!-- ===== Login ==== -->
<div class="menu-auth login">

    <!-- Head -->
    <div class="m-head">
        <ul>
            <li>
                <a href="index.php" aria-label="logo suara">
                    <img src="assets/images/logo.svg" width="150px" height="18px" class="img-logo" alt="Logo suara.com" />
                </a>
                <i class="icon-svg icon-close" id="close-menu-auth-login"></i>
            </li>
        </ul>
    </div>
    <!-- End Head -->

    <!-- Body -->
    <!-- <div class="m-body">
        <h2>LOGIN</h2>
        <div class="wrap-form">
            
            <form action="#" method="POST">
                <div class="input-group">
                    <input type="text" name="username" placeholder="Username" />
                </div>
                <div class="input-group">
                    <input type="password" name="password" placeholder="Password" />
                </div>
                <ul>
                    <li>
                        <div class="wrap-checkbox">
                            <input type="checkbox" name="remember-me" />
                            <label>Ingatkan saya</label>
                        </div>
                        <a href="#">
                            Lupa Password
                        </a>
                    </li>
                </ul>
                <button type="submit" class="btn bg-darkslateblue">
                    Login
                </button>
            </form>

            <div class="auth-sosmed">
                <a href="#" class="btn btn-sosmed bg-steelblue">
                    <span>
                        <i class="icon icon-fb"></i>
                    </span>
                    <span>
                        Daftar/Masuk Lewat Facebook
                    </span>
                </a>
                <a href="#" class="btn btn-sosmed bg-tomato">
                    <span>
                        <i class="icon icon-google"></i>
                    </span>
                    <span>
                        Daftar/Masuk Lewat Google
                    </span>
                </a>
            </div>
            <p class="account_">
                Belum memiliki akun suara.com?
                <br/>
                <a href="javascript:;" class="open-regis">
                    Daftar Akun
                </a>
            </p>

        </div>
    </div> -->
    <!-- End Body -->

</div>
<!-- ===== End Login ====== -->


<!-- ===== Registration ===== -->
<div class="menu-auth regis">

    <!-- Head -->
    <div class="m-head">
        <ul>
            <li>
                <a href="index.php" aria-label="logo suara">
                    <img src="assets/images/logo.svg" width="150px" height="18px" class="img-logo" alt="Logo suara.com" />
                </a>
                <i class="icon-svg icon-close" id="close-menu-auth-regis"></i>
            </li>
        </ul>
    </div>
    <!-- End Head -->

    <!-- Body -->
    <!-- <div class="m-body">
        <h2>DAFTAR</h2>
        <div class="wrap-form">
            
            <form action="#" method="POST">
                <div class="input-group">
                    <input type="email" name="email" placeholder="Email" />
                </div>
                <div class="input-group">
                    <input type="text" name="username" placeholder="Username" />
                </div>
                <div class="input-group">
                    <input type="text" name="name" placeholder="Nama Lengkap" />
                </div>
                <div class="input-group">
                    <input type="text" name="password1" placeholder="Password" />
                </div>
                <div class="input-group">
                    <input type="text" name="password2" placeholder="Konfirmasi Password" />
                </div>
                <ul>
                    <li>
                        <div class="wrap-checkbox">
                            <input type="checkbox" name="aggre" />
                            <label>Saya setuju dengan Syarat & Ketentuan</label>
                        </div>
                    </li>
                </ul>
                <button type="submit" class="btn bg-darkslateblue">
                    Daftar
                </button>
                <button type="reset" class="btn bg-silver">
                    Batal
                </button>
            </form>

            <p class="account_">
                Anda sudah memiliki akun suara.com?
                <br/>
                <a href="javascript:;" class="open-login">
                    Klik Masuk
                </a>
            </p>

        </div>
    </div> -->
    <!-- End Body -->

</div>
<!-- ====== End Registration ====== -->