<div class="navbar">

    <!-- Navbar Top -->
    <ul>
        <li>
            <i class="icon-svg icon-menu" id="open-menu"></i>
        </li>
        <li>
            <a href="index.php" aria-label="logo suara">
                <img src="assets/images/logo.svg" width="150px" height="18px" class="img-logo" alt="Logo suara.com" />
            </a>
        </li>
        <li>
            <i class="icon-svg icon-search" id="open-search"></i>
            <a href="#" aria-label="account">
                <i class="icon-svg icon-user"></i>
            </a>
        </li>
    </ul>
    <!-- End Navbar Top -->


    <!-- Search -->
    <div class="wrap-search">
        <form action="search" method="GET">
            <input type="text" placeholder="Cari berita.." name="q" />
            <button type="submit">Cari</button>
        </form>
    </div>
    <!-- End Search -->
    
</div>
<div class="distance-from-top"></div>