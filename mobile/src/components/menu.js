// import $ from 'jquery';

/* Open Menu */
// $('#open-menu').on("click", function() {
//     $(".menu-left").animate({
//         width: "toggle"
//     }, 200);
// })
document.getElementById('open-menu').onclick = function() {
    var menuLeft = document.querySelector('.menu-left');
    var openedMenuLeft = `
        -webkit-animation: expand-open-menu-left 0.1s;
        display: block;
    `;
    menuLeft.style = `${openedMenuLeft}`;
}

/* Close Menu */
// $('#close-menu').on("click", function() {
//     $(".menu-left").animate({
//         width: "toggle"
//     }, 200);
// })
document.getElementById('close-menu').onclick = function() {
    var menuLeft = document.querySelector('.menu-left');
    var openedMenuLeft = `
        -webkit-animation: expand-close-menu-left 5s;
        display: none;
    `;
    menuLeft.style = `${openedMenuLeft}`;
}

/* Login */
// $('.open-login').on("click", function() {
//     $(".menu-auth.login").animate({
//         width: "toggle"
//     }, 200);
//     $(".menu-auth.regis").css({
//         display: 'none'
//     });
// })
// $('#close-menu-auth-login').on("click", function() {
//     $(".menu-auth.login").animate({
//         width: "toggle"
//     }, 100);
// // })
// document.getElementsByClassName('open-login').onclick = function() {
// document.addEventListener('click', function (e) {
// 	if (!e.target.matches('.open-login')) return;
//     var menuAuth = document.querySelector('.login');
//     var openedMenuLeft = `
//         -webkit-animation: expand-open-menu-left 0.1s;
//         display: block;
//     `;
//     menuAuth.style = `${openedMenuLeft}`;
// });
// document.getElementById('close-menu-auth-login').onclick = function() {
//     var menuAuth = document.querySelector('.login');
//     var openedMenuLeft = `
//         -webkit-animation: expand-close-menu-left 5s;
//         display: none;
//     `;
//     menuAuth.style = `${openedMenuLeft}`;
// }



/* Regis */
// $('.open-regis').on("click", function() {
//     $(".menu-auth.regis").animate({
//         width: "toggle"
//     }, 100);
//     $(".menu-auth.login").css({
//         display: 'none'
//     });
// })
// $('#close-menu-auth-regis').on("click", function() {
//     $(".menu-auth.regis").animate({
//         width: "toggle"
//     }, 200);
// })
// document.addEventListener('click', function (e) {
// 	if (!e.target.matches('.open-regis')) return;
//     var menuAuth = document.querySelector('.regis');
//     var menuAuthLogin = document.querySelector('.login');
//     var openedMenuLeft = `
//         -webkit-animation: expand-open-menu-left 0.1s;
//         display: block;
//     `;
//     menuAuth.style = `${openedMenuLeft}`;
//     menuAuthLogin.style.display = 'none';
// });
// document.getElementById('close-menu-auth-regis').onclick = function() {
//     var menuAuth = document.querySelector('.regis');
//     var openedMenuLeft = `
//         -webkit-animation: expand-close-menu-left 5s;
//         display: none;
//     `;
//     menuAuth.style = `${openedMenuLeft}`;
// }

/* Open Sub */
// $('.open-sub').on("click", function(e) {

//     if($(this).find("~ .sub-menu").hasClass('has-opened')) {
//         $(this).css({
//             'transform': 'rotate(0deg)',
//             'justify-content': 'flex-end'
//         })
//         $(this).find("~ .sub-menu").removeClass('has-opened');
//     } else {
//         $(this).css({
//             'transform': 'rotate(180deg)',
//             'justify-content': 'flex-start'
//         })
//         $(this).find("~ .sub-menu").addClass('has-opened');
//     }
//     $(this).find("~ .sub-menu").slideToggle();

// })

document.addEventListener('click', function (e) {
	if (!e.target.matches('.open-sub')) return;
    var thisnode = e.target.parentNode;
    var thisMe = thisnode.querySelector(".sub-menu");

    if(thisMe.classList.contains("opened")) {
        var openedIcon = `
            transform: rotate(0deg);
            justify-content: flex-end;
        `;
        var openedUl = `
            -webkit-animation: expand-close-sub-menu 0.1s;
            display: none;
        `;
        e.target.style = `${openedIcon}`;
        thisMe.style = `${openedUl}`;
        thisMe.classList.remove("opened");
    } else {
        var openedIcon = `
            transform: rotate(180deg);
            justify-content: flex-start;
        `;
        var openedUl = `
            -webkit-animation: expand-open-sub-menu 0.1s;
            display: block;
        `;
        e.target.style = `${openedIcon}`;
        thisMe.style = `${openedUl}`;
        thisMe.classList.add("opened");
    }
})


