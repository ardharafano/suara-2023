// import $ from 'jquery';

// $('#open-search').on("click", () => {
//     $('.wrap-search').slideToggle();
// })


// const css = styleBlock => {
//     const className = someHash(styleBlock);
//     const styleEl = document.createElement('style');
//     styleEl.textContent = `
//       .${className} {
//         ${styleBlock}
//       }
//     `;
//     document.head.appendChild(styleEl);
//     return className;
// };
// //   const className = css(`
// //     color: red;
// //     padding: 20px;
// //   `); // 'c23j4'


document.getElementById('open-search').onclick = function() {
    var wrapSearch = document.getElementsByClassName('wrap-search')[0];

    if(wrapSearch.style.display === '' || wrapSearch.style.display === 'none') {
        var wrapSearchStyle = `
            -webkit-animation: expand-open-search 0.1s;
            display: block;
        `;
        wrapSearch.style = `${wrapSearchStyle}`;

    } else {
        var wrapSearchStyle = `
            -webkit-animation: expand-close-search 0.1s;
            display: none;
        `;
        wrapSearch.style = `${wrapSearchStyle}`;
    }
}