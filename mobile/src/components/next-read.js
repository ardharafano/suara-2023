if(document.getElementById("next-article")){
    var next_read = document.getElementById("next-article");
    var element_position = document.getElementById("tag-detail").offsetTop;
    var nav = document.querySelector(".navbar").clientHeight;
    var limit_nav = document.querySelector(".navbar");
    
    window.addEventListener("scroll", (event) => {
        
        var y_scroll_pos = window.pageYOffset;

        if(y_scroll_pos > (element_position - 200) && limit_nav.style.position == 'fixed') {
            next_read.style.display = 'block';
        } else {
            next_read.style.display = 'none';
        }


    });

    
    document.querySelector('.close-next').addEventListener("click", (e) => {
        next_read.style.display = 'none';
        next_read.remove();
    })
}