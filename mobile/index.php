<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Suara.com - Berita Hari ini, Berita Terbaru dan Terkini</title>

    <!-- Meta -->
    <meta name="description" content="Portal berita yang menyajikan informasi terhangat baik peristiwa politik, entertainment dan lain lain">
    <meta name="keywords" content="Berita, Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga">
    <!-- End Meta  -->

    <!-- Favicon -->
    <link rel="Shortcut icon" href="https://assets.suara.com/mobile/images/new-images/favicon.png">
    <link rel="apple-touch-icon" href="https://assets.suara.com/mobile/images/new-images/suara-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://assets.suara.com/mobile/images/new-images/suara-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://assets.suara.com/mobile/images/new-images/suara-icon-114x114.png">
    <link rel="icon" type="image/png" href="https://assets.suara.com/mobile/images/new-images/suara-icon-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="https://assets.suara.com/mobile/images/new-images/suara-icon-512x512.png" sizes="512x512">
    <!-- End Favicon -->

    <!-- Font external -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Poppins:400,600,700&amp;font-display=swap" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&display=swap">
    <!-- End Font external -->

    <!-- My Style -->
    <link rel="stylesheet" href="assets/css/main.css?<?= time() ?>" />
    <link rel="stylesheet" href="assets/css/splide.min.css?<?= time() ?>">
    <!-- End My Style -->

</head>
<body>

    <!-- Ads paralax top  -->
    <?php include("include/components/adds-paralax-top.php"); ?>
    <!-- End Ads paralax top  -->

    <!-- Navbar -->
    <?php include('include/blocks/navbar.php'); ?>
    <!-- End Navbar -->

    <!-- Menu Left -->
    <?php include('include/blocks/menu-left.php'); ?>
    <!-- End Menu Left -->

    <?php
        if(isset($_GET['page'])){ 
            if($_GET['page'] == 'detail') { ?>
            <!-- Baca selanjutnya -->
            <?php include('include/components/baca-selanjutnya-detail-page.php'); ?>
            <?php include('include/components/share-sosmed.php'); ?>
            <!-- End Baca selanjutnya -->
        <?php }
        }
    ?>

    <div class="wrap">
        <?php
            if(isset($_GET['page'])){ 
                $url = 'index.php?';
                include("include/pages/".$_GET['page'].".php");
            }else{
                $url = 'index.php?';
                include("include/pages/home.php");
            }
        ?>
    </div>

    <?php include('include/blocks/footer.php'); ?>

    <!-- Ads bottom fixed -->
    <?php include("include/components/adds-bottom-fixed.php"); ?>
    <!-- End Ads bottom fixed -->

    <!-- Custom Js -->
    <script src="assets/js/main.js?<?= time() ?>"></script>
    <!-- <script src="assets/js/splide.min.js"></script> -->
    <!-- End Custom Js -->

</body>
</html>