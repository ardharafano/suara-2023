<!doctype html>
<html amp lang="id-ID">

<head>
    <title>Richard Lee Bebas dari Status Tersangka, Begini Kronologi Perseteruannya dengan Kartika Putri - Indotnesia
    </title>
    <meta charset="utf-8">
    <meta name="description" content="Kasus perseteruan Richard Lee dan Kartika Putri telah berlangsung sejak 2021." />
    <meta name="keywords" content="kartika putri, richard lee, kronologi, krim wajah" />
    <meta name="news_keywords" content="kartika putri, richard lee, kronologi, krim wajah" />
    <meta name="amp-google-client-id-api" content="googleanalytics">
    <meta name="viewport" content="width=device-width">
    <meta name="google-site-verification" content="xgzlpTB4aNO3Ni2ORbt0wUhTYGUXYWJhNQcfVW9Ojd0" />
    <meta name="googlebot-news" content="index,follow" />
    <meta name="googlebot" content="index,follow" />
    <meta name="robots" content="index,follow,max-image-preview:large">
    <meta name="language" content="id" />
    <meta name="geo.country" content="id" />
    <meta http-equiv="content-language" content="In-Id" />
    <meta name="geo.placename" content="Indonesia" />
    <meta name="theme-color" content="#ff0000">
    <meta property="fb:app_id" content="802054763141639" />
    <meta property="fb:pages" content="636794109715023" />
    <meta property="og:title"
        content="Richard Lee Bebas dari Status Tersangka, Begini Kronologi Perseteruannya dengan Kartika Putri">
    <meta property="og:description"
        content="Kasus perseteruan Richard Lee dan Kartika Putri telah berlangsung sejak 2021.">
    <meta property="og:type" content="article" />
    <meta property="og:url"
        content="https://indotnesia.suara.com/read/2022/11/17/125634/richard-lee-bebas-dari-status-tersangka-begini-kronologi-perseteruannya-dengan-kartika-putri">
    <meta property="og:image"
        content="https://media.suara.com/suara-partners/indotnesia/thumbs/970x545/2022/11/17/1-screenshot-20221117-125426.png">
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="970">
    <meta property="og:image:height" content="545">
    <meta property="og:site_name" content="suara.com">
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="Suara.com" />
    <meta name="twitter:title"
        content="Richard Lee Bebas dari Status Tersangka, Begini Kronologi Perseteruannya dengan Kartika Putri - Indotnesia">
    <meta name="twitter:description"
        content="Kasus perseteruan Richard Lee dan Kartika Putri telah berlangsung sejak 2021.">
    <meta name="twitter:image"
        content="https://media.suara.com/suara-partners/indotnesia/thumbs/970x545/2022/11/17/1-screenshot-20221117-125426.png">
    <meta name="twitter:image:src"
        content="https://media.suara.com/suara-partners/indotnesia/thumbs/970x545/2022/11/17/1-screenshot-20221117-125426.png">
    <link rel="image_src"
        href="https://media.suara.com/suara-partners/indotnesia/thumbs/970x545/2022/11/17/1-screenshot-20221117-125426.png" />
    <link rel="canonical"
        href="https://indotnesia.suara.com/read/2022/11/17/125634/richard-lee-bebas-dari-status-tersangka-begini-kronologi-perseteruannya-dengan-kartika-putri" />

    <!-- Favicon -->
    <link rel="Shortcut icon" href="https://assets.suara.com/mobile/images/new-images/favicon.png">
    <link rel="apple-touch-icon" href="https://assets.suara.com/mobile/images/new-images/suara-icon.png">
    <link rel="apple-touch-icon" sizes="72x72"
        href="https://assets.suara.com/mobile/images/new-images/suara-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114"
        href="https://assets.suara.com/mobile/images/new-images/suara-icon-114x114.png">
    <link rel="icon" type="image/png" href="https://assets.suara.com/mobile/images/new-images/suara-icon-192x192.png"
        sizes="192x192">
    <link rel="icon" type="image/png" href="https://assets.suara.com/mobile/images/new-images/suara-icon-512x512.png"
        sizes="512x512">

    <link rel="preconnect" href="https://assets.suara.com/">
    <link rel="preconnect" href="https://media.suara.com/">
    <link rel="preload" href="https://cdn.ampproject.org/v0.js" as="script">
    <link rel="preload" href="https://assets.suara.com/mobile/fonts/pxiEyp8kv8JHgFVrJJfecnFHGPc.woff2" as="font"
        type="font/woff2" crossorigin>
    <link rel="preload" href="https://assets.suara.com/mobile/fonts/pxiByp8kv8JHgFVrLCz7Z1xlFd2JQEk.woff2" as="font"
        type="font/woff2" crossorigin>
    <link rel="preload"
        href="https://media.suara.com/suara-partners/indotnesia/thumbs/653x367/2022/11/17/1-screenshot-20221117-125426.png"
        as="image">

    <script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "WebPage",
        "headline": "Richard Lee Bebas dari Status Tersangka, Begini Kronologi Perseteruannya dengan Kartika Putri",
        "url": "https://indotnesia.suara.com/read/2022/11/17/125634/richard-lee-bebas-dari-status-tersangka-begini-kronologi-perseteruannya-dengan-kartika-putri",
        "datePublished": "2022-11-17T12:56:34+07:00",
        "image": "https://media.suara.com/suara-partners/indotnesia/thumbs/970x545/2022/11/17/1-screenshot-20221117-125426.png",
        "thumbnailUrl": "https://media.suara.com/suara-partners/indotnesia/thumbs/90x90/2022/11/17/1-screenshot-20221117-125426.png"
    }
    </script>
    <script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "NewsArticle",
        "headline": "Richard Lee Bebas dari Status Tersangka, Begini Kronologi Perseteruannya dengan Kartika Putri",
        "datePublished": "2022-11-17T12:56:34+07:00",
        "dateModified": "2022-11-17T12:56:34+07:00",
        "mainEntityOfPage": {
            "@type": "WebPage",
            "@id": "https://indotnesia.suara.com/read/2022/11/17/125634/richard-lee-bebas-dari-status-tersangka-begini-kronologi-perseteruannya-dengan-kartika-putri"
        },
        "description": "Kasus perseteruan Richard Lee dan Kartika Putri telah berlangsung sejak 2021.",
        "image": {
            "@type": "ImageObject",
            "url": "https://media.suara.com/suara-partners/indotnesia/thumbs/970x545/2022/11/17/1-screenshot-20221117-125426.png",
            "width": 970,
            "height": 545
        },
        "author": {
            "url": "https://indotnesia.suara.com/indeks",
            "@type": "Person",
            "name": "Asy Syaffa Nada A"
        },
        "publisher": {
            "@type": "Organization",
            "name": "Suara Indotnesia",
            "logo": {
                "@type": "ImageObject",
                "url": "https://assets.suara.com/mitra/desktop/images/../../general/indotnesia-logo.svg",
                "width": 298,
                "height": 50
            }
        }
    }
    </script>
    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement": [{
            "@type": "ListItem",
            "position": 1,
            "name": "indotnesia",
            "item": "https://indotnesia.suara.com/"
        }, {
            "@type": "ListItem",
            "position": 2,
            "name": "aktual",
            "item": "https://indotnesia.suara.com/aktual"
        }]
    }
    </script>
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
    <script async custom-element="amp-social-share" src="https://cdn.ampproject.org/v0/amp-social-share-0.1.js">
    </script>
    <script async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>
    <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
    <script async custom-element="amp-instagram" src="https://cdn.ampproject.org/v0/amp-instagram-0.1.js"></script>
    <script async custom-element="amp-twitter" src="https://cdn.ampproject.org/v0/amp-twitter-0.1.js"></script>
    <script async custom-element="amp-facebook" src="https://cdn.ampproject.org/v0/amp-facebook-0.1.js"></script>
    <script async custom-element="amp-dailymotion" src="https://cdn.ampproject.org/v0/amp-dailymotion-0.1.js"></script>
    <script async custom-element="amp-ad" src="https://cdn.ampproject.org/v0/amp-ad-0.1.js"></script>
    <script async custom-element="amp-sticky-ad" src="https://cdn.ampproject.org/v0/amp-sticky-ad-1.0.js"></script>

    <style amp-custom>
    .blog-header,
    .site-header {
        margin: 0 auto
    }

    .lanjut,
    .post .bottom-teaser,
    .post .post-content,
    .post .post-content .article a,
    .post .post-content h1,
    .post .post-content h2,
    .post .post-content h3,
    .post .post-content li,
    .post .post-content p,
    body,
    div.new_bacajuga a,
    p {
        font-family: Arial, Helvetica, sans-serif
    }

    .authors,
    .bacajuga,
    .content article .post-meta h2,
    .content article .post-title,
    .post .post-meta,
    .post .post-meta-text {
        font-family: Poppins, sans-serif
    }

    .sidebar .sidebar-other-media .row .col:first-child,
    pre>code {
        padding-left: 0
    }

    @font-face {
        font-family: Poppins;
        font-style: normal;
        font-weight: 400;
        font-display: optional;
        src: url("https://assets.suara.com/mobile/fonts/pxiEyp8kv8JHgFVrJJfecnFHGPc.woff2") format("woff2");
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD
    }

    @font-face {
        font-family: Poppins;
        font-style: normal;
        font-weight: 700;
        font-display: optional;
        src: url("https://assets.suara.com/mobile/fonts/pxiByp8kv8JHgFVrLCz7Z1xlFd2JQEk.woff2") format("woff2");
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD
    }

    *,
    .pagination a:hover,
    .share a:hover {
        color: #333
    }

    .boxread a,
    .post .post-content li a,
    .share a,
    .sidebar a,
    .suara-populer-feed a,
    a {
        text-decoration: none
    }

    .content,
    .site-footer,
    .site-header {
        width: 100%;
        justify-content: space-between
    }

    main {
        display: block
    }

    p {
        font-variant-ligatures: no-common-ligatures
    }

    body {
        margin: 0;
        padding: 0;
        -moz-font-feature-settings: "none";
        -ms-font-feature-settings: "none";
        -webkit-font-feature-settings: "none";
        -o-font-feature-settings: "none";
        font-feature-settings: "none"
    }

    .site-header {
        background-color: #fff;
        top: -1px;
        position: sticky;
        z-index: 999;
        -webkit-box-shadow: 0 1px 1px 1px rgb(0 0 0 / 38%);
        -moz-box-shadow: 0 1px 1px 1px rgba(0, 0, 0, .38);
        box-shadow: 0 1px 1px 1pxrgba(0, 0, 0, .38);
        border-bottom: 1px solid #fff
    }

    .site-header .logo {
        padding: 10px 10px 5px 55px;
        float: left
    }

    .site-header .menu {
        padding: 16px 16px 5px 15px;
        float: left
    }

    .site-header .page-links {
        display: block;
        position: absolute;
        top: 10px;
        right: 16px;
        font-weight: 200;
        font-style: normal;
        font-size: 28px;
        line-height: 30px
    }

    .blog-header {
        position: relative;
        padding: 0;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box
    }

    .content,
    .post .bottom-teaser .isLeft {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        overflow-x: hidden
    }

    .blog-header .blog-title {
        margin-bottom: 8px;
        font-size: 50px;
        font-weight: 700;
        letter-spacing: -2px;
        outline: 0;
        line-height: 50px;
        word-break: break-word;
        color: #333
    }

    .blog-header .blog-description {
        font-size: 28px;
        margin: 0 0 20px;
        padding: 0;
        line-height: 1.2;
        color: #666;
        font-weight: 300
    }

    .content {
        margin: 15px auto 0;
        box-sizing: border-box;
        padding: 0 15px
    }

    .content article {
        padding: 0 0 20px;
        border-bottom: 1px solid #f2f2f0
    }

    .content article:last-child {
        border-bottom: 0
    }

    .content article .post-title {
        letter-spacing: normal;
        font-weight: 700;
        display: block;
        font-size: 20px;
        line-height: 32px;
        margin: 0;
        color: #222;
        text-align: center;
    }

    .content article .post-title a {
        text-decoration: none;
        color: #333332
    }

    .content article .post-excerpt {
        letter-spacing: -.02em;
        font-weight: 300;
        font-style: normal;
        font-size: 20px;
        line-height: 1.59;
        color: #666665
    }

    .content article .post-meta {
        font-size: 14px;
        color: #b3b3b1;
        line-height: 30px;
        margin: 15px 0
    }

    .content article .post-meta h2 {
        display: none;
        font-weight: 400;
        color: #666;
        line-height: 20px;
        font-size: 14px;
        margin: 7px 0
    }

    .content article .top-menu {
        border-bottom: 1px solid #e9e9e9
    }

    .content article .top-menu .top-menu--menu {
        font-size: 14px;
        text-transform: uppercase;
        padding-bottom: 5px
    }

    .content article .top-menu .top-menu--date {
        font-weight: 500;
        font-size: 12px;
        text-transform: none
    }

    .content article .top-menu .top-menu--menu a.active {
        text-decoration: none;
        color: #c00;
        font-weight: 700
    }

    .content article .top-menu .top-menu--menu a {
        color: #666;
        text-decoration: none
    }

    .hr--primary {
        border-bottom: 3px solid #ef1b1c
    }

    .left,
    .list-title,
    div.paging,
    div.paging ul.pagination li a,
    ul.pagination li span {
        float: left
    }

    .right {
        float: right
    }

    .clearfix {
        clear: both
    }

    .content article .post-meta a {
        color: #b3b3b1
    }

    .content article .post-meta a:hover,
    .post .bottom-teaser .isRight .site-footer a:hover {
        text-decoration: underline
    }

    .post-template .content {
        max-width: 700px
    }

    .amp-ads {
        text-align: center;
        margin: 20px 0
    }

    .nomargin {
        margin: 0
    }

    .authors {
        font-size: 12px;
        margin: 10px 0 5px;
        font-weight: 400;
        text-align: center
    }

    .authors .left {
        width: 13%
    }

    .authors .right {
        width: 83%;
        padding-left: 5px
    }

    .authors .wrap-authors span {
        text-decoration: none;
        text-transform: capitalize;
        color: #666;
        text-align: left
    }

    .authors .right span:first-child a,
    .authors .wrap-authors span:first-child {
        text-decoration: none;
        color: #555;
        text-align: left
    }

    .authors .wrap-authors time {
        color: #666;
        font-weight: 400
    }

    .reset {
        clear: both;
        margin: 0;
        padding: 0
    }

    .reset.h10 {
        height: 10px
    }

    .reset.h20 {
        height: 20px
    }

    .box-others {
        background: #f8f8f8;
        padding: 0 10px 10px;
        margin: 0
    }

    .box-others h2 {
        padding-left: 15px;
        color: #666;
        padding-bottom: 20px;
        padding-top: 20px
    }

    .headline-image {
        margin: 0 -15px
    }

    .box-wrapper {
        margin: 0;
        padding: 0
    }

    .box-wrapper ul.tag li {
        border: 1px solid #dadada;
        display: inline-block;
        border-radius: 5px;
        color: #000;
        font-weight: 600;
        padding: 5px 8px;
        font-size: 14px;
        margin-left: 0
    }

    .bacajuga,
    code,
    pre {
        border: 1px solid #e8e8e8
    }

    .post .post-content .article ul li {
        list-style-type: initial
    }

    .post .post-content .article ol li {
        list-style-type: decimal
    }

    .bacajuga {
        font-weight: 700;
        font-size: 16px;
        background-color: #f8f8f8;
        padding: 5px 10px;
        text-decoration: none;
        margin-top: 20px
    }

    .bacajuga a {
        text-decoration: none;
        color: #666;
        font-weight: 400
    }

    .index-headline {
        border-top: 1px solid #dededc;
        margin: 0;
        padding: 16px 0
    }

    .index-headline span {
        color: #b3b3b1;
        font-size: 10px;
        text-transform: uppercase;
        letter-spacing: 1px
    }

    .pagination,
    .site-footer {
        font-size: 12px;
        text-align: center
    }

    .pagination {
        padding: 16px 0 0
    }

    .pagination a,
    .site-footer a {
        color: #999;
        text-decoration: none
    }

    .footer {
        background-color: #1f1e1e
    }

    .site-footer {
        margin: 0 auto;
        padding: 20px 0;
        color: #999;
        line-height: 17.6px
    }

    .post .post-title {
        font-weight: 700;
        font-style: normal;
        letter-spacing: -.04em;
        font-size: 50px;
        line-height: 1.1;
        color: #333332;
        margin-bottom: 50px
    }

    .post .post-meta-text {
        color: #b3b3b1;
        letter-spacing: -.02em;
        font-weight: 400;
        font-style: normal;
        font-size: 14px;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis
    }

    .post .post-content {
        width: 100%;
        color: #333
    }

    .post .post-content h3,
    .post .post-content h4,
    .post .post-content h5,
    .post .post-content h6 {
        letter-spacing: -.02em;
        font-weight: 700;
        font-style: normal;
        font-size: 24px;
        line-height: 1.3;
        margin-top: 50px;
        margin-bottom: 0;
        font-family: Arial, Helvetica, sans-serif
    }

    .post .post-content h3 {
        font-size: 36px
    }

    .post .post-content h1,
    .post .post-content h2 {
        font-weight: 400;
        font-style: normal;
        font-size: 22px;
        line-height: 1.2;
        margin-top: 30px;
        margin-bottom: 0
    }

    .baca-juga ul li a,
    .post .post-content p {
        font-style: normal;
        line-height: 1.59;
        letter-spacing: -.002em
    }

    .post .post-content p {
        font-weight: 400;
        font-size: 1.1rem;
        margin-top: 10px;
        margin-bottom: 15px;
        color: #333;
        -webkit-hyphens: auto;
        -moz-hyphens: auto;
        hyphens: auto
    }

    .post .post-content .article a {
        color: #c00;
        text-decoration: none;
        font-weight: 700;
        font-size: 1rem;
        line-height: 28px
    }

    .post .post-content figure {
        margin: 0;
        padding: 0 0 30px
    }

    .post .post-content figcaption {
        font-size: smaller;
        line-height: 1.3;
        color: #666665;
        outline: 0;
        background: #f2f2f2;
        padding: 8px
    }

    .post .post-content hr {
        padding: 0;
        display: block;
        width: 15%;
        margin: 30px auto;
        border: 0 solid #ddd;
        border-top: 1px solid #ddd
    }

    .post .post-content blockquote {
        margin: 0 0 30px -26px;
        border-left: 3px solid #57ad68;
        padding-left: 20px
    }

    .post .post-content blockquote p {
        letter-spacing: .01rem;
        font-weight: 400;
        mborder-left: 3px solid #57ad68;
        mpadding-left: 20px;
        mmargin-left: -26px;
        padding-bottom: 3px
    }

    .post .post-content ol,
    .post .post-content ul {
        padding: 0;
        margin: 0
    }

    .post .post-content li {
        padding: 0;
        font-weight: 400;
        font-style: normal;
        font-size: 15px;
        line-height: 24px;
        margin-left: 20px;
        margin-right: 10px;
        margin-bottom: 12px;
        color: #444
    }

    .post .post-content li p {
        padding: 0 0 1.618rem
    }

    .post .bottom-teaser {
        padding: 50px 0 0
    }

    .post .bottom-teaser hr {
        padding: 0;
        display: block;
        width: 15%;
        margin: 16px 0 16px 100px;
        border: 0 solid #ddd;
        border-top: 1px solid #ddd
    }

    .post .bottom-teaser .isLeft {
        float: left;
        width: 100%;
        box-sizing: border-box;
        padding-bottom: 32px
    }

    .post .bottom-teaser .isLeft .bio {
        margin-top: 18px;
        margin-bottom: 18px
    }

    .post .bottom-teaser .isLeft .username {
        margin-left: 4px;
        margin-right: 18px;
        margin-bottom: 18px
    }

    .post .bottom-teaser .isLeft .index-headline,
    .post .bottom-teaser .isRight .index-headline {
        padding-bottom: 32px
    }

    .post .bottom-teaser .isLeft a {
        color: #000;
        text-decoration: none
    }

    .post .bottom-teaser .isLeft a:hover {
        color: #333;
        text-decoration: underline
    }

    .post .bottom-teaser .isLeft h4 {
        font-size: 18px;
        line-height: 1.1;
        font-weight: 700;
        padding: 0 0 0 100px;
        margin: 0
    }

    .baca-juga ul li a,
    .post .post-content .article .btn-selengkapnya a {
        font-weight: 400;
        color: #333
    }

    .post .bottom-teaser .isLeft p {
        font-size: 14px;
        line-height: 1.3;
        font-weight: 400;
        padding: 0 0 0 100px;
        margin: 0
    }

    .post .bottom-teaser .isLeft p.published {
        color: #999
    }

    .post .bottom-teaser .isRight {
        float: right;
        width: 100%;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box
    }

    .post .bottom-teaser .isRight .site-footer {
        margin: 0;
        padding: 0;
        text-align: left;
        font-size: 14px;
        line-height: 1.3;
        color: #999
    }

    .post .bottom-teaser .isRight .site-footer a {
        color: #333;
        text-decoration: none
    }

    .post .bottom-teaser .isRight .site-footer .poweredby {
        display: block;
        padding-bottom: 18px;
        font-weight: 700;
        color: #333
    }

    .share {
        text-align: right;
        padding: 20px 0 0
    }

    .share a {
        color: #666;
        padding-left: 12px
    }

    .share a .hidden {
        display: none
    }

    code,
    pre {
        font-size: 15px;
        border-radius: 3px;
        background-color: #eef
    }

    code {
        padding: 1px 5px
    }

    pre {
        padding: 8px 12px;
        overflow-x: scroll
    }

    pre>code {
        border: 0;
        padding-right: 0
    }

    .amp-sticky-ad-close-button {
        min-width: 0
    }

    .caption {
        left: 0;
        right: 0;
        padding: 8px;
        background: #f2f2f2;
        color: #555;
        font-size: smaller;
        max-height: 30%
    }

    .btn-close,
    .menu {
        background-repeat: no-repeat;
        background-position: center;
        background-size: 20px 20px
    }

    .boxread {
        background-color: #f8f8f8;
        border: 1px solid #e8e8e8;
        padding: 10px;
        margin-bottom: 20px;
        display: table
    }

    .baca-juga,
    .baca-juga .title {
        margin-bottom: 10px;
        margin-top: 10px
    }

    .boxread a,
    .lanjut a {
        color: #000
    }

    .baca-juga {
        border: 1px solid #e9e9e9;
        background-color: #f2f2f2;
        padding: 10px
    }

    .baca-juga .title {
        font-weight: 700;
        text-transform: uppercase;
        margin-left: 0;
        font-size: 14px;
        color: #ed1b2b
    }

    .baca-juga ul li a:hover,
    .baca-juga ul li.baca-juga-amp {
        color: #ed1b2b
    }

    .baca-juga ul li a {
        font-size: 14px;
        margin-bottom: 0;
        margin-top: 0
    }

    .image-body {
        max-width: 100%
    }

    div.paging ul.pagination li a,
    div.paging ul.pagination li.disabled,
    ul.pagination li span {
        display: inline-block;
        font-size: 14px;
        line-height: 30px;
        color: #666;
        text-transform: uppercase;
        min-width: 30px;
        text-decoration: none
    }

    .menu-top ul li a,
    div.btn-pagination a {
        text-transform: capitalize;
        text-transform: capitalize
    }

    .social-icon {
        text-align: center;
        margin-top: 10px;
        overflow: hidden
    }

    .clear,
    div.paging {
        clear: both;
        width: 100%
    }

    div.btn-pagination a {
        background: #c00;
        border-radius: 3px;
        color: #fff;
        width: 120px;
        padding: 0;
        height: 30px;
        display: flex;
        justify-content: center;
        align-items: center
    }

    div.btn-pagination a center {
        color: #fff;
        font-size: 13px
    }

    div.paging ul.pagination {
        margin: 0;
        display: inline-block
    }

    div.paging ul.pagination li {
        list-style: none;
        display: inline-block;
        margin: 0 2px
    }

    div.paging ul.pagination li:first-child {
        margin-left: 0
    }

    div.paging ul.pagination li a,
    div.paging ul.pagination li span {
        background: #c00;
        border-radius: 5px;
        border: 1px solid #e4e3f0;
        margin: 5px 0;
        display: inline-block;
        color: #fff;
        font-weight: 700;
        font-size: 13px;
        padding: 0 3px;
        position: relative;
        text-align: center
    }

    div.paging ul.pagination li.active span {
        background: #eee;
        color: #363636
    }

    div.new_bacajuga a {
        margin-top: 15px;
        font-size: 14px
    }

    amp-sidebar {
        max-width: 100vw;
    }

    .sidebar {
        width: 100%
    }

    .sidebar a {
        color: #333;
        font-weight: 600
    }

    .sidebar .sidebar-middle ul li {
        border-bottom: 1px solid #d0d0d2;
        list-style-type: none;
        padding: 5px 0;
        margin-bottom: 5px
    }

    .sidebar .logo {
        float: left;
        margin: 7px 10px 0
    }

    .sidebar .sidebar-top {
        background: #fff;
        height: 40px;
        padding: 3px;
        box-shadow: 2px 2px 2px #bebebe
    }

    .sidebar .sidebar-middle {
        background: #fff;
        width: 100%;
        top: 5px;
        position: relative;
        font-size: 14px;
        text-transform: uppercase
    }

    .sidebar .sidebar-middle ul li i {
        font-size: 11px;
        margin-right: 3px
    }

    .sidebar .sidebar-middle .sidebar-left {
        float: left;
        padding: 0 10px;
        width: 45%
    }

    .sidebar .sidebar-middle .sidebar-right {
        float: right;
        padding: 0 10px 0 0;
        position: relative;
        padding: 0 10px 0 0;
        width: 45%
    }

    .sidebar .sidebar-sosmed {
        width: 300px;
        padding: 20px 0
    }

    .sidebar .sidebar-other-media .row,
    .sidebar .sidebar-sosmed.row {
        display: flex;
        flex-wrap: wrap;
        justify-content: center
    }

    .sidebar .sidebar-sosmed.row .col {
        width: 20px;
        margin: 0 10px
    }

    .sidebar .sidebar-other-media {
        position: relative;
        background: #fff;
        padding: 15px
    }

    .sidebar .sidebar-other-media .row .col {
        padding: 2px;
        margin-bottom: 10px
    }

    .post .post-content .list-wrapper li {
        list-style-type: none;
        margin-left: 0;
        border-bottom: 1px dashed #ccc;
        clear: both;
        display: block
    }

    .list-image {
        width: 90px;
        height: 90px;
        float: left;
        margin-right: 10px;
        position: relative
    }

    li.suara-populer-feed {
        clear: both;
        overflow: hidden;
        margin: 5px auto;
        padding: 15px 7px;
        border-bottom: 1px solid #ebebeb
    }

    .suara-populer-feed a {
        display: inline-block;
        width: 100%;
        font-size: 14px;
        line-height: 20px;
        font-weight: 600
    }

    .lanjut,
    .tag a {
        font-weight: 700
    }

    .suara-populer-thumb {
        position: relative;
        float: left;
        width: 35%
    }

    .suara-populer-title {
        width: 60%;
        float: right
    }

    .copyright,
    .navbar-regional .close a,
    .navbar-regional .menu a {
        color: #fff
    }

    .fixed-height-container {
        position: relative;
        width: 100%;
        height: 300px
    }

    .menu {
        background-image: url(https://assets.suara.com/mobile/images/menu.png);
        width: 20px;
        height: 20px;
        display: block
    }

    .btn-close {
        float: right;
        margin: 5px 10px 0 0;
        background-image: url(https://assets.suara.com/mobile/images/close.png);
        width: 30px;
        height: 30px;
        display: block
    }

    .btn,
    .menu-top,
    .menu-top ul {
        width: 100%;
        display: flex
    }

    .box-wrapper .wrapper-title h2,
    .list-wrapper .wrapper-title,
    .list-wrapper .wrapper-title h2 {
        color: #c00;
        margin-bottom: 10px;
        font-weight: 700;
        font-size: 18px;
        text-transform: capitalize;
        font-family: Poppins, sans-serif
    }

    .lanjut {
        font-style: italic
    }

    .btn,
    .menu-top ul li a {
        font-weight: 600;
        color: #fff
    }

    .sidebar-regional {
        width: 100%;
        background: #ed1c24;
        background: linear-gradient(90deg, #ed1c24 0, #ed1c24 0, #41227a 87%);
        padding: 20px 0 10px 0;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column
    }

    .sidebar-regional span {
        margin-bottom: 3px;
        color: #d0c0c6;
        margin-top: 0;
        font-size: 11px;
        font-weight: 700
    }

    .side-suararegional {
        display: -ms-grid;
        display: grid;
        -ms-grid-columns: (1fr);
        grid-template-columns: repeat(4, 1fr);
        justify-items: left;
        width: 310px;
        margin: 0 auto
    }

    .side-suararegional a {
        text-decoration: none;
        font-size: 12px;
        color: #fff;
        text-transform: uppercase;
        font-weight: 700;
        line-height: 30px
    }

    .arkadiacorp {
        padding: 10px 0;
        width: 100%;
        text-align: center
    }

    .bg-sosmed {
        background-color: #fff;
        padding: 0 5px
    }

    .side-logo-partner {
        display: -ms-grid;
        display: grid;
        -ms-grid-columns: (1fr)[3];
        grid-template-columns: repeat(3, 1fr);
        justify-items: center;
        width: 100%;
        margin: 0 auto;
        background-color: #fff
    }

    .sosmed-side {
        padding: 15px 0;
        font-family: Poppins, sans-serif;
        max-width: 100%;
        display: block;
        margin: 15px auto 0 auto;
        background-color: #f6f6f6
    }

    .sosmed-side span {
        font-size: 13px;
        font-weight: 700;
        color: #000;
        display: block;
        text-align: center;
    }

    .sosmed-side .info {
        font-size: 13px;
        color: #000;
        display: block;
        text-align: center;
    }

    .sosmed-side .list-sosmed-side {
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        padding-left: 0
    }

    .sosmed-side .list-sosmed-side li {
        list-style-type: none
    }

    .sosmed-side .list-sosmed-side li a {
        border: 1px solid #dadada;
        width: 40px;
        height: 40px;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 8px;
        margin: 5px 10px
    }

    .m-foot {
        z-index: 2;
        position: relative;
        background: #fff
    }

    .m-foot .other-info {
        /* margin-top: 15px; */
        background: linear-gradient(90deg, #e82724, #41227a);
        padding: 5px;
        font-family: Poppins, sans-serif;
        position: relative
    }

    .bg-gr-foot {
        width: 100%;
        position: absolute;
        top: 0;
        left: 0;
        height: 10px;
        background: linear-gradient(180deg, rgba(0, 0, 0, .3), transparent)
    }

    .m-foot .other-info ul {
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
        list-style-type: none;
        padding-left: 0
    }

    .m-foot .other-info ul li {
        display: flex;
        margin: 0 10px 8px 0;
        padding-right: 10px;
        font-size: 12px;
        font-weight: 400;
        border-right: 1px solid #fff;
        text-transform: capitalize;
        height: 13px;
        align-items: center;
        list-style-type: none
    }

    .m-foot .other-info ul li:last-child {
        border-right: none
    }

    .m-foot .other-info ul li a {
        color: #fff
    }

    .mitramenu {
        text-transform: uppercase
    }

    .post .post-content .article h3 {
        font-size: 17px;
        margin-bottom: 5px;
        margin-top: 0
    }

    .menu-top {
        overflow-x: scroll;
        background-color: #333;
        padding: 0;
        position: relative
    }

    .menu-top ul:first-child {
        margin: 0;
        padding: 10px 20px
    }

    .menu-top ul:first-child li {
        position: relative;
        margin-left: 5px
    }

    .menu-top ul li {
        list-style: none;
        display: flex;
        padding-right: 15px
    }

    .menu-top ul li a {
        font-family: Poppins, sans-serif;
        font-size: 13px;
        white-space: nowrap
    }

    .btn {
        height: 50px;
        border-radius: 10px;
        font-size: 18px;
        margin-bottom: 15px;
        background: #000;
        justify-content: center;
        align-items: center;
        border: 0;
        outline: 0
    }

    amp-sidebar {
        background: #fff
    }

    .btn-selengkapnya {
        background-color: #f4f4f4;
        padding: 7px 10px
    }

    amp-social-share {
        background-size: 35px;
        border-radius: 25px;
        padding: 5px
    }

    .amp-social-share-facebook,
    .amp-social-share-line,
    .amp-social-share-linkedin,
    .amp-social-share-twitter,
    .amp-social-share-whatsapp {
        background-size: 25px
    }
    </style>
    <style amp-boilerplate>
    body {
        -webkit-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
        -moz-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
        -ms-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
        animation: -amp-start 8s steps(1, end) 0s 1 normal both
    }

    @-webkit-keyframes -amp-start {
        from {
            visibility: hidden
        }

        to {
            visibility: visible
        }
    }

    @-moz-keyframes -amp-start {
        from {
            visibility: hidden
        }

        to {
            visibility: visible
        }
    }

    @-ms-keyframes -amp-start {
        from {
            visibility: hidden
        }

        to {
            visibility: visible
        }
    }

    @-o-keyframes -amp-start {
        from {
            visibility: hidden
        }

        to {
            visibility: visible
        }
    }

    @keyframes -amp-start {
        from {
            visibility: hidden
        }

        to {
            visibility: visible
        }
    }
    </style><noscript>
        <style amp-boilerplate>
        body {
            -webkit-animation: none;
            -moz-animation: none;
            -ms-animation: none;
            animation: none
        }
        </style>
    </noscript>
</head>

<body>
    <amp-analytics type="googleanalytics" id="analytics1">
        <script type="application/json">
        {
            "vars": {
                "account": "UA-48479604-1"
            },
            "triggers": {
                "trackPageview": {
                    "on": "visible",
                    "request": "pageview"
                }
            },
            "extraUrlParams": {
                "cd1": "Asy Syaffa Nada A",
                "cd2": "2022-11-17 12:56:34",
                "cd4": "1529928",
                "cd5": "kartika putri, richard lee, kronologi, krim wajah"
            }
        }
        </script>
    </amp-analytics>


    <amp-sticky-ad layout="nodisplay">
        <amp-ad width=320 height=50 type="doubleclick" data-slot="/148558260/Suara/AMP"
            json='{"targeting":{"pos":["bottom"],"page":["Amp"]}}'>
        </amp-ad>
    </amp-sticky-ad>



    <header class="site-header">
        <div class="menu" on="tap:sidebar.toggle" aria-label="menu" role="button" tabindex="0"></div>
        <div class="logo">
            <a href="https://indotnesia.suara.com/" title="Partner indotnesia">
                <div>
                    <amp-img src="https://demo.suara.com/arkadia.suara.sit/suara-2023/mobile/assets/images/logo.svg"
                        width="180" height="24" alt="Logo indotnesia" />
                </div>
            </a>
        </div>
        <div class="clear"></div>

    </header>

    <amp-sidebar id="sidebar" class="sidebar" layout="nodisplay" side="left">
        <div class="navbar-regional">
            <div class="logo">
                <a href="https://indotnesia.suara.com/" title="Logo indotnesia">
                    <div>
                        <amp-img src="https://demo.suara.com/arkadia.suara.sit/suara-2023/mobile/assets/images/logo.svg"
                            width="180" height="24" alt="Logo indotnesia" />
                    </div>
                </a>
            </div>
            <div class="btn-close" on="tap:sidebar.close" aria-label="close menu" role="button" tabindex="0"></div>
        </div>

        <div class="clear"></div>

        <div class="sidebar-middle">
            <ul class="sidebar-left">
                <a href="https://www.suara.com/">
                    <li>
                        Home
                    </li>
                </a>
                <a href="https://www.suara.com/terpopuler">
                    <li>
                        Terpopuler
                    </li>
                </a>
                <a href="https://www.suara.com/pilihan">
                    <li>
                        Topik Pilihan
                    </li>
                </a>

                <a href="https://www.suara.com/news">
                    <li>
                        News </li>
                </a>
                <a href="https://www.suara.com/bisnis">
                    <li>
                        Bisnis </li>
                </a>
                <a href="https://www.suara.com/bola">
                    <li>
                        Bola </li>
                </a>
                <a href="https://www.suara.com/sport">
                    <li>
                        Sport </li>
                </a>
                <a href="https://www.suara.com/lifestyle">
                    <li>
                        Lifestyle </li>
                </a>
                <a href="https://www.suara.com/entertainment">
                    <li>
                        Entertainment </li>
                </a>
                <a href="https://www.suara.com/otomotif">
                    <li>
                        Otomotif </li>
                </a>
                <a href="https://www.suara.com/tekno">
                    <li>
                        Tekno </li>
                </a>

            </ul>
            <ul class="sidebar-right">

                <a href="https://www.suara.com/health">
                    <li>
                        health
                    </li>
                </a>
                <a href="https://www.suara.com/foto">
                    <li>
                        Foto
                    </li>
                </a>
                <a href="https://clickmov.suara.com/">
                    <li>
                        Clickmov
                    </li>
                </a>
                <a href="https://yoursay.suara.com/">
                    <li>
                        Your Say
                    </li>
                </a>
                <a href="https://www.suara.com/partner/content">
                    <li>
                        Our Networks
                    </li>
                </a>
                <a href="https://www.suara.com/pressrelease">
                    <li>
                        Press Release
                    </li>
                </a>
                <a href="https://www.suara.com/wawancara">
                    <li>
                        Wawancara
                    </li>
                </a>
                <a href="https://www.suara.com/liputankhas">
                    <li>
                        Khas
                    </li>
                </a>
                <a href="https://www.suara.com/cekfakta">
                    <li>
                        Cek Fakta
                    </li>
                </a>
                <a href="https://www.suara.com/indeks">
                    <li>
                        Indeks
                    </li>
                </a>
                <a href="https://www.suara.com/pages/kontak">
                    <li>
                        Kontak
                    </li>
                </a>
            </ul>
            <div class="clear"></div>
        </div>

        <div class="sidebar-regional">
            <span>SUARA REGIONAL</span>
            <div class="side-suararegional">
                <a href="https://suarajakarta.id" rel="" title="Suara Jakarta">Jakarta</a><a
                    href="https://suarabogor.id" rel="" title="Suara Bogor">bogor</a><a href="https://suarabekaci.id"
                    rel="" title="Suara Bekaci">bekaci</a><a href="https://suarajabar.id" rel=""
                    title="Suara Jabar">jabar</a><a href="https://suarajogja.id" rel="" title="Suara Jogja">jogja</a><a
                    href="https://suarajawatengah.id" rel="" title="Suara Jateng">jateng</a><a
                    href="https://suaramalang.id" rel="" title="Suara Malang">malang</a>
                <a href="https://suarajatim.id" rel="" title="Suara Jatim">jatim</a><a href="https://suarabali.id"
                    rel="" title="Suara Bali">bali</a><a href="https://suaralampung.id" rel=""
                    title="Suara Lampung">lampung</a><a href="https://suarabanten.id" rel=""
                    title="Suara Banten">banten</a><a href="https://suarasurakarta.id" rel=""
                    title="Suara Surakarta">surakarta</a><a href="https://suarakaltim.id" rel=""
                    title="Suara Kaltim">kaltim</a><a href="https://suarakalbar.id" rel=""
                    title="Suara Kalbar">kalbar</a>
                <a href="https://suarasulsel.id" rel="" title="Suara Sulsel">sulsel</a>
                <a href="https://suarasumut.id" rel="" title="Suara Sumut">sumut</a><a href="https://suarasumbar.id"
                    rel="" title="Suara Sumbar">sumbar</a><a href="https://suarasumsel.id" rel=""
                    title="Suara Sumsel">sumsel</a><a href="https://suarabatam.id" rel=""
                    title="Suara Batam">batam</a><a href="https://suarariau.id" rel="" title="Suara Riau">riau</a>
            </div>
        </div>

        <div class="arkadiacorp">
            <a href="http://arkadiacorp.com/" target="_blank" rel="noopener">
                <amp-img src="https://assets.suara.com/mobile/images/icon_arkadia.png" alt="Arkadia Digital Media"
                    width="110" height="40"></amp-img>
            </a>
        </div>
        <div class="bg-sosmed">
            <div class="side-logo-partner">
                <a href="https://www.suara.com/">
                    <amp-img src="https://assets.suara.com/mobile/images/partner/suara.svg" width="117" height="25"
                        alt="Logo suara" style="margin:4px auto"></amp-img>
                </a>

                <a href="https://www.matamata.com/" target="_blank" rel="noopener">
                    <amp-img src="https://assets.suara.com/mobile/images/partner/matamata.svg" width="117" height="25"
                        alt="Logo matamata" style="margin:4px auto"></amp-img>
                </a>

                <a href="https://www.bolatimes.com/" target="_blank" rel="noopener">
                    <amp-img src="https://assets.suara.com/mobile/images/partner/bolatimes.svg" width="117" height="25"
                        alt="Logo bolatimes" style="margin:4px auto"></amp-img>
                </a>

                <a href="https://www.hitekno.com/" target="_blank" rel="noopener">
                    <amp-img src="https://assets.suara.com/mobile/images/partner/hitekno.svg" width="117" height="25"
                        alt="Logo hitekno" style="margin:4px auto"></amp-img>
                </a>

                <a href="https://www.dewiku.com/" target="_blank" rel="noopener">
                    <amp-img src="https://assets.suara.com/mobile/images/partner/dewiku.svg" width="117" height="25"
                        alt="Logo dewiku" style="margin:4px auto"></amp-img>
                </a>

                <a href="https://www.mobimoto.com/" target="_blank" rel="noopener">
                    <amp-img src="https://assets.suara.com/mobile/images/partner/mobimoto.svg" width="117" height="25"
                        alt="Logo mobimoto" style="margin:4px auto"></amp-img>
                </a>

                <a href="https://www.guideku.com/" target="_blank" rel="noopener">
                    <amp-img src="https://assets.suara.com/mobile/images/partner/guideku.svg" width="117" height="25"
                        alt="Logo guideku" style="margin:4px auto"></amp-img>
                </a>

                <a href="https://www.himedik.com/" target="_blank" rel="noopener">
                    <amp-img src="https://assets.suara.com/mobile/images/partner/himedik.svg" width="117" height="25"
                        alt="Logo himedik" style="margin:4px auto"></amp-img>
                </a>

                <a href="https://regional.suara.com/" target="_blank" rel="noopener">
                    <amp-img src="https://assets.suara.com/mobile/images/partner/regional.svg" width="117" height="25"
                        alt="Logo regional" style="margin:4px auto"></amp-img>
                </a>

                <a href="https://clickmov.suara.com/" target="_blank" rel="noopener">
                    <amp-img src="https://assets.suara.com/mobile/images/partner/clickmov.svg" width="117" height="25"
                        alt="Logo clickmov" style="margin:4px auto"></amp-img>
                </a>

                <a href="https://yoursay.id/" target="_blank" rel="noopener">
                    <amp-img src="https://assets.suara.com/mobile/images/partner/yoursay.svg" width="117" height="25"
                        alt="Logo yoursay" style="margin:4px auto"></amp-img>
                </a>

                <a href="https://www.serbada.com/" target="_blank" rel="noopener">
                    <amp-img src="https://assets.suara.com/mobile/images/partner/serbada.svg" width="117" height="25"
                        alt="Logo serbada" style="margin:4px auto"></amp-img>
                </a>

                <a href="https://www.theindonesia.id/" target="_blank" rel="noopener">
                    <amp-img src="https://assets.suara.com/mobile/images/partner/theind.svg" width="117" height="25"
                        alt="Logo theindonesia" style="margin:4px auto"></amp-img>
                </a>

                <a href="https://hits.suara.com/" target="_blank" rel="noopener">
                    <amp-img src="https://assets.suara.com/mobile/images/partner/beritahits.svg" width="117" height="25"
                        class="logo-partner" style=" margin:4px auto" alt="Logo hits"></amp-img>
                </a>

                <a href="https://www.iklandisini.com/" target="_blank" rel="noopener">
                    <amp-img src="https://assets.suara.com/mobile/images/partner/iklandisini.svg" width="117"
                        height="25" alt="Logo iklandisini" style="margin:4px auto"></amp-img>
                </a>
                <a href="https://nexuscreatorhub.com/" target="_blank" rel="noopener">
                    <amp-img src="https://assets.suara.com/mobile/images/partner/nexus.svg" width="117" height="25"
                        alt="Logo Nexus" style="margin:4px auto"></amp-img>
                </a>
                <a href="https://www.suara.com/kotaksuara" target="_blank" rel="noopener">
                    <amp-img src="https://assets.suara.com/mobile/images/partner/kotaksuara.svg" width="117" height="25"
                        alt="Logo Kotaksuara" style="margin:4px auto"></amp-img>
                </a>
                <a href="https://liks.suara.com/" target="_blank" rel="noopener">
                    <amp-img src="https://assets.suara.com/mobile/images/partner/liks.svg" width="117" height="25"
                        alt="Logo Liks" style="margin:4px auto"></amp-img>
                </a>
            </div>
        </div>

        <div class="sosmed-side">
            <span>Ikuti Kami</span>
            <ul class="list-sosmed-side">
                <li>
                    <a href="https://www.facebook.com/suaradotcom" target="_blank" rel="noopener"
                        aria-label="share facebook">
                        <svg width="14" height="25" viewBox="0 0 14 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M13.5655 0.0852065L9.9452 0.00927734C8.51218 0.00927734 7.15458 0.54078 6.09867 1.52786C5.04275 2.51493 4.5148 3.88165 4.5148 5.3243V8.81703H1.1208C0.969954 8.81703 0.894531 8.89296 0.894531 9.04482V13.3728C0.894531 13.5246 0.969954 13.6006 1.1208 13.6006H4.5148V24.6102C4.5148 24.7621 4.59022 24.838 4.74107 24.838H9.19098C9.34182 24.838 9.41725 24.7621 9.41725 24.6102V13.6765H12.8112C12.9621 13.6765 13.0375 13.6006 13.0375 13.4487L13.5655 9.12075C13.5655 9.04482 13.5655 8.96889 13.49 8.89296C13.4146 8.81703 13.3392 8.81703 13.2638 8.81703H9.2664V6.61509C9.2664 6.61509 9.19098 5.55209 9.71893 4.94466C9.9452 4.64094 10.3223 4.48908 10.7748 4.48908H13.3392C13.49 4.48908 13.5655 4.41315 13.5655 4.2613V0.312993C13.7917 0.237064 13.7163 0.0852065 13.5655 0.0852065Z"
                                fill="#4C6CAD" />
                        </svg>

                    </a>
                </li>
                <li>
                    <a href="https://twitter.com/suaradotcom" target="_blank" rel="noopener" aria-label="share twitter">
                        <svg width="27" height="23" viewBox="0 0 27 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M23.6385 4.17267C23.6385 4.17267 25.1469 3.03373 25.6749 1.3633C25.6749 1.3633 23.6385 2.35037 22.3563 2.35037C21.3758 1.05958 19.792 0.300293 18.1327 0.300293C15.1158 0.300293 12.7023 2.73002 12.7023 5.76717C12.7023 6.37461 12.7777 6.90611 13.004 7.51354C13.004 7.51354 13.004 7.51354 12.9285 7.51354C4.85836 6.98204 1.6152 1.74294 1.6152 1.74294C1.6152 1.74294 -0.345772 5.8431 2.82196 9.10805C2.82196 9.10805 1.99232 9.18398 1.01183 8.65247C1.01183 8.65247 0.333028 12.1452 4.70752 14.0434C4.70752 14.0434 3.50076 14.3471 2.82196 14.2712C2.82196 14.2712 3.87787 17.5362 7.79983 17.9917C7.79983 17.9917 6.14054 20.1177 0.408447 20.4974C0.408447 20.4974 9.91165 26.7236 19.1132 18.2954C19.1132 18.2954 23.1105 14.8786 23.6385 7.96911L26.3537 3.56524L23.6385 4.17267Z"
                                fill="#29B3F3" />
                        </svg>

                    </a>
                </li>
                <li>
                    <a href="https://www.youtube.com/user/suaradotcom" target="_blank" rel="noopener"
                        aria-label="share youtube">
                        <svg width="27" height="21" viewBox="0 0 27 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M21.1494 0.818359H5.61237C2.82175 0.818359 0.559082 3.09623 0.559082 5.9056V15.093C0.559082 17.9024 2.82175 20.1802 5.61237 20.1802H21.1494C23.94 20.1802 26.2026 17.9024 26.2026 15.093V5.9056C26.2026 3.09623 23.94 0.818359 21.1494 0.818359ZM10.2131 14.7133V6.20931C10.2131 5.52595 10.9674 5.14631 11.4953 5.45002L18.585 9.70204C19.113 10.0058 19.113 10.841 18.585 11.2206L11.4953 15.4726C10.9674 15.8523 10.2131 15.3967 10.2131 14.7133Z"
                                fill="#FF2A1A" />
                        </svg>

                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/suaradotcom/" target="_blank" rel="noopener"
                        aria-label="share instagram">
                        <svg width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink">
                            <rect x="0.492188" y="0.224121" width="25.9552" height="25.9552" fill="url(#pattern0)" />
                            <defs>
                                <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                                    <use xlink:href="#image0_832_42" transform="scale(0.00195312)" />
                                </pattern>
                                <image id="image0_832_42" width="512" height="512"
                                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAYAAAD0eNT6AAAgAElEQVR4Aey9B3hU55n3PXZsx2m7m+xu1t+32WTXsXevZL/dN7bjDZqmUQfjQmyEpFGnCASIbgxuDCCKEE2iCoEkiigDEqIIEBKWTTNlpBk1ME3COM73On5dghvGxs973SMMwirTzpk55z5/X9dzeZhzzvM89+/+32XOFOl0+A8EeiFgs9TfM/tJ58O5sY3mmXGOgbNiTyfOjmsYkdvfMWl2bMOMubGO/Nmxp1fPiXOU5/Y/vXt23On62bEOx5w4x7nZsQ3v5sY5rubGNnyTG+cQGGAADfiogc7YuUqx1BlTDgfFGMUaxRzFXmcMNsxwx2RcwwiKUYpVilmKXYrhXsIbT4MACICATmezOP5h1oDThjmxjUNz4xrycuMcVbmxjrO5sY7rSNo+Jm00O2j2lKQBimGKZYrpuIY8inGKdYp55D4QAAGNELD9tu2+vOiG38yNdQzKjXVMy41zlM6OazieG+v4AEUeRR4a0KAGYh0fuHNAnKOUcgLlBsoRlCs0khZhJgjwJDA/3vG3s/uffnpOnGNpbqyjOTfO8TWSvAaTvJJejWIvark78jXlDModlEMol/DMkrAKBJgQmBh//AdzYp0xuf0d83JjHSdR8FHs0fBBAxJpgBqCk5RbKMdQrmGSNmEGCKiTQNFjjnvnDjhtyI1zvDqn/6nX58Q5vpwTd1pggAE0AA3IqwHHl5RzKPdQDqJcpM4sil2DgIoIzIlzPJobe2rK3NhT++fGnv5U3iBHEgVfaAAa8KwBykWUkyg3zYo+/YiKUiq2CgLKJjAv6vSD7k471nEBychzMgIjMIIGQquBubGOC5SzKHcpO7tidyCgQAJLBjn/bm7cqRHz4k4fmRt3SmCAATQADahRAzdz2AjKaQpMtdgSCCiDgPtHd2JPPjk37rR9TuzJL9QY7NgzihQ0AA30pIHOnHbaPjv25JP4cSJl1BzsQgEE5sWeeGxe7ImCuf1P/WVe3CmBAQbQADTAWQPuXBd7ooBynwJSMLYAAsElkGc58Yv5saemzet/so1zoMM2FDJoABroUwP9T7ZRLqScGNwsjNVAIMgE5j/h+K/5sae2zIs7daPPoMCdANwJgQagAW1p4AblRsqRQU7LWA4E5CWwsP+xx+fHnayaH3Pqm/mxJwUGGEAD0AA00IMGKEfGnayinClvVsbsICAzgblxJ03zY07WINB7CHQ0QmgEoQFooC8NxJysoRwqc5rG9CAgLYEFMSdi82JPHs6LPSkwwAAagAaggYA0cJhyqrRZGrOBgIQEhBB35UefeGZ+zMlTebEnBAYYQAPQADQgnQYot1KOpVwrYerGVCDgPwGbzXb3/NgTiXkxJ5oR7NIFO1iCJTQADfSogZgTzZRzKff6n7lxJQgESGBBzMn4vJg3zy+IfVNggAE0AA1AA8HTgDv3xpyMDzCN43IQ8I3AgpgTDy+IOV6DYA9esIM1WEMD0ECPGqBcHHPiYd+yOM4GAR8J2Cz19y+MOTEzP+74tfzYNwUGGEAD0AA0oAANxB2/RrmZcrSPaR2ng4BnAgtj3xywIPb4xfzY4wIDDKABaAAaUJ4GKEdTrvac0XEGCHhBYFHsG/+SH/NmBYJdecEOn8An0AA00KMGYt6soNztRYrHKSDQnQD9xar8/sefz485+unC2GMCAwygAWgAGlCPBih3Uw7HXx/sXt/wTB8EFsYeMy+MO9aKYFdPsMNX8BU0AA30qIHOXG7uI+XjEAjodPmxx36+KPb4+h5FhLsAuAsCDUAD0IBqNUC5nXI8ah0IdCOQH3vkqYUxxz5YGHNMYIABNAANQAMsNfAB5fpuBQBPaJNA0WOOexfFHVm0KPaYwAADaAAagAY0oIG4I4so92uz6sFqN4GFTxz71cKYIycWxRwVGGAADUAD0IB2NEC5n2oAyqEGCeRHH3tmYcyRDxHw2gl4+Bq+hgagga4aoBpAtUCDJVCbJtt+23bfotijSxbHHBUYYAANQAPQADRANYFqgzarokaszo9949+WRB89tTj6iMAAA2gAGoAGoIFvNUC1gWqERsqhtsxcGH302SXRRz5aEnNEYIABNAANQAPQQDcNRB/5iGqFtqojY2vdt/yjDxcuiT4sMMAAGoAGoAFowJMGFkUfLsRbAipvDJZEHXlwacwRhydn4zgSAjQADUAD0EBXDbhrR9SRB1VeBrW5/YVRrz++JOqN95dGHxYYYAANQAPQADTgqwaohlAt0WYVVanVhTFvxBZEvf7J0ug3BAYYQAPQADQADfirAaolVFNUWg61te0lka8nFUQdvl4Q/YbAAANoABqABqCBgDUQdfg61RZtVVOVWVsQ9cb4gqg3vimIfl1ggAE0AA1AA9CAZBqg2hL1xniVlUVtbHdp9BvzJHM0Ggg0UNAANAANQAM9aIBqjTaqqgqstMfbv1cQ/VpJYfTrAgMMoAFoABqABuTWANUcqj0qKJF8t7g4/vgPCqJe3y23szE/Ego0AA1AA9BAVw1Q7aEaxLfCKtiylQOP/HRZTP3RZdGvCwwwgAagAWgAGgi6BmLqj1ItUnCp5Le1xZGH/nl5TH1r0J2NZgPNFjQADUAD0EAXDVAtoprEr9Iq0KLlUXX/Xhj92tvLousFBhhAA9AANAANhFoDVJOoNimwZPLZEnVZywh0dL3AAANoABqABqABpWiAahPuBMjUb6wcuPeny2MOtS6Pfk1ggAE0AA1AA9CA4jQQc6iVapVMZVCb09InLZfHvHZ0RfRrAgMMoAFoABqABpSqAapV+HaARL0KfddyRdSh3Up1NvaFRAQNQAPQADRwhwaiDu3G7wRI0AQsjz5UsiL6kMAAA2gAGoAGoAG1aIBqlwQlULtTrIg+NG9l1CGBAQbQADQADUADatMA1TDtVvAALF8dVTd+ZVSdwFAugxWRdZ+vjK59b1Vk3cWVUbXOlVF1h1dEHqpeGVm3ZVVk7ZqVUQcXrYyom7kistaGAQbQgG8aoNihGHLHUmTdFndsRdUdplhzx1x07XvuGESeVHSdoFoWQCnU3qWrIw8mrY489M2qqDqBEVoGq6Nq/7wqqq5+VUTd6lURtRNXRh4asDKq7sF6S/092lMmLAYB5RGgWKSYpNikGHXHalRd/c3YRQ4NcR2hWkY1TXnKUeCOimIOxa6OPHQdhT/IhZ9ezUfV1q6Kqpu1Kqo+eZml/vd5hqM/UaBEsCUQAAEvCVAMUyxTTFNsu2M8su5z5Nfg5leqaVTbvHSbNk9bEV73+MrI2k9WRdUKDHkZrI6su74qsvboqqiDs1ZF11oKH9r3fW2qDlaDgLYIUKxTzLtjP7L2qDsXIOfKXnOotlGN05bavLSWbmGtjqx7f3VUrcCQnsGqqNobqyNrG1ZH1uSviTw0YIWl/sdeugangQAIMCZAuYByAuUGyhHuXIE8LE8diqx7n2odYzn5bpr9t233FUUedKDwS1/4i6JqT62Mrs0pNO37R989gytAAAS0RoByBeUMyh3IyTLk5MiDDqp5WtNVr/aujqwtLIqqFRgSMYisvVwUXZO7sv/+/+gVOg6AAAiAgAcClEMolxRRTkGOlqxGUc3zgF4bh1dH1D4LYUlQ+KNrPy6KOli8OrbGLIS4SxvqgZUgAALBIEA5hXIL5Zgid66RIGdpvKGg2hcM3yl2jeLYmn8riqz9qCjqoMDwk0FkzYlVEbVDSi319yvW0dgYCIAAGwKUayjnFEXWnEDe9jNvU82LrP2IaiAbYfhiiPt9/6iDpyAgPwUUWfPa6oiDUb4wx7kgAAIgICUBykFFkTWvIY/7mcejDp7S5OcBVkfWLIFo/BLN3nVR+8OkDGLMBQIgAAKBEKCcVBR1cC9yuu85nWphIOxVd21x9IFn1kTVCAzvGBRF1dwoijxoXxNz8HeqczY2DAIgoBkClKMoV1HOQn73Lr8TJ6qJmhDJ2shDvyqKrPkQ4vAsjuLIg9+siTywAZ/m10RowEgQYEOAchblLncOw4s9jy92qSZSbWQjgJ4MKXrMcW9RxIETayIPCAwPDCIOOHGrvycV4TkQAAG1EKActibigBP53kO+jzwgqDZSjVSLb33e55qoA4uKIw8IjN4ZrInc/9fiqJpx9nj793wGjAtAAARAQGEEKJdRTnPnNuT/Pusf1UiFuU+a7ayN3PcUCn/vhf8mm/IVT9Q/IA1xzAICIAACyiFAua048kA56kDfdYBqpXK8JsFOVulrfr42cv8HayP3C4zuDIojD5xZF3UgQgLUmAIEQAAEFE2Ach3lPNSC7rXgJpMPqGYq2om+bG5dxL71cHYPzo7Y/8XayH0vsH7fxxeh4FwQAAFNEKCcR7lvrTsH9pAbNf5ikWomCyGsjaw2o/j3IPCo/eeKo/b/NwsnwwgQAAEQ8IMA5cC1UfvPoUb0UCMiq81+IFXOJfWW+ntKIva3rovcJzBuM1gbUV2+wmLHn+NVjlSxExAAgRARoFxIORE14naNIBZUO6mGhsgtgS+7LqL6eTi1i1Mj9n2xLnLfiMDJYgYQAAEQ4EWgJGJv1rrOHIkXjN++aI6ofl6VXi4x1/xLSWT1pyWR1QKjWpRGVp9bj1v+qtQyNg0CIBAcApQjKVeiZtyqm59SLQ0OfQlXKYnYWwEn3nQibvlLqCxMBQIgwJkAvSVQElFdjvrxbf3YW6Eqf6+P3DcAzqsW6yL2fVViqR6pKudhsyAAAiCgAAKUO905FHeRBdVUBbjE8xbob0WXRlRfLI2oFloeJRF7PyuN2POEZ2I4AwRAAARAoCcClEM7c6m264m7plrq7++JkaKeK4nYO1PLhd9te2T1B8XRu/spyjHYDAiAAAiokADl0tLI6g+0XleotirafaXmPQ+XRVZfK4vYK7Q6Si3VV4qj9/5G0Y7C5kAABEBARQQop1Ju1WpdcdsdWX2Naqxi3VYWUV2jZQeVRuxtK7bs+4ViHYSNgQAIgIBKCVBupRyr5RpDNVaR7tsQuTd+fcQeodlh2X3cHmf/mSKdg02BAAiAAAMClGPXW3Yf12ydidgjqNYqypXCZrt7vWXPea06pcyyd2/RU3t+qCinYDMgAAIgwJAA5VrKuVqtN1RrqeYqxrUbovYmatUZJERV/1yjYlSEjYAACICAdwQo52q5CaCa6x0pmc8SQty10bKreUPEbqG1Qbei8MpfZoFhehAAARDogQDlXsrBWqs7ZC/VXKq9PWAJ7lPrI6qf0aQDIna14T3/4GoNq4EACIBAVwKUgzdG7GrTYg2i2tuVRUgeb7TsPrWROhINjQ2WXVfwaf+QyA2LggAIgMAdBCgXU07WUg1y22rZfeoOEMH+R1nkntiNEbuElsaGiKoPNuB7/sGWGtYDARAAgV4JUE6m3KylWkS2Ug3uFYrcBzZaqg5rCfiGiF2fleIX/uSWFeYHARAAAZ8JUG6mHK2lmkQ12GdQUlxQGl5l2mipEpoZ4bu+2mCuwm/7SyEezAECIAACMhCgHL0xfNdXmqlLlipBtVgGlH1PuTG8qkZLkNdbqvBX/fqWBI6CAAiAQMgJUK7WUm2iWhxU6JvCKx7fZKkSmhkRVeVBBYzFQAAEQAAE/CawKaKqXDP1iWpxeMXjfsPy9cJNlsqqTZadQgujPLzy3AqL/ce+MsL5IAACIAACoSFAOZtytxZqVKeNlVVBIV1qrvqv8vCqb7QBtvKLzaZd/x0UsFgEBEAABEBAMgKUuzdZKr/QQq2imky1WTJ4vU1UbqncUm7ZKTQxInZm9cYBz4MACIAAJwL2ePsPtlh2/iunHzgrj9iZpYla5a7JlVtk1eMmS8UvNoVX3tAC0E0RO/G+v6xqwuQgAALBJkC/ob85sjKy3Fwxu9yys7Q8vPLgZktla3n4zg/vzOuVX5SHV14qD688sjm8clt5ROXi8siKVDU2B5TL77SN5wtYqs1Uo2XT1FZL5bTNlp2C/QivPGfH+/6y6QgTgwAIBI/AOsOun2yxVA0ut1Ru2hy+88OA8nf4zq+2WHbWb7ZUTLDH2v8teFb4vxLl8s3hlecCslsldY9qtP+kPFxZHl7ZttlSKXiPii82m3bgfX8PWsBhEAABZRPYZKnotyW8cs9mS+U1uXJ2eXhl82ZLxUil/0VUyumbLRVfyMVBKfNSjZZFleUR9seUYqSc+yi3VL4gC0BMCgIgAAJBILDVWPnvWyIqK+TMkz3M/Va5ufLZIJjn9xKU23vYN7sXtFSr/YbU24VbwncUbLFUCubjTNFjjnt7Y4DnQQAEQECpBOyW6ge2WipXb3Hfpg9Nrt5qqXhzSyh+mc4Lp1Bu32KpPMO8hgmq1V7g8P4Uur2zJXzHX7ZYKgTnsS28IsJ7KjgTBEAABJRBYIulYthmS8WnSsnPmyMqNxc9teeHyqBzexeU45XCSLZ9hO/4i6RvyWy1VD651VIhOI8tETvwqf/bcYJHIAACKiBgj7d/b6tl5xJl5uZKp91g/6XSMFKuVyYvKWts5ZOScd8aXmHfatkhuI4t4dv/arfYH5AMGCYCARAAAZkJlFpK/26rZccBJeflLeEV722KqDDIjMKn6SnXU85XMreA9xZeYfcJSm8n3xTZFwFvSMENxLaIynG92Y/nQQAEQEBpBMrNOx7eatn+lhry8rbwHV/SWxRKYkg5Xw3sAtjjF1S7A2a+LXzHiG2WHYLrsFt2OOk2WsCgMAEIgAAIBIFARVTFg9vCt7+vupysoBdalPMp96uOoS+1OHzHiIDlaA/ffmSbZbvgOOzhO77ZHF4ZFjAkTAACIAACQSBAP+qzNdzeosZ8bLds/3qbZVt0EDB5tQTlfqoBamTpzZ6pdnsForeTKqLKH7Rbtgu2I9y+oTfb8TwIgAAIKImAEOIue7h9l5rz8bZw+4d2i/0hpXC1h9s3qJmnp71TDfebtT3c/qo93C44jm3mbTfsBvt/+A0HF4IACIBAEAlsM22fwyMXbzuz6Q+b/iaI6HpdimqAuxYwrXNUw3s13tMBe7j9Ag/B9djESPMpSU8QcRwEQAAEAiRgt9gTeOXibdV0RyNALJJcbg+323mxvaPeXfALUoVp66Pbw+2C67Bb7L/zCwwuAgEQAIEgErBH2/92e7j9/3DLxXZzhTWIGHtdimoBN7Zd7aFa3qvxvR3Ybt42ZXv4NsFx2C32vb3ZjedBAARAQEkEdpjt8zjm4e3h29rtv7XfpwTWVBOYMhZUy31mvD3cvn9H+DbBcdjD7fjkv8+KwAUgAALBJlBhqfjFdvO2zznmYbLJbraPDzbTntajmsCVMdXynmzu9TnHY457t4dv+XRH+FbBbpi3vtar4TgAAiAAAgoisCPcXsouB3epK9vDt7yvlA8E7jBvfY0ja6rlVNO9lvU20zYDRxBk03bTliivQeBEEAABEAgRAbvZ/l/bzVtucM3Ft+yybM0NEeI7lqXacGtPXZoUDs9RTb/D2L7+sSPc/ioHo7vZYN5yoi+7cQwEQAAElEKgwrJ1U7ccxqwwkX3bw7d+Yo+3/0AJ3HeYt5zgydyHrwNWmLe8vsO8VfAbm4coQWTYAwiAAAj0RcDxWNG9281bP+aXg3upK+HbnuqLR7CO7TBvHsKROdV0rxhSJ1Zh3vZlRfhWwWnsCN/6caml9H6vIOAkEAABEAghgQpjeTSn/OvJlp2WrWtDiPvW0lQjqFZ42q/qjpu3fenVXZbK8M0xqjPOu2al+JaX8QAEQAAEFEygwrx1GdM83PMLS/OW94TNdrcSXFIRvrWYI3uq7R75Vpo3z6swbxHcxi5zudmj8TgBBEAABBRAoMK89W1uOdiTPdvNW/QKQK+jWuFpr2o8TrXdI99K8+aTleYtgtOoMG2+rJSfnfToAJwAAiCgaQK7zFse4ZR/vbWlwrQ1TwmOp1pBNcPbfavnvM0n++Rrf6zobyvDt3xdad4smA1FfM2kT/g4CAIgAAI6nW6nuXw8s/zrZT0pf1MpAqg0b85l54PwLV9Tje+VcYV5y9M7zZsFt7ELf/WvV5/jAAiAgLIIVIVvWsAtB3tlj2nzFaV4gmqGV3tWWb2kGt8r40rz5qXcjK40l5/q1WAcAAEQAAGFEagyb97ELQ97Y0+lefN1Jb1VS7XDm32r6Ryq8b3Kfae5vHmnuVxwGhWmzTm9GowDIAACIKAwAjtNm+s55WBfbNn1h83/pBR3UO3wZe8qObe5R770V5mqwjd/XWUuF1zGTtOmG/tM9n/s0WA8CQIgAAIKJFBl3nyOSw721Y5d5vWPKMUlVDuohvhqg6LPD9/8dY9/gbHSvOE3VeZNgtPYad7YoBQxYR8gEAoCe54q+iG9qtoZufXXOy3lv6u0bDbusmzuv8tYHl9p3JhJgx7Tc3SMzqFz6Rq6NhR71vqaO82bPuGUh32xZZdp40Al+Z9qiC/7V8O5VOu7Md5j2jhol2mT4DSqTBvzuxmKJ0BA5QToB1P2GssfrDKXP7HLuHHSLnN50S5T+aEqU7ljl2njuSrTpnerjBuvVhk33gg0nmkO91ymTe92zk1rlB9yr2ncOIn2QHtRyo+4qNy1Ovq1tkB9pubr9xg3ZirJh1RD1Myzp71Tre/GuMq8eVpPJ6v5ud3hmwZ0MxRPgIBKCOwyrPvJ7vBNj+8ybUrZbd6UW2XcuH23eVPLLtPGa8qLy43XaG8395jr3jPt3bDuJyrBrZht7jJv+kJ5/g3Oi8NKU/kzinGETqejGsLNF1TruzGuMm0q3WXaKLiMKuPG63bLih93MxRPgIACCewbsO/7laYN4TuNG21Vxo11VaaN9IqbRTySLWQT2UY2kq0KdIFitrTLtOkSF9/7agc1vIpxhE6noxpCtcRXO5R8PtX6box3mzYc323aKBiNo92MxBMgoBAC9Zb6e3YbNvXbbVr/4i7Tptrdxo2fM4q9vvOIcePnbptN618kBsRCIW5RxDb2mDce1owWvlNzdhm2/L+KcEKXTew2bTzKyx8bjncxr/PhbuOGDzgZuce0cVY3I/EECISIAL1Hvtu08dHd5o1Tdps3Vu8ybrjKKd4CscXNwryx2s3GtPFRrX+eYLdp09ZAeKr12l3GDTfs8fbvhShEe12Waolamfa4b+OGD+4wdo9l8z/sMW0QnMZOY6nlDiPxDxAIMoEDcfaf7TZvHLnbuGHnHtPGDznFl7y2bPyQmBE7Yhhkt4V8ud2mDYvl5avYXP/nkMPvYQNUS9j5w7L5H26Zusu0ycDJwN3GDZ/jfcZb7sWDIBJwPFZ07y5D2dO7jesrdhs3fMkprkJhCzEklsSU2AbRlSFbare5bEooWId+zfWOkEHvY2GqJVRTQs9HusaNav4tk/ea1w/dY1ovuIy9pvW1t4zDAxAIAoFqw4bf7zasL9xjWv8+lzhSoB3vE2NiHQSXhmyJvYZSkwLZy14fdhvXrwgZdA8LU03h5BOq+bdMrjZtyONk3B7Terz/f8u7eCAXgX2WTb+oNq+ftse0/gyz+JE92UvA6wyxJx/I5d9QzUvvg2uxkdxn3hQbKuae1qWaIoFmFRNXVPNv2bzXtL5qr2m94DL2mMuSbxmHByAgIYGa2A0/2mMsS9tjLKvbYyy7wSVm1GoH+eCmL9LINxK6OqRT7TWVlarVJ37t27j+rz3+RG1IvXB7caopftml3Lpadcu6vcays5yM436L8Jbj8CBoBOqi1v/9XnNZbrWp7GNOscLJFrdvzGW55KugCUOmhfaYSgdx8o0nW6pN67fKhFKSaammeLJBVceNZWfdYOg7uHuNZddVtXkPXRV+fUwSzWMSnU5XbSl9YK+xbOFe0/pPOcUIc1s+JZ+R79QqYvo7DHuN6z9n7qdbd52rjWVJSvYV1RRWvjCWXXf//sYec+nD1aYywWgo8qskShY39tadwG5D0S+rjWXLq82lXzCKDU5x7tkW8p2xbDn5sruHlf9Mtblstya0Zyy7bn+s6G+V7pFqU9mfOfmDar+u2lxm5mTUPmNpvdKFhP0pl8ABy8aH9hlL11Uby65zigtN22Isu04+Jd8qV3ndd7Y3fH2EFvy211i6prv1ynuGagsrf5jLzLoDprKB+0ylgsvYbyxdrTzpYEdKJ3DAUPafB4yl5ftMZV9ziQXY8d28VvY1+Zh8rXQ9fru/fabSas5+rDaWfHbQsE5xP//7Lf+u/6fawskXVPt1+02liZyM2m8qmdjVaXgMAn0R2Gsu/s1+Y2nFfmPZN5ziALZ8t/jf/jf5mnxOvu9LG0o4ttey9v+rNpbcYOzPOUrg7M0eqLZw8gPVft0+U+kITkbtC1+HPwHsjZo1fg59ZWyfsSRvn7H0Oiv9M7qbJ7tfyPfGkjylf32Q3r6QnUVodPN/9v2h8G/UkoqotjDzwwjdfmPppP2mEsFl7DWWPKgWQWGfoSGw31z67H5TyRUumocdAeevK6SJ0KjR86p7Itf/835jyefc/LzPsG6CZ+uVcwbVFlY+MJZOogZgxn5jieAy8KdFlRMwStvJgbCNDx0wrtvPReuwQ9q8RdogjShNt7SffYaSKaz8bSppUPIP//SkAaotrHxgLJ2hqzGV5B8wlggOo8ZY8nlPjsNz2iZQbym9/4Bh7cwDppJrHHQOG2TMV6QRw9qZpBmlRc1+U+kmFr43lfz/+yzFqvwJZ6oxLHxgLBFU+3U1xpLVB4zrBIdRY1j3ntKCFvsJLYEDprUD9xvXXeKgb9gQvDzl1oxp7cDQqvfO1d2NrHHdCVXrwLTuWq2huN+dlqnnX1RjVM2/S62n2q87YCwp52LQfuPai+qREnYqJ4Fq/dpfHTCVVHHRNuwIXvG/g7WppIq0JKdWfZn7gKno/6kxlvzpjj12SerKf35tqi/2Ku1cqjHKZ+xtrJSU62oMxbtrjGsFi2EodipNMNhP8AkcMKwbVWMo/oyFprnEpprtMBR/RpoKvpJ7XrHGWPxYjXHd52rT9wHj2tt/ga5n02VesS4AACAASURBVBT/bI2h2Kk27r3u11C8W3fQUFzf6wlqC1pD8WHFKwgblI0A/V73AcParWz0rLb4Y75f0pZS/s7IQdNaQ42h+D0VaX22EOIu2YI/SBPXGIoPq4h5ny/sqfbraozFjoPGtYLFMK2tDpIOsIzCCOwzr32kxrT2Agsdc4lHhnaQxkhrSpB/raHolzXGdS4la57uxB0wrBuiBF5S7OGgaW21knn7sjeq/bqDxuJzB43FgsOoMa7dIoWTMYe6CNSairNrjMXXOGgYNig/F5HWSHNKiJKa2Pwf1RjXVCpRNzXG4itKaZak8hXVGCWy9nNP56gBeNfPixXXNNQY16jij0pIJUatz0O/InbQULyNi35hh/KL/x0+MhRvU8Iv2dGt9Rrj2tkHDWu/uWN/IXxhV2MsPlr3h+J/4pajqMYohbEE+3hXV2NYc/WgcY1gMhZxExzs6ZnAAVPJozXGNReZ6JZL/GnODtIgabFnlQb32YP6Nb87aFhzMJQxUWMsulJrXJ0mbLa7g2t9cFY7aFyzKJR8pVybar+u1lD8jZSThnKuOsPqmcGRAVYJJYGDxqIxB41rroVSa1ibzYsGKZqWa6TJUMZE17UP6otiDxrWuIKp0Vpj8UcH9UVTlfgDSl3ZBPqYakwwucq5FtV+Xa1xjeAyaoxFtkAdjOuVS4But9YZ12znolfYwSf3kC9Jm0p4S4AimF6B0yvxWtOaC3LqrM6w5mqtoWjxgbC1P1Nu5pBuZ1Rj5OQZ7LnRAEinDcwkI4H6x1c8UGcocgU7QLAeryIttz9Jo6RVGUPB56lrw4r+q9a4+uU64xqHRPa/W2coWn0ofM2AfQ8Vft/nDan4AjQACr5jgDsAKo6sPrZ+IGztQ7XGoksSJS82d7zAQ6nNSdEl0mwfkg7ZIfoN/hr9mtHuO2mGNW8epPfsDUXXe9JSnWHNN7XGovdrjWua6gxF++oMRXMOha39Hw7f5/fXAewagLrOW1d0+0r1oxZvAfira8VeV2te+0itoeg9DvqEDerPMd76kDRL2lVsYHXZGBX0Y/pVP6cPEdaFre5/yLxGv7/fqn9t+639vi6n4aFOp6Ma460G1HCers5YJLgMNAC8YvSQuTjykGHNVS76hB18co03viTtkoZ5RaW2relsAPjoWHfIWCS4DDQAfIKzzrR68CFj0TUu2oQdfPKMj768RlrmE5natoRqjI/+V3R91R0yrhZcRq1xFb4FwCA+D5mKsg8ZVt3gokvYwSfH+OVL0rKpSBG/HMggPYTUBKoxfmlAoXUWDUBI5YTFv0ugXl80g1OAwRaNF/8uiZ+0/V2949/qIsCuAXjNsFpwGYf0uAOgrnC6vVv63vJr+qKVXLQIO/jkFUl9qS9ayfVX8m5HM99HVGMk1UOI66/uNcMqwWWgAVBn4FFCrNev3MJFh7CDT06Rw5ekdTQB6sxVnQ0AH33r6g2rBJeBBkCdQfWafuVKLhqEHXzyiZy+JM2rM1q1vWuqMXLqIthz6+oNKwWXcUi/Ah8CVFl81utXzeCiP9jBJ5cExZf6VfhMgMryFdWYoGgjSHUZDYDKBMhpu4dMq7I5BRNsQQPgqwYoBjjFNHdb2DUArxtWCi6jHncAVBN/9foVg1/Xr7jBRXuwg08eCaov9StuUCyoJnA1vlGqMUHVh8z1Wfe6YYXgMtAAqCM639Cviqw3rLjGRXewg08OCYUvKRYoJtQRvdreZWcDwEfvaAC0reegW/+GedUjr+tXXA1FosWafBIXO1/qV1yl2Ah6QGJBnwigAVDwHQPcAfBJy0E/uT5sxUNv6Fe8xy55KzgmwFo9TQ/FBsVI0AMTC3pNgGEDsFy8buAx6vXL8C0Ar6Uc3BPpb6S/YVh+iYvWYAePnKE0P1KMUKwENzqxmrcEqMYoTTOB7Ef3hmG5YDPQAHir46Cet+8PhX/zumGZi43OOMUMbFFc/qNYoZgJapBiMa8IvKFfZuOUx3RvGJYJNgMNgFciDvZJh43Lt7PRGKd4gS2KzX0UM8GOU6znmUBnA8CnZuoOG5YJLoOc49mFOCOYBN4wLh/DRV+wg0+uUIMvKXaCGatYyzMBqjFq0I63e0QD4NnnOMNPAodNyx89bCi85q0YcR4KLDTQVQOF1yiG/Aw/XCYDAYYNQKE4bOAx3tAX4A6ADKL3Z0p6D/OIvuAiF22pxg59wZdv6JedOawv3HnYWJB3WL9s6mFTYfYR/fLUw2HLBh02FUQd6bf0D4dNhb89Yij6JQ33Y/dzBVF0jvtcU2G2+1r3HIU7O+cs+FI1HJjkNIohfB7AnwwkzzVUYzjFgO6IoVCwGWgA5FG9H7MeMRRuY6MrBcbIUX3h9cP6gmNHDIWrDxuWTXzDUPDEIX3hr0W8/Xt+uMurS2huWoPWojXda+sLjtFe4GtZ8+g2rxyEk2QncERfYOOkdTQAsktGewsc1hdmcwoSJdhyVF9w44ihwHE0rHDBkbCC/jWx+T9SirJoL7Qn995oj+69yloQ+bxo8bK5pJhSir+1vA92DcBRQ6HgMsg5WhanEmw/Hlb4yBFDwTUumgqlHcf0BR1HDQWFx+m2vHHlT5XgX2/2QHulPdPeO23gk2NCpQeKKYotb/jjHPkIUI0JlQbkWFd3VF8ouAw0APIJ35uZjxryfnIsrPACFz2Fwo5jYYUfHwkrWHtMv9wshLjLG+5KPodsIFvcNoUVfhwKplzWdMeWIe8nSvY39725GwBGNVN3TF8g+IwluAMQwgg8pl+6lY+WghoXXx3TF+w92m9pQv2/2u4PoQtlXZpsIxvJ1mP6ArKZUe4Jli1Lt8rqJEzeJ4Fj+iU2TrpFA9Cnu3HQWwJvGgpGcQqMoNgStvRPR/RLJx/T5//cW85cziOb3baHLf1TUFgzajYo1rjoQG12MGwAlopjei4DdwBCEVDH9Mt/dSys4DM+OpI3Ho6HLW0/HlaQ1fZb232h8JeS1iQGxIKYQD9e6s4da8t/pSQ/amUvnQ2Al35SQV3VHdcvFVwGOUcrQlSSncf0S6q4aEhOO46FFZw9pl+SWm+x3aMk/ylhL8SE2BAjOX3AZW6KOSX4TWt7oBrDRUNkBxoArSlYYnuP6ZcM5BQQcthyzLDEdUy/ZLCw2e6WGD+76YgRsSJmcviC05wUe+wEoHCDGDYAS8RxPY9xTL8IdwCCGED0oa7j+iWXuOhHBjs+OtZvSTYKv++idDcC/ZZkH9cv+UgGv3DJeZc4f2jUd9XIfwXVGE561HEyBg2A/AHQdYUTYUtnctKPpLYYlpRp8cN9XfUhxWNieNywpExS3zB5wUNMKAal4Iw5vCPAsAFYLI7reQw0AN6JWIqzjoctfuiYfvE1LtqRyo5jYYtajhoWm6RgjDluEyCmxFYqP3GZxx2DYYsfuk0Kj+Qk0NkA8KiXFAO6N/WLBZeBBkBO6d8595thi/Zz0Y00diz55IR+8WR8wO9OnUj5L2JLjN/UL/lEGp8xyX1hi/ZLyRlz9U6Aagwn7aEB6N3XONILgTf7LXyWUxAEastx/WLniT8seLgXXHhaYgLEmpgH6jdW1/db+KzEmDFdDwTYNQAn9IsEl/GmPh8fAuxBtFI+1RSb/6MThkVXuGgmUDtOhi1chQ9iSakw7+Yi5sQ+UP+xud6w6ArFpnf0cJa/BKjGsNGMfpHQcTIGDYC/svb+upP6RXmcNOOvLW/qF1491W9RgvfkcKYcBMgH5At//cjpOopNORhjztsE+DUAYQvFCSYDDcBtocrx6M1+eb85GbbwOhe9+GvHm2ELG+lDkHIwxpy+EyBfkE/89SeX6yg2KUZ9J4grvCXgbgCY1EvSve6kfqHgMtAAeCtj/847FZZfwUUrAdhRfOGhwu/7RxBXyUWAfHJSv7A4AL+yyIMUo3Ixxrw6HdUYThpDAwBVe0XglGHxf54KW/gNJ/H7asvpfvkzvIKFk0JGgHzkq185nU8xSrEaMgcwX5hdA3BKny+4jJP6PHwIUKYAPBmWX85FJ77acTos/8ZJw0L8BTaZtCX1tOQr8pmvfuZyPsWq1EwxXycBqjFcdEJ26DgZgwZAnjCl91hP6vO/5qQVH2y5dlq/4Dl5yGJWuQiQz07p86/54GdGL4Tyv8ZnVORRFr8GICxfnGIyTvbDHQA5ZH8ybOE6LhrxxY6T/fL/+ma/fIscTDGn/ATId+RDX3zO5VyKWfkJa28FqjFcNEJ26E6HLRBcBhoA6QPS8djSXzr6LbjORSPe2nGq34L/3fg/i/6X9EQxYzAJkA/Jl976nct5FLMUu8FkrYW1qMZw0QjZgQZAC6oNwMZTYQuWcxK8N7ac6rfgryj+AYhGYZfebAL+6o3vOZ1DsaswV6h+O+waAEdYnuAyTvebhw8BShhipx5f8MDpsLwvuOjDSzuune43D7f9JdSREqYinzrC8q55qQEWOZFil2JYCfy57IFqDCcN6TgZgwZA2jBz9MtbyEkfHm3pl3fjtH4ePvAnrYwUMxv51tEv74ZHHTB6UUQxrBgHMNgIGgAFBwcaAOki7OT/zP17R7+8TzWVLA15+KqfdBJS5EwOQ94oTWm6X96nFMuKdIYKN8WwAZgvHGE8BhoA6SLqdL+8XC668MaOhn7z8CM/0slH0TORr73RBJdzKJYV7RAVba6zAeBRL0nfOi4iJzvQAEgTSfRXxRz95n/MSRt92tJvXrE05DCLWgg4+s0r7lMTTF4UuW3sN/9j/KVAaZTJrgFoCJsn2Ix+c/AhQAl03hg2P42NJjzruxG/7S+BaFQ2Bfm8IWxeo1Z0TjGtMhcpcrsN/ebYOGlGx8kYco4iVaOyTTWEza9jpYtem4D5VxvC5uGv+qlMn1Jtl3zfEObWAJ8XQb1rvU4qblqeBw1ArwJTwJ0ENAABx2bTH/J+0dhv3g1NNAD95iUEDAwTqJpAQ795CVrQOsU0xbaqnaWAzaMBUHIDoMfvAAQaI41h86dpISE6wuatCpQVrudBgLSgBc1TbPPwWOisaNDPw1sAig0WNAABR0ZDv3lnFOtfiZrPRv18Z4fFdn/AsDABCwKkBdIEd91TbLNwWAiNQAMgURKWJdjQAAQUGo2G3N/L4hdFaWbuJ84/zH44IFC4mB0B0kRD2NxPuOufYpyd84JoEL8GQD9PNPAZ+BBgAMHQqJ9byEgLPeraoZ83OQBEuJQxAdIGd/1TjDN2oeymuRsAPvVS6Br0cwWfgW8B+BsBjseK7nXo57zPRwvdde0Im9siLLZ7/GWE63gTIG2QRljHgH7O+xTrvD0pn3UN+jk2TvpAAyCfVlQ1c6Nh7tOchN2jLYa5JlU5BZsNOoEGw1xTj9ph9EKJYj3oYJksyK8BMMwVDVyGEXcA/I2zBsPcCjY66EHPDsPcMn/Z4DptESCtcI4FinVteVQ6axuMc2yctKHjZAw5RzpXa2em42GLf9aon/slKy3c0QTM+eiY3vZz7XgUlgZCgLTSYJjzEdd4oFinmA+EkVavZdgAzBENBiYDDYBfcdlgmDuSjQZ60LJDPzvbLzC4SLMESDOcY4JiXrPODcDwzgaASb00zBE6ViJHA+CXtBuMc3ay0kGXJqDRkOsSNtvdfoHBRZolQJoh7XCNC4p5zTo3AMPRAHRJrooLDjQAPkvbneiMcz5UnC8l0tlp/dzBPkPBBSCg0+lIO1zjotE450M0xr7LHA2ARIlZlsBCA+Czoh395jwqiy8UoBOHfs5ZJDmfJYELbhIg7ZCG2MZHvzmPwtm+EeDXABhzRQObMRsfAvRNzzqnIXcKH//fqeVGfW6qjzhwOgjcQYA0xDU+KPbvMBb/8EigwTjbxkkPOk7GkHM8ehAn3EGg0ZRbzUsDnU2Aw5Tbjh/9ucPV+IcfBNw/DmTKbecYIxT7fiDR9CXsGoBGY67gMtAA+BablNychtyrXPzf1Y4G45ws32jgbBDomQBpqau2uDym2EeT3LPPe3uWagwX/5MdukZTruAy0AD0Jtuen28w5Pbj4vuudjiNuX9q+63tvp6txrMg4BsB0hJpqqvGuDymHOAbDW2f7W4AGNVMXaNptuAy0AD4FpxOU+6LXHzf1Q6HcTb+4I9vUsDZHgiQprpqjMtjygEeTMfhLgQ6GwA+NRMNQBfnau1ho3F2LZdE9q0dDabZX+FX/7SmZPntdf86oGn2V9/qjM3/jbNr5afHZwV+DYB5tmhkMhrM+BCgt6F24aHC7zeacj/n4vtv7XCacvd6ywDngYAvBEhb3+qMzf9NuZ9TLvCFg5bPpRrDxvfm2ULHyRg0AN6HZqNpVjgn339rS4M5N8F7CjgTBLwnQNr6Vmes/m+aFe49BW2fybABmCUazTxGg3kmvgboZXwSKy5+72LHxx0W2/1eIsBpIOATAdJWo3nWx130xiJ3Im96LwNueVPHScwQsg9CNs2s4+T7TltmrvWeAM4EAd8JNJpnruUWNw2mmXW+k9DmFWgAFHy3AA2A90HZaJr5LrdE1hhpM3tPAGeCgO8ESGPs4sY0813fSWjzCoYNwEzRaOYx0AB4F5RHDVN/wsXnXezoEELc5R0BnAUC/hEgjTWaZ3Z00R2L/Ek5wT8i2rqqswHgUS9Jw7rG8JmCy2iw4DMA3oSjM3zW41x8ftsOW6E3tuMcEAiUQGO4rfC27njkT8oJgXLRwvVUYzj5Hg2AFlT7HRtdFlsKJxGTLU6LbdB3zMQ/QUAWAqQ1bvFDOUEWWMwmRQOg4DsGuAPgXbQ1mG25nBKY0zzzxhHjvJ96Zz3OAoHACJDWSHOcYohyQmBUtHE1vwbAYhONTEaDxYavAXoRh43htu1cfE52NFhmOrwwG6eAgGQESHOcYohygmRwGE9ENYaT33WcjEED4F3kOcNtLZz87gy3LfDOcpwFAtIQIM0xi6EWacjwngUNgILvFqAB8Bx8wma7uzFi5jVOyavJYuvv2XKcAQLSESDNcYohygmUG6QjxHMmfg1AhE00MhkNkXgLwFPYnTbaHuTib7cdFtv1ptgpP/JkN46DgJQESHONFtt1TrFEuUFKRhznohrDyee6xogZgstAA+A55JwRtie4+PumHcc8W40zQEB6Ao0RM45xiiXKDdJT4jVjZwPAp2aiAeClT4/WNIS/OpFT0mqMsK32aDROAAEZCJD2OMUS5QYZMLGaEg2Agu8Y4A6A51hriJhRxCppRSBpefY6zpCDQEMEr2aacoMcnDjNiQYADYCq9dwQMeMQpwYAty1VLUdVb57b22mUG1TtkCBsnmED8KpojOAxGiyv4ncAPARBQ8QrDi7+JjsckS//2oPJOAwCshAg7XGKJcoNsoBiNCnVGE4+1zVGviq4jIZINACeYq0xcsY5Lv52Rr36pYi3f8+TzTgOAnIQIO2RBrnEE+UGOThxmpNqDB9/vyrQAHBSpxe2NEbNeJePgF8544XJOAUEZCPQGPnKGTbxFDUDfxbYg1IYNgCviMZIHgN3ADyoV6fTOSNfvsrH3y/v9GwxzgAB+Qg0RL68k0s8UW6QjxSPmTsbAB71knSra4x6RXAZaAA8B5kz6tUbXPztjHglz7PFOAME5CNAGmQTT1Gv3pCPFI+Z3Q0Ao5qJBoCHLr2ywvGU7YdckhXZ4Yx6ZapXhuMkEJCJAGmQU0xRjpAJFYtp0QAouPvBHYC+Y+xE1Iv/xClZNUa/nN23xTgKAvISIA1yiinKEfISU/fs/BqA6JdFI5PREP0yvgbYR3y5v7bExNekWVfMS6l9mItDICA7AdIgl/xJduBrtX1LhmoMJ3/rOBmDBqBv8TpjXvwdJ387o6YP6ttiHAUBeQmQBlnFVMyLv5OXmLpnZ9cAOKNfFowG7gD0EV+u6JeMjHwtGmNejOrDXBwCAdkJkAY5xRTlCNmhqXgBZ/TLNk7+1jljXhZsBt4C6DO0mqJe7s/G1zEvi6aIV/7Qp8E4CAIyEyANsoqpqJf7y4xM1dO7GwBGNVPnjHlJsBloAPoMLmfMS/FsfB3zkmiMsv22T4NxEARkJkAa5BRTlCNkRqbq6TsbAD41Ew2AquXo2+Zd0S9nckpWbdEzfukbAZwNAtISIA1yiinKEdIS4jUbGgAl3zGIxbcA+go3NAB90cExEPCdABoA35mp+Qpn7Ms2Tg0frzsAaAD6jC28BdAnHhwEAZ8J4C0An5Gp+gKGDcCLwhnDZMS+iG8B9BFenR8CZOLrmBdFUyw+BNiHu3EoCARIg2zyJ8UUPgTYp2qcsS/aOPlb54x9UTAaaAD6kK+r/0tGRr7G1wD78DUOBYeA+2uAjHIo5YjgkFPnKu4GgJG/0QCoU4d+7Zp+CIhTA+CMxQ8B+SUEXCQZAdIgq5jCDwH1qQ00AMrufnAHoA/50s98ckpWrpjp+CngPvyNQ/ITIA1yiin8FHDfmuHXAMRNF04uoz8+A9CXfOkPfbDxddx00ThgOv4YUF8OxzHZCZAGOcUU/hhQ35Jx9n/RxsnfOk7GkHP6dp+2jzqeyvohK3/HTcefA9a2pENuvTNu+lROMUU5IuRQFbwBNABKvluABsBj6LjiXrzBKGHleTQYJ4CAjASccdPzuMQT5QYZUbGYml8D0H+6cLIZuAPgKcpc/adfZePvuOk7PdmL4yAgJwFn3PSdXOKJcoOcrDjM7W4A2NTL6ULHRbyddkzDWwAeoswZN+1dNj6Pm3bGg7k4DAKyEnDGTTvDKJ7elRUWg8md/afZ2Pi7PxoABpL0zQTXgGnnuAjY1X/6lyLe/j3fCOBsEJCGAGmPNMgmngZMOycNGb6z8GsABkwXTi7jCdwB8BR6jf2nOdj4e8B04Rjw/K892YzjICAHAdIep1ii3CAHJ05zOp+YZuPkc51zwDTBZqAB8BhrrgHTDrHxN2m3//QnPBqNE0BABgKkPU6xRLlBBkyspuxsAPjUTDQArOTp2Rhn/2lFnJJWQ/9pEz1bjTNAQHoCpD1OsUS5QXpKvGbk1wA8MU04+Qx8CNBDvLmTFh9/i8Ynpq/2YDIOg4AsBEh7jHKnQDPtWSbuBoBR/tRxEjA5x7MLtX2G+7YlIwE7n5h2TNsehfWhIkDaY5U/8XaaRykxbABeEM4nuIypaAA8SLh54LQH+fjbrdvrTalTfuTBbBwGAUkJkOacT7xwnVMsUW6QFBLDyZxPTLVx8rmOkzHkHIaak9QkYbPd7XzihWuc/N70xPP9JYWEyUDAAwHSHKcYopxAucGD2Zo/jAZA0XcL0AB4E6GugVNbOCWvpiemLvDGbpwDAlIRIM1xiiHKCVKx4TwPGgA0AKrXt3PgC9s5Ja/Ggfj+supFqTIDSHOcYohygspcEJLt8msABk4VTi4DbwF4FRTOgVNz2fh84FThGvjCjeaB037qlfE4CQQCJEBaI81xiiHKCQFi0cTl7gaAS70cOFXoWIkYDYBXQega+EIKK79TQD41dZBXxuMkEAiQAGmNW/xQTggQiyYuRwOg5O4HDYBXQeh8atrjDBNYoVfG4yQQCJCAa+ALhdzih3JCgFg0cTm/BuDJqcLJZ+BbAF6E4dGnp/6Ekc879TtwaocQ4i4vzMcpIOA3AdKYc+DUDm7xQznBbygautD55FQbJ9/rnE8+L/iMKWgAvAxG55PPv8vH750abnx6stlL83EaCPhFgDTGLW4oF/gFQ4MXOZ+cYuPkf53rqecFl0HO0aAm/TK56cnn67j4/bYdU9f6BQMXgYCXBFxPTV17W288ciflAi/N1/xpVGM4+R8NgEYlzU3I7qB8curHHRbb/Rp1KcyWmQBpy/Xk1I85FQCyBS+cvBcOt7ypcz01RXAZELL3Qm58amI4F7/fYceTkxK8p4AzQcB7Aq4nJyXcoTUmuZNygfcUtH1mZwPAp2bqXE9PEVyG82m8BeBteF4YkPN919OTP+fi+9t2TN7rLQOcBwK+EHA9PXnvbZ1xyZuTP6dc4AsHLZ9LNYaTBtAAaFjNTU9PqeUkZrLF+fSUr479ccrPNexWmC4DAdIUaYtbvFAOkAEX2ynRACj4jgHuAPgWd86nJr/ILaGRPY1PT57sGwmcDQJ9EyBNcYwVygF9W46jXQnwawCemSxcTIbz6Un4FkBXtXp43PTUpH5cfH+HHU9P+lNbvO0+D+bjMAh4RYC05Hp60p/u0BiTnEk5wCsIOMlNgGoMJx3oOBmDBsC3KBUW2z2upydf5aSBb21xDpqc5RsNnA0CPRMgLX2rK1b/f3ryVcoBPVuNZ3sigAZAwZ0vGoCeJNv3c85nplSzSmq39dmO5Na373HUMwF3k/zM5HaOMUKx75kAzuhKgF8DMGiycDEZzkF4C6CrWL153PTMpClc/N+DHaneMMA5INAbAdegyak96IpFzqTY781uPN8zAaoxnPSgcw2aJLgMNAA9i7avZx3PTniUi/+/a0fTM5POCpvt7r7sxzEQ6I0AaYc09F1dcfk3xX5vtuP5ngl0NgB8aiYagJ79rJlnKcm5npn0IZek9l07Gp+ZMFgzzoShkhIg7XxXT2z+/cykD9Ec+y4XNAAKvmOAOwC+C5qucA2atJNNYvuuPv840YVE558utHyVuzH+40QX27gYNGmnlv3rr+1oAL6bYBX0bzQA/sna9eykkYwTnWgaNCnbPzK4SqsESDOcY4JiXqu+DcRuhg3AROEaxGM4B03A7wD4oe7jcRN/1jRo4pdcdNCDHR/h1wH9EIZGLyGtuAZN/KgHHbHIlRTrFPMadW9AZlON4aQLneuPEwWX4XwWDYC/6nb9cWIFFx30ZEfTHyeW+csG12mLAGmlJw0xeq5CWx6VzlqqMYx0INAASKcNVc/UFD/paU7C7smWpsETTap2EjYvOwHSSE/a4fQcxbrsIJkugAZAwXcMcAfA/6hzZGXd6xw08X1Oia67LRNa8ONA/muE+5XuH/3544SW7rphdJd00MT3Kda5+1Iu+/g1AM9OEC4mAw1AYLJ3l3o0NwAAIABJREFUDZpQyEULvdnROGg8/lBQYDJhezVpozfdsHl+0IRCtg4MgmHuBoBJvSRN69gI+9kJAg1AYBHQ8tzE33PSQ8+2jP/E+dy4hwMjhau5ESBNuJ4d/0nPmuHzIolinJvvgmkPGgAFdz9oAAIPhaY/TjjDPQk6nxvv7LBk3B84LczAgQBpgTTBXfcU2xz8FUob+DUAz40XLibDOXgcvgYYYHS4nhs3jYse+rZj3KoAUeFyJgRcz41b1bdWuOTIcdOYuCxkZlCN4aQVHSdj0AAEHhdNz+b8oum5CTc46aJXW54dnxA4McygZgKuZ8cn9KoPJi+MyD6KaYptNftKCXtHA6DgoEADIE2IND03vk4LSdH13PirDfGjH5KGGmZRGwHyPWlAC1qnmFabf5S4X34NwODxwsVkoAGQJmSa/jg+jYsmPNrx3LjGCwNyvi8NOcyiFgLkc9dz4xo96oNJbqSYVotvlLxPdwPARBOkfZ1r8DjBZaABkCZ0mlJTf+QanPMxF114ssM5OKdYGnKYRS0EyOeedMHneM7HFNNq8Y2S99nZAPCpmWgAlKy2EO7NGT8ul08C9BywTYPHzgghbiwdRALkay1pm2I5iHhZL8WvAYgfJ1xMhnMIvgUgVfSd/OOYv3fFj/+Uiza8smPIuFFS8cM8yiTgGjJulFdaYJITKYYplpXpDfXtimoMJ/3oOBmDBkDagHINzlnISR+ebGmKH3+j6bnxz0lLEbMphQD5lnzsSQesjg/OWagU/hz2wbAByBGueB7DOWQsfgdAwig7FT/6AdeQnC+46MMrOwbnXHMmjrVIiBFTKYAA+dQ1OOeaVxpgkg8pdimGFYCfzRaoxnDSkM41JEdwGWgApI8z55Cc5Vz04a0dziE5f21MyPlf0tPEjKEgQL4kn3rrfy7nUeyGgjfnNd0NAKOaiQaAs1olsK0tftwvXUNyrnNJit7a0TQk53+jCZBAQCGegnxIvvTW74zOu06xG2L87JZHA6Dg7gd3AOSJN9fgnHWMEqMvd7z+ircD5NFUMGZ13/bX4Ct/d6wOzlkXDMZaW4NfA5AwVriYDGciPgMgR0C6fzFtyJivuejEFzuahoy91jRkLD4YKIewZJyTfEa+88XXbM4dMuZr/MKlPOKiGsNGJwljhY6TMWgA5BE9zdqUMKack1Z8saUpIeeGa8hYfEVQPnlJOjP5yu0zJi9sfNEqnUuxKilQTHaLABoABQcVGoBbOpX8gStpzH82DRnzja/JiNP5TUNG48eCJFeWtBOSjzhpzldb3DGaNOY/paWK2b4lwK8BSBwrXEwGGoBvZSrP/10JYyu4aMVfO5qScorxtwPk0Vcgs5JPyDf++pXNdQljKwLhiGv7JuBuAJjUS9K8zpU4RnAZzsTR+B2AvvUb0NGGhNG/cSWOuc5FL/7bMbYR77EGJCVJL3Z/RiVxbKP//mSTA69TjEoKF5PdQYBqDCedoQG4w734hycCroTReZwCwG9bEkZfdSWOTvDEC8flJUA+cLl9waaI+/+CLGF0nry0MTsaAAXfMXBacQdA7hB1/6XAxDFX/C6cCtaPPzY1JY5e1ZGRcb/c3DH/nQSIObH3x2dMr7mCv/h3p0bk+BfVGE764XUHAA2AHJrvNqcrYfSznIIgUFucCaOdTuvIh7uBwhOyECDWxDxQv7G6PmH0s7LAxqR3EGDYAIwWrkQeA3cA7tCqrP9wJY7ez0U3ktiRMPoTV9KoycJmu0dW8BqenNgSYxexZpKzJLJjv4ZlEVTTOxsAHvWStKdzJY0WXAYagODFQkPK6IeakkZf46IdCe1oaUrJNgXPE9pYiZi6kka3SOgnFnmPYpBiURsqCL2V7gaAUc1EAxB6Tal2B81JY2YiIffcQDclZZc1pY76uWqdq5CNE0NiCZ31rDOKQYW4ShPbYNgAZAtXEo/hTByFrwEGMQzpg1iupOxLXPQjgx0fNSWMyhY2291BdAuLpYgZsXMlZX8kg1+45LxL+ABqcOVONYaTHnUua7bgMpxWNADBDQedriUlayAX/chmR1K2qylx5GA0Ap7V6S78iSMHu5KyXbL5g0nOo9jzTBRnSEmAagwnXaIBkFIdGp2ryTqqilNQyGZL0qizLuuoVHxQsHuguD/gZx2V6nIz4vOiRC4tUcx1p4hn5CaABkDB3TPuAMgt/57nP2Md8yuXddRnciU7hvO2N1uzs9ri4+/rmah2niUGxMJlzW5n6GeZ7q6O+oxiTjsqUY6l/BqA5FHCxWSgAQhdoLiSs0dx0VEQ7fiTKzlrshY/LEg2k+2u5FF/CiJvJrkuG3+ZMkSpzt0AMKmXFHc6TsGHBiBEUXFzWVdy9lZOegqWLU7rqK9cydl7XckjEzh/qMv9odHkkQlka6fNfF58BEsrFGOhjXJtr44GQMHdDxqA0AbnW0On/sSVnH0heMmQZQH5uMmavbbZOtIshLgrtB4NfHWygWwhm1zJoz6GNgLRbPYFirHAvYIZ/CXArwFIGSVcTIYzBd8C8FfYUl3nShv1SFPqqGtcNBVSO5JHdriSswubU7IHNVuzfyqVj+Seh/ZKe6a9u8gGJvkllHa4Yypt1CNy+w7z902AakwodSD12jpXykjBZThTsvA7AH3rNyhHm6yjsrloSil2NKWMvNGUPMrRlJy1oCllZH8l/eEX2ot7T7Q32mPKyBtK4cZlHxRTQQleLNInAaoxXDRFdqAB6NPdOOgvAVfKyG2cAkV5tmRdd6aMOtacMmq1KzVrotM68omW9Kxfi/j47/nrM0/X0dy0Bq1Fa9LatAdXStZ15fHh88KGYsmTb3A8OAT4NQCpI4WLycAdgOAEgTernEjO+RtnatZFLtpSix1NKSO/bErNOuNMzdrpSh2V50wdNbUpZVS2K3VUKt2Wb0ofGdWUmvWHxuSRv21LzfolDXpMz9Ex96371FGpdA1d2zlH1k6ak+ZWCwcu+6QYoljyJuZwjvwE3A0Ak3pJMaJzpWYJLgMNgPwB4MsKjpSsR5vSsq5x0Rfs4JMr1OBLih2KIV9iDufKS6CzAeATB2gA5NWL5mdvTs0ao4Zkiz3ySWpcfEmxo/kEojAA/BqAtCzhYjKcafgQoMLixb0dZ9qI7Vw0Bjv45Asl+5JiRomxrPU9UY1Rsm583ZvO1wuUfD4aAGWGJ72H6Uob6VKydrA3FHblaGCkC+/7KzOXMWwARghXGo+BBkCZQUO7OpWR8YArNesSF63BDh45Q3F+TM26RLGi3EjW9s46GwA+2te50rMEl4EGQNnB2TBs2EOutKz3uOgNdvDJHYrwZVrWexQjyo5ibe/O3QAwqpm6pvQswWWgAVB+cLrShj3SlD7iKhfNwQ4++SO0vhxxlWJD+RGs7R1SjQmtTqSNNzQA2tZzSKxvyhwe2ZyRdY1TIMEWaROTlni6YyFzeGRIghGL+kSAXwOQMUI0MRnODHwLwCc1h/DkprThg5szRtzgoj3YwSePBNOX7hhIGz44hKGIpX0gQDUmmPqQey2d3AsEc340AD4oWQGnNqUPzw6mPrAWirTiNJA+HL/xr4Bc5O0W0AAo+G4BGgBvZayc81rShs9QXFJWsMbBik8TQ9pXTiRiJ94Q4NcAZI4QTUwGGgBvJKy8c5oys1Zy0SDs4JNP5PVl1krlRSJ25ImAuwFgUi9J37qmzOGCy3BmDMefA/akYAUeFzbb3U0Zw7Zw0SHs4JNTZPFlxrAtpHkFhiK25IEA1RhZNBGiOowGwIPDcTg4BNxNQObwlZyCC7agEehBAytR/IOTU+RYBQ1AiDqVHgKp250L51DcAZBD9MGcsyVj+AxvfI1zUFzVpgHSdjBjCWtJT4BqjNp019d+ed0BQAMgveJDMGNT5vDs5szhN/oSLo6hAVCLBm5qGZ/2D0EukXpJhg3AMNGUyWPgDoDUcg/dfE2ZmYObM4dd46JN2MEjx/jqx04NZ+J7/qFLJZKu3NkA8NGyrmnoMMFloAGQVOshn6xp+PDIpqHDrnLRJ+zgk2u89OVV0nDIAwkbkIyAuwFgVDPRAEgmDUwkBwHXsGGPuDKHv+dlwmXTzMJedTcLpFnSrhwxgTlDRwANgIK7H9wBCF1gyLly27BhDzVnDr2EoqjuoqgV/5FWSbNyxgTmDg0Bfg3AsKGiiclwDh+K3wEITVzIvuqp0RkPNA/NdHHRKuzgk3e6+pI0SlqVPSCwQEgIUI3p6m+1P9ap3YCu+0cDEJKYCNqiJ3Jy/qZpaOb2rj7HY56FVJV+HZq5nTQatIDAQkEnwK4BaB6a+Y0qg62HuxbNwzJnBl0RWDDoBJqHDh3TPGzoNS66hR3qbmJIi6TJoAcCFgw6AaoxXOKVar+uafjQq03DhwoOo3nY0EVBVwQWDAmBpqysR5uGZ17koFvYoOb8k3mRtBiSIMCiQSdANYZRvF7VuUYMfbdpxFDBZKwJuiKwYMgI0O1W1/Ch25hol0sMasYO0h5u+Ycs/EOycNOIoWu45Buq/brmEUPPNY8YKjiMphFDt4REFVg0pARasjKzW0YMvcZBw7BB+bmItEaaC6nosXhICFCNYRSj53TNWRmO5qxMwWKMzKgOiSqwaMgJuEYNe6R5xNALLHTMJR452jFi6AXSWsgFjw2EhEDzyIxqPjkmw6FrHplRz8agkRmHQ6IKLKoIAm9NHfqTlqzMrWz0zLGAqtgm0hZpTBFixyZCQqB5ZMZhNvllZEa9riUrc3fzyEzBYbRkZThDogosqigCLSMzRjVnDf2Mg6ZhgwJyU9bQz0hTihI5NhMSAlRjuMQk1X5dc1ZmOReDmkdmXAyJKrCo4gicGTbsV80jM6v4aFsBhZDJCwUfNVFFWlKcwLGhkBCgGuOjfpT74jors1zXMipzdfOoTMFhNI3KeC8kqsCiiiXQkjVsYHN25iUO+oYNQcxT2ZmXSDuKFTY2FhICVGO4xCHVfl1LdmZ+y6hMwWE0Z2d8HhJVYFFFE+jIyLi/dWTmzNZRmdc46Bw2yJevSCOkFdKMokWNzYWEANUYNvGXnZmvaxuVMaMlO1NwGcJmuyckysCiiifQNjrlodbsjP1ctA47pM1bpA3SiOKFjA2GhADVFk4xR7WfvgUwiZNRzdlDHwyJOrCoagi0jM54tiU74won3cOWQJqBjCukCdUIGBsNCQGqLZzijGq/rnnU0BEtozMFl9GcPXRASNSBRVVFoGlK6o+aR2fmtWRnXueifdjhYx7LzrxOGiAtqEq82GxICFBt4RRjVPt1raOGJXIyqnVU+sSQqAOLqpJAy+jhv2kZnVHRmp3xDac4gC29NwOdvs6oIN+rUrTYdEgIUG3hFFdU+3Ut2cMGtozJEIzG6pCoA4uqmoBrTPp/to7JKG8Zk/E1o1jgFNdS2PI1+Zh8rWqxYvMhIdAyJmM1q9yQPWyg7syYYebWMRmC0agPiTqwKAsC9CGwttEZ61pHZ1xnFBOc4tt3W0ZnXCef4gN+LEI0ZEa0jsmo55QTqPbrzozLeLh1bIbgMtrGpv85ZArBwmwItGWl/rI1J31565j0L7jEhubsIN/lpC8nX7IRJgwJGQGqLZxiiGq/jr7a0Dom4zonw/B73SGLEXYLt43OeKBlTObC1rHpn3KKEd62pH9KPiPfsRMkDAoJAaoprGJmTMb1W1+Zbx2bcbY1J0NwGS1jh/4+JCrBomwJnB2T9vdtY9Nz28ZmfMwlTrjZQb4hH5Gv2AoRhoWEANUUVvEyNuPsLZBtORlVnIxry0lPvmUcHoCAhAToK2OtOelpreMy6trGZdzgFDdqtMXtg3EZdeQTfJ1PQqFjqjsIUE1RY3z0tmeq+bcMbB2Xmdc6LkMwGrNuGYcHICATgaac4b9oG5sxrXVc+hlGsaOSPJB+htiTD2RyL6YFgVsEWsdlzOIV45l5t4xry0kb2jYuQ7AZOem1t4zDAxAIAgG6RdgyNq2wbVzm+2ziSHE5IfN9Yoy3+IIgaCxxB4G2nPRaVnGdkzb0loGtE9INrIwbl/75hZyc798yEA9AIEgEHFlZ97aNTX+6NSejonV8xpe84ir4LxKIIbEkpsQ2SG7EMiBwiwDVkrZx6Z9zimWq+bcMdGQl/QMn48iWlgkZllsG4gEIhIBA28RhP2sbnzmybXz6ztbx6R9yizG57HGzGp++081u4rCfhcB1WBIEbhGgWiKX1kM1L9X8WwbSg9bx6R+0jU8XXEbrhDR8DuAOD+MfoSQgbLa7myZkPtoyLn1K2/i06rbx6Ve5xJoEdlwlJsSGGBGrUPoKa4NAVwJUSyTQuGJqK9X6rva5H7eOTzveNiFdcBmtE9KPdjMST4CAQgjQd3CbcjL6tU1Me7FtQnpt24S0z7nEnmc73LbWku3E4Nb3kRXiG2wDBLoSoFriWdPqqZ1U67va537cOiG9lJWRE9Kvt9lG/7iboXgCBBRIgN5nPDsxM7xlQoatbWJ6Xdv49HfZxCPZMjG9jmwjG/H5HAUKEFvqkQDVkFaqJbxeHJd2M7ZtYuq0tonpgtWYjD8N3M3ReEI1BNy/PjY+4/G28WkpbZPSc1snpm1vm5TecmZC+jWlxal7T5PSW27uMZf23Do+43H8Kqdq5IaN9kCgbfLQAUqLtcD3kzqtm6nNEzIGBT6xshqIsxPS87sZiidAQOUE6D3y8xOtD56ZlPHE2QkZk9ompBe1TUg7dGZCuuPMpLRzZya4X3FfPTMp/UagMX1zjqs05825HbQWrUlr0x5oL3jfXuWiwvZ7JEA1JNAYUtr1VOu7GdsyIfU3bZPSBasxMb2hm6F4AgQ0RMCR9dQPW15M+qeW59N/3fa89Xdtk1ONbRNT+rdNyohvm5ie6R7uxyn93ceet/6OzqVr6FoNoYKpINCNQNvE9AZWNXFSuqBa391QW/x9Zyamf83J2DOT02805mT+Yzdj8QQIgAAIgAAI9EGAagfVEFY1kWq8Lf6+Hs1um5Te3DY5XfAaGTk9GosnQQAEQAAEQKAXAm2TM3J41UL3Hf7mXszV6c5MTlvKzuDJ6ad6NRgHQAAEQAAEQKAHAm2T009xq4dU43swtfOptknpT5+Zkia4jbemDv2PXo3GARAAARAAARDoQoBqBrc6SPZQje9i5p0PL72Q9bdnJ6d9zc/w1Nw7LcW/QAAEQAAEQKBnAmempOZyq4NU26nG92zxzWfPPJ928szzaYLVmJJ2WQhxV5+G4yAIgAAIgIDmCVCtODMl7TKrGthZ0096dG7b86nzGBouzryQbvZoPE4AARAAARDQNAGqFRxrINV2j449Mzkl5szUNMFwFHs0HieAAAiAAAhomsCZqWnFDOufoNru0bHvTIz/wZmpqV+yA/BC6scdtoz7PQLACSAAAiAAApokQDXizAupH7Orf1NTv6Ta7pVTz05Lff3sC2mC23jrhfQhXgHASSAAAiAAApojQDWCW91z2zMt9XWvnXn2+fRXOUI4My3thNcQcCIIgAAIgICmCFCN4Fj7qKZ77ci3nk82nJ2WJjiOc1PTo7wGgRNBAARAAAQ0QYBqA8eaRzZRTffaiSIr696zL6R8enZaqmA3pqe+5jUInAgCIAACIKAJAmenp77Grt5RDX8h5VOq6T458ey01P1np6cKniMtzCcYOBkEQAAEQIAtgbPT08J41jr3i/j9Pjvu7PSUKXyBpO31GQguAAEQAAEQYEng7LS0vWzr3fSUKT477cyLmY++9WKa4Drans/4nc9QcAEIgAAIgAArAlQLuNY5sotquV8OOzs99QJfMKl2v6DgIhAAARAAATYE3nox1c61zlEN99tR56anvsoVzLmX0m689TL+SqDf4sCFIAACIKByAlQD3LWA6d1uquF+u+j8NOuDXBsAt10vpW7wGw4uBAEQAAEQUDWBt15K3cC5xlEND8hBb72UduStl9IEx3HuxdRv6NOfAQHCxSAAAiAAAqojQLmfagDH2nbTpiMBO+Xsy6kjGAMS515Mcwp7/PcCBoUJQAAEQAAEVEGAcj7lfs61jWp3wM7osGX83fmX074493Ka4DrOv5Q+LmBQmAAEQAAEQEAVBCjnc61nZBfVbKrdkjjj/Kup9nOvpAq+I+Wvbc9nPCAJLEwCAiAAAiCgWAKU68+9kvJXvvUsVVDNlswBZ19Oe5IzLLLt/Cup5ZIBw0QgAAIgAAKKJEC5nns9o5otGXxhs91z7tW0v5x7NVVwHhdfTYuQDBomAgEQAAEQUBQByvGca1inbWl/oZotKfi3Xkkt4A8u9Ywo8vGPJkhKGZOBAAiAAAjIQYBy+7lXU89wr2NUqyXnd3ZW+mPnZqQK7uOtV9NekBweJgQBEAABEAgpAcrt3OsX2Ue1WhbQ52ektJ2fkSqYjy/esiX9tywAMSkIgAAIgEDQCVBOPz8j9QvmtUtQjZYN7oUZqdPO21IF93HOlnquzTb6x7KBxMQgAAIgAAJBIUC5nHI697pF9lGNlg1qky35F+dtqTe0APK8Dd8KkE1ImBgEQAAEgkSAcrlGatYNqtGyYj1nS9lyfmaq0MI4NyslS1aYmBwEQAAEQEA2ApTDtVCryEaqzbKB/Hbis7OS/+v8zORvzs9MEeyHLQWfB/jW8fg/CIAACKiIgPt9f1vKF+zrlLsWJ39DtTko7jk3K7Xq/KwUoYVxbmYKPg8QFFVhERAAARCQhoD7ff+ZKee0UKPIRqrJ0pDzYpZzs5If1wrYTjvxeQAvZIFTQAAEQEARBM7PSi3XUo2imhxU8OdnJ9ecn50iNDRGBhUwFgMBEAABEPCZwPnZKSM1VJcE1WKfIQV6wblZKSZNQZ6V/NW52alPBMoN14MACIAACMhDgHL0+VnJX2mpNlEtloemh1nPzUo5rCXQ52elfHYhN7mfByw4DAIgAAIgEGQClJspR2upJlENDjLm28tdmJ0eqyXYnbamfnB+dupvblPAIxAAARAAgVASoJx8fjblZk29LS2oBoeSu+787ORT53NThJbGhdmpVy7kyfyDCyH1KhYHARAAAXUQoFxMOVlLNcht6+zkUyH30IVc6zOaA+9ueJLb2mzxPwu5A7ABEAABENAoAcrB53OT27RYg6j2htztQoi7Ls5Nbr4wJ0VobyQf/7PtqR+G3AnYAAiAAAhojADl3gtzko9rr+6kCKq5VHsV4fJzs6yJF+YkCy2O87nJe4XNdo8iHIFNgAAIgIAGCFDOpdyrxZpDNlPNVYybhc1298U51vMX5iYLLY7z85L34k6AYuSIjYAACDAmQLmWcq4Waw3ZTLWWaq6iXHwh1xqvVYd02m093rYYnwlQlCixGRAAAVYEKMdemGs9rulak2uNV6RTL8xLrrkwL1loeLTh2wGKlCY2BQIgoHIC7k/7z0tu03B9odoa/F/981Y3l2ZnPHxhXso1TTtorvXK+Tz8ToC3msF5IAACIOCJAOXUC3OtVzRdW+alXKMa64lVSI9fmp888+L8ZKHlcWl+8gcX8vCLgSEVIhYHARBgQYByKeVULdcUsp1qq+Id2mHLuP9CnvXixbxkoekxP/mzS3PxtwMUL1hsEARAQLEEKIdenJ/8maZrSV6yoJpKtVWxjuq6sYvzkwdo3WGd9lu/upifhL8i2FUceAwCIAACXhCg3Hkxz/oVaon7jvoAL5Ap55SL+daKiwuSBUayuLTAWt5mG/1j5XgHOwEBEAABZRKgXEk5E7XjZv3Mt1Yo01N97OriosR/ubAg+dOLC6wCwyou5Seda89P++8+kOEQCIAACGiaAOVIypWoGZ11011DFyX+iypFcTE/5flL+VT8MIhBe771i/b8xBGqdCY2DQIgAAIyEmhfaM2iHIl6cbteUg2VEbm8U9PPNV5akNQKh952qJvFAmv5X/CWgLziw+wgAAKqIEC5kG75o058t04ktar+Z+bPL7Sa4djvOJbuiCzEWwKqyE7YJAiAgGwE3Lf8FyadQ43oXiOodsoGPpgTt+cnrYeDuzv45u2uF0RR1r3B9AfWAgEQAIFQEqCcdynf+gJu+XevC1QrqWaG0j+Srn0x/48/v7TQ+sGlhfTKF+O7DC7mJ5+5mGeNkBQ6JgMBEAABBRKgXEc577t5EP++VRs/oJqpQNf5v6X2/KSn4OBbDu65EVpkLe9YkPGA/5RxJQiAAAgokwDltkuLrOWoA33XAaqVyvRggLu6tMi66NIiq8DoncHFRda/XlxkHSfs8d8LEDcuBwEQAIGQE6BcRjmNchtyf++5/yabRSF3mFwboPd92hcmnWhfZBUYHhgsTnZeWJIWJpcvMC8IgAAIyE2Aclj74mQn8r2HfE81cWHSCfafB2tfaP1V+yLrh+2LkwVG3ww6Flu/aV+cvKE9L+U/5A5UzA8CIAACUhGgnEW562YOQ673VO+oJi60/koq/oqep2Ox9Zn2xVaB4R2DjsXWG+2LrPaOJdbfKdqx2BwIgICmCVCOcucqylnI8V7XOKqJmhJOx5KkJe1LrALDVwbJey8sScJbA5qKFhgLAsomQDmpfUnyXuRzX/O5VVAtVLZ3ZdidsMXf1740+RQE47tgiFnHYutrlwoSo2RwDaYEARAAAa8IUA6iXIQ87l8epxpItdAr2NxOemtZ/L91LLV+1L7UKjD8ZZB0or0gaYhq/lY0NxHDHhDQGAHKNZRz2pcmnUDe9jdvWwXVPqqBGpPPneZeXmJ9tmOpGwYBwfCTQfsS68ftS5KK25dazUKIu+6kjH+BAAiAgP8EKKdQbnHnmCXWj5GrA69VVPv89wijKy8XJBd2FFgFhjQM2gusl9sLknLbl+LbA4zCBKaAQNAJUA5x55IC62XkZ2nyM3Gkmhd0Zyp1QffnAQqsDghMOoF9y7LzcxZJORcK4/9Rqf7HvkAABJRDgHJF+5KkHMod3+YR/F+63NxeYHVo9n3/3mT+9hLrg+2FSe93FFoFhiwMbnQsszZcXmZB2iIjAAAJUUlEQVTNv7wseQD+HHFvSsTzIKAtApQLKCdQbqAc0VFovYEcLEsOFlTjqNZpS2FeWntpacLj7cuSPoH45BFfV67thdbr7YXWo+0F1lkXliRaLhQO+L6XbsJpIAACKiZAsU4xT7HvzgGF1utdcwMey5N/qbZRjVOxdOTf+pXC5Nj2wqTrHYVJAiOYDBI/by9Mqm0vSJx1qdCafGlZ4u/fyhv6E/k9jhVAAATkIkAxTLFMMU2xTTHeUZj4OXJrMHNrEr3yv061TS4/s5q3ozAl6fKyxG86liUJjBAzKEz6c8eyxPqOwqTVHcsSJ15eljDg7ZXWB0W97R5WooMxIKBSAhSLFJMUmxSjN2OVYvbPyJ8hzp/LkoS7lhWmJKlUXqHZdseyhPEQb+jF25cPLtMriWVJ73UUJl7sWJbk7FiWdLhjeVJ1x7KkLZcLE9e0FyYt6ihMmtlRYLVhgAE04KMGCpNmUgxRLFFM3Yytw+5Y64y5927GIF4oKfrFYsL40FRRla/asSxpXsfyJIEBBtAANAANQAOq08CypHkqL8Oh3X7HsqQS1TkdTQuaNmgAGoAGtK2BZUkloa2eDFYX9vjvdaxI2t2xIklggAE0AA1AA9CACjSwm2oXgxIcehPeWRz/g8vLE4+qwOloUtCoQQPQADSgYQ1QraKaFfrKyWgHb6+0/vTtFYmtl1cmCQwwgAagAWgAGlCaBqhGUa1iVHqVY8q5xfH/fHml9W2lOR37QSKCBqABaEDrGrC+TTVKORWT4U4uLU/498urrW9fXpUkMMAAGoAGoAFoIOQaWG19m2oTw5KrPJPOrYr/57dXJrZeXpUoMMAAGoAGoAFoIFQaoFpENUl5lZLxjuh9lsurEo5eXp0oMMAAGoAGoAFoIOgaWJVwFO/5h6jRoE9avr06aXfQnY6mA00XNAANQAOa1gDVHnzaP0TF/9tl6buWV1YnlbxdlCgwwAAagAagAWhAbg1QzcH3/L+twgr4/9tFSfPkdjrmR2KBBqABaEDrGsDP+yqg5HffQseahPFX1iR8gwDVeoDCfsQANAANSKsBqi1UY7pXHjyjGAJXipOS3i5KvA7xSyt+8ARPaAAa0LAGrlNtUUyhw0Z6J3ClKCH27aKET95ekygwwAAagAagAWjAbw0UJXxCNaX3ioMjiiNwZU3C428XJb7vt9PRPKB5ggagAWhA2xooSnyfaoniChw25JnA2yXWB68UJziuFCcIDDCABqABaAAa8EEDDqohnisNzlAsAWGPv+/t4iGFPjgdzQIaJmgAGoAGNKwBqhlUOxRb2LAx3whcLk549sq6IR9dWZsgMMAAGoAGoAFooJsG1g35iGqFb9UFZ6uCwOXi1H+7sm7IqW5OR1OApggagAagAW1rYN2QU1QjVFHMsEn/CNBtnSvrhiy5si5BYIABNAANQAPQANUE3PL3r6aq8qorJQnPXFmb8CGCH8EPDUAD0IBGNUA1oCThGVUWMWw6MALta5/91TvrEk+8U5IgMMAAGoAGoAENaWBd4gmqAYFVEVytagKiKOveK+uSFiHwNRT4aPjQ8EIDmtYA5XzK/aouXti8dATeLk166kpJwgfvlCYKDDCABqABaICfBijHU66XrnJgJjYELm5I/fmVsqT1CHx+gQ+fwqfQgLY1QLmdcjybggVD5CFwZe0Q85XShNZ3ShMEBhhAA9AANKBeDVAup5wuT7XArCwJiHrLPe+sj3/+nbKET98pSxAYYAANQAPQgKo08CnlcMrlLIsUjJKfwLslif/yTmlCBQJfVYGPhg1NKzSgZQ2UJlRQ7pa/QmAFTRB4Z33CgCvrEy6+sz5BYIABNAANQAPK08DNHD1AE0UJRgaXQEep5f53yhJmXlk/5BqCX3nBD5/AJ9CANjXgzsllCTMpRwe3KmA1zRF4p9z68DvrE2re2ZAg/m97Z+8aRRDGYRQUrLSwtBJSpLDQYBW008avfNw7G1IK8Q8Qi1SaTiSNdlqF5Hb3duYsghZipRgQFKuASJJCzO7MihALEYISM/I7LEICCYm53O7trxhIuONu99nnfWYJ4Y6LDOgAHaADHXQALY5Geyq3EfGEO0sgmwkknQ4WOfwdHH7ehPEmlA5U04HpYBEN7uwuwHevNAE/MXHYzgQjy6GaT+uB5yIDOkAH6ED7HEBr0Vy0t9KbD0++OAS894eWp4MbaV29z+qB5yIDOkAH6MD+OYC2orFobXHKzyMhgU0ElsPgcloP3mSh8lxkQAfoAB3YuwNoKZq6KbP8lQSKTcDVaxdsKC85/HsffrIjOzpQTQfQTjS02JXn0ZHADgTycOi8DWXWxrKeRcpzkQEdoAN0YKsDaCRaiWbukFU+TALlIpCGciaLVMOG6g+Hf+vwkwmZ0IFqOvCviQ00slxV59GSwC4JZOHQqTSWcRvJRwavmsHjded1pwPKo4FoIZq4y4zy6SRQfgIukj4bqUculm82Vp6LDOgAHehmB1qtQ/Mi6St/wXkGJLAPBPCNVVlSu+qiwNg4WO3mAPDcuMHRgao5EKyibWgcv51vHzYMvkT3Evg8NXDChWrMxmqOoaxaKHm+dL6rHJhDy9C07i02z4wE2kTgSzR6Ok/krk3Ukm0oz0UGdIAOFNqBRC2hWWhXm7LIlyWB6hGw9eCcbag7riEvXKx+FjoCvFnhzRodqIQDaBGahDbZxvDZ6pWZZ0wCB0zAP7l1xGrVjzvtTMvrTMsvm4jnIgM6QAfa6QBag+a0/jKpVT9adMD549uRAAlsJJAaOWaNXHKx3M+1vMu1rLlEPBcZ0AE68D8OoCVoCtqCxqA1G9vDn0mABApG4LuR49bIdZfIw1zLfG5kzWnxXGRAB+jAdg6gFWgG2oGGoCUFyxsPhwRIYDcEvJGjLhnsdVoNOCPjuZEpq+WtM7KyXQz4GDcLOtClDhhZQQPQAjSh1YZksBet2E1b+FwSIIESE3DPr53E/xN81epmrtUDZ2TWmtqnXMtvZ8RzkQEdKKcDmGHMMmYas40Zx6xj5kucLB46CZBAuwngAzvSp8M9Tg9dzIxcsc1gxGk15ozczpvqnm2qSafVY2skyo16ljfllTPywWlZyI2yrln7kTdlnZtHOTcPXrfOXrfW7GCGMEtaFjBbmDHMGmauNXtNNYlZxExiNjGjmFXMLGaXH7rT7kqW+/X/Aj4L+Fm18GO9AAAAAElFTkSuQmCC" />
                            </defs>
                        </svg>

                    </a>
                </li>
                <li>
                    <a href="https://www.tiktok.com/@suaradotcom" target="_blank" rel="noopener"
                        aria-label="share tiktok">
                        <amp-img src="https://assets.suara.com/mobile/images/partner/tiktok.svg" width="25" height="25"
                            alt="Logo tiktok"></amp-img>
                    </a>
                </li>
            </ul>
            <div class="info">Dapatkan informasi terkini dan terbaru yang dikirimkan langsung ke Inbox anda</div>
        </div>

        <div class="m-foot">
            <div class="other-info">
                <div class="bg-gr-foot"></div>
                <ul>
                    <li><a href="https://www.suara.com/pages/redaksi" title="Redaksi" rel="">Redaksi</a></li>
                    <li><a href="https://www.suara.com/pages/kontak" title="Kontak" rel="">Kontak</a></li>
                    <li><a href="https://www.suara.com/pages/tentangkami" title="Tentang Kami" rel="">Tentang Kami</a>
                    </li>
                    <li><a href="https://www.suara.com/karir" title="Karier" rel="">Karir</a></li>
                    <li><a href="https://www.suara.com/pages/pedomanmediasiber" title="Pedoman Media Siber"
                            rel="">Pedoman Media Siber</a></li>
                    <li><a href="https://www.suara.com/pages/sitemap" title="Site Map">Site Map</a></li>
                </ul>
            </div>
        </div>

    </amp-sidebar>
    <main class="content" role="main">
        <article class="post">
            <div class="top-menu">
                <div class="top-menu--menu left">
                    <a class="active" href="https://indotnesia.suara.com/aktual" title="aktual">aktual</a>
                </div>
                <div class="top-menu--date right">
                    <time itemprop="datePublished" datetime="2022-11-17T12:56:34">Kamis, 17 November 2022 | 12:56
                        WIB</time>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="post-meta">
                <h1 class="post-title" itemprop="headline">Richard Lee Bebas dari Status Tersangka, Begini Kronologi
                    Perseteruannya dengan Kartika Putri</h1>
                <h2>Kasus perseteruan Richard Lee dan Kartika Putri telah berlangsung sejak 2021.</h2>
            </div>

            <div class="authors">
                <div class="wrap-authors">
                    <span>
                        Asy Syaffa Nada A </span>
                </div>
                <div class="clearfix"></div>
            </div>

            <!-- Begin Social Icon -->
            <div class="reset"></div>
            <div class="social-icon">
                <amp-social-share type="facebook" width="25" height="25" data-param-app_id="802054763141639"
                    data-param-href="https://indotnesia.suara.com/read/2022/11/17/125634/richard-lee-bebas-dari-status-tersangka-begini-kronologi-perseteruannya-dengan-kartika-putri">
                </amp-social-share>
                <amp-social-share type="twitter" width="25" height="25"
                    data-param-text="Richard Lee Bebas dari Status Tersangka, Begini Kronologi Perseteruannya dengan Kartika Putri"
                    data-param-url="https://indotnesia.suara.com/read/2022/11/17/125634/richard-lee-bebas-dari-status-tersangka-begini-kronologi-perseteruannya-dengan-kartika-putri">
                </amp-social-share>
                <amp-social-share type="whatsapp" width="25" height="25"
                    data-param-text="Richard Lee Bebas dari Status Tersangka, Begini Kronologi Perseteruannya dengan Kartika Putri - https://indotnesia.suara.com/read/2022/11/17/125634/richard-lee-bebas-dari-status-tersangka-begini-kronologi-perseteruannya-dengan-kartika-putri">
                </amp-social-share>
                <amp-social-share type="line" width="25" height="25"
                    data-param-url="https://indotnesia.suara.com/read/2022/11/17/125634/richard-lee-bebas-dari-status-tersangka-begini-kronologi-perseteruannya-dengan-kartika-putri"
                    data-param-text="Richard Lee Bebas dari Status Tersangka, Begini Kronologi Perseteruannya dengan Kartika Putri">
                </amp-social-share>
                <amp-social-share type="linkedin" width="25" height="25"
                    data-param-text="Richard Lee Bebas dari Status Tersangka, Begini Kronologi Perseteruannya dengan Kartika Putri"
                    data-param-url="https://indotnesia.suara.com/read/2022/11/17/125634/richard-lee-bebas-dari-status-tersangka-begini-kronologi-perseteruannya-dengan-kartika-putri">
                </amp-social-share>
            </div>
            <div class="reset h10"></div>
            <!-- End Social Icon -->




            <div class="headline-image">
                <amp-img
                    src="https://media.suara.com/suara-partners/indotnesia/thumbs/653x367/2022/11/17/1-screenshot-20221117-125426.png"
                    alt="Richard Lee Bebas dari Status Tersangka, Begini Kronologi Perseteruannya dengan Kartika Putri"
                    width="653" height="367" layout="responsive" data-component-name="amp:image"></amp-img>
                <div class="caption">Kronologi perseteruan Richard Lee dan Kartika Putri. (YouTube/dr. Richard Lee,
                    MARS)</div>
            </div>

            <div class="amp-ads">
                <amp-ad width=320 height=100 type="doubleclick" data-slot="/148558260/Suara/AMP"
                    data-multi-size="320x50,300x100" json='{"targeting":{"pos":["leaderboard"],"page":["Amp"]}}'>
                </amp-ad>
            </div>

            <section class="post-content">
                <div class="article" itemprop="articleBody">

                    <p><strong>Indotnesia -</strong> Pengadilan Negeri Jakarta Selatan resmi membebaskan <a
                            href='https://www.suara.com/tag/richard-lee'>Richard Lee</a> dari status tersangka atas
                        dugaan pencemaran nama baik dan akses ilegal yang dilaporkan oleh <a
                            href='https://www.suara.com/tag/kartika-putri'>Kartika Putri</a>, pada Senin (14/11/2022).
                    </p>













                    <p>Putusan tersebut ditetapkan hakim lewat praperadilan dengan nomor register
                        99/Pid.Pra/2022/PN.JKT.SEL yang menyatakan dugaan kasus tersebut tidak sah dan memutuskan dokter
                        37 tahun itu bebas dari segala tuduhan.</p>













                    <p>Diketahui, perseteruan Richard Lee dengan Kartika Putri telah berlangsung sejak Januari 2021
                        bermula dari unggahan video pembahasan tentang <a
                            href='https://www.suara.com/tag/krim-wajah'>krim wajah</a> yang diiklankan oleh istri Habis
                        Usman bin Yahya.</p>













                    <p>Lalu, bagaimana <a href='https://www.suara.com/tag/kronologi'>kronologi</a> lengkap terkait awal
                        mula perseteruan Richard Lee dan Kartika Putri? Berikut penjelasannya.</p>








                    <p><strong>Baca Juga: <a
                                href="https://indotnesia.suara.com/read/2022/11/17/113524/dijual-khusus-berikut-distributor-resmi-penjual-materai-elektronik-dan-cara-pembeliannya">Dijual
                                Khusus, Berikut Distributor Resmi Penjual Materai Elektronik dan Cara
                                Pembeliannya</a></strong></p>





                    <p><strong>Kronologi Perseteruan Richard Lee dan Kartika Putri</strong></p>
                    <div class="amp-ads">
                        <amp-ad width=336 height=280 type="doubleclick" data-slot="/148558260/Suara/AMP"
                            data-multi-size="300x250,336x280,1x1"
                            json='{"targeting":{"pos":["read_body_1"],"page":["Amp"]}}'>
                        </amp-ad>
                    </div>













                    <p>Konflik di antara dokter dan publik figur itu bermula saat Richard mengulas produk skincare lokal
                        yang diakuinya telah dilakukan uji lab dan menemukan kandungan yang semestinya perlu pengawasan
                        dokter.</p>













                    <p>Lebih lanjut, dalam videonya itu Richard mengatakan bahwa produk berupa krim wajah lokal tersebut
                        &nbsp;mengandung merkuri hidrokuinon tinggi hingga 5,7 persen, sedangkan batas maksimal yang
                        dianjurkan hanya 2 persen.</p>













                    <p>Dari temuan tersebut, dokter sekaligus aktivis media sosial itu menghimbau kepada masyarakat agar
                        lebih berhati-hati dalam menggunakannya, apalagi kandungan didalam krim wajah tersebut
                        sebenarnya perlu resep dokter dan tidak bisa digunakan secara umum.</p>













                    <p>Kemudian, Richard memperingatkan ‘Karput’ selaku <em>influencer </em>yang pernah menerima
                        <em>endorsement </em>skincare tersebut agar lebih berhati-hati dan memperhatikan kualitas dari
                        barang yang diiklankannya.
                    </p>









                    <p><strong>Baca Juga: <a
                                href="https://indotnesia.suara.com/read/2022/11/17/101850/profil-basuki-hadimuljono-menteri-pupr-yang-jadi-fotografer-di-acara-g20">Profil
                                Basuki Hadimuljono, Menteri PUPR yang Jadi Fotografer di Acara G20</a></strong></p>




                    <p>Tak terima dengan pernyataan dalam video Richard, artis 31 tahun itu lantas melaporkan Richard
                        Lee atas dugaan pencemaran nama baik dan meminta dokter kecantikan tersebut meminta maaf
                        kepadanya disertai sejumlah syarat.</p>













                    <p>Atas laporan tersebut, akun Instagram @dr.richard_lee disita polisi sebagai barang bukti dari
                        kasus dugaan pencemaran nama baik tersebut.</p>













                    <p>Ia kemudian ditetapkan sebagai tersangka dan sempat ditangkap polisi karena access illegal
                        Instagram, meski kemudian dibebaskan.</p>

                    <div class="amp-ads">
                        <amp-ad width=336 height=280 type="doubleclick" data-slot="/148558260/Suara/AMP"
                            data-multi-size="300x250,336x280,1x1"
                            json='{"targeting":{"pos":["read_body_2"],"page":["Amp"]}}'>
                        </amp-ad>
                    </div>












                    <p>Pihak Richard lantas mengungkapkan keinginan untuk berdamai dengan Kartika Putri lewat mediasi
                        oleh polisi, namun berakhir buntu.</p>













                    <p>Setelah itu, Kartika Putri tetap meneruskan perkara kasus dugaan pencemaran nama baik tersebut
                        hingga akhirnya Richard Lee ditetapkan sebagai tersangka pada Selasa (5/4/2022).</p>













                    </p>

















                </div>


                <div class="list-wrapper">
                    <div class="wrapper-title">
                        <h2>Berita Terkait</h2>
                    </div>
                    <ul>


                        <li class="suara-populer-feed">
                            <a href="https://sulsel.suara.com/read/2022/11/17/113151/menang-di-pengadilan-dokter-richard-lee-tidak-mau-maafkan-kartika-putri"
                                title="Menang di Pengadilan, Dokter Richard Lee Tidak Mau Maafkan Kartika Putri">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/pictures/336x188/2022/11/16/24442-richard-lee.jpg"
                                        alt="Menang di Pengadilan, Dokter Richard Lee Tidak Mau Maafkan Kartika Putri"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Menang di Pengadilan, Dokter Richard Lee Tidak Mau Maafkan Kartika Putri </div>
                            </a>
                        </li>


                        <li class="suara-populer-feed">
                            <a href="https://mamagini.suara.com/read/2022/11/17/110925/status-tersangka-hilang-dr-richard-lee-senang-bukan-kepalang-menang-praperadilan-dari-kartika-putri"
                                title="Status Tersangka Hilang, Dr. Richard Lee Senang Bukan Kepalang: Menang Praperadilan dari Kartika Putri">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/mamagini/thumbs/336x189/2022/11/17/1-screenshot-3.png"
                                        alt="Status Tersangka Hilang, Dr. Richard Lee Senang Bukan Kepalang: Menang Praperadilan dari Kartika Putri"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Status Tersangka Hilang, Dr. Richard Lee Senang Bukan Kepalang: Menang Praperadilan
                                    dari Kartika Putri </div>
                            </a>
                        </li>


                        <li class="suara-populer-feed">
                            <a href="https://semarang.suara.com/read/2022/11/17/094944/kronologi-denise-chariesta-positif-hiv-aids-bermula-hubungan-dengan-pengacara-s-nular-ke-rd-dan-istrinya-kita-cek-bertiga-ayo"
                                title="Kronologi Denise Chariesta Positif HIV AIDS, Bermula Hubungan dengan Pengacara S Nular ke RD dan Istrinya: Kita Cek Bertiga Ayo">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/semarang/thumbs/336x189/2022/11/17/1-img-20221117-094643.jpg"
                                        alt="Kronologi Denise Chariesta Positif HIV AIDS, Bermula Hubungan dengan Pengacara S Nular ke RD dan Istrinya: Kita Cek Bertiga Ayo"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Kronologi Denise Chariesta Positif HIV AIDS, Bermula Hubungan dengan Pengacara S
                                    Nular ke RD dan Istrinya: Kita Cek Bertiga Ayo </div>
                            </a>
                        </li>


                        <li class="suara-populer-feed">
                            <a href="https://www.suara.com/foto/2022/11/17/075019/dokter-richard-lee-girang-menang-praperadilan-lawan-kartika-putri"
                                title="Dokter Richard Lee Girang, Menang Praperadilan Lawan Kartika Putri">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/pictures/336x188/2022/11/17/30490-richard-lee.jpg"
                                        alt="Dokter Richard Lee Girang, Menang Praperadilan Lawan Kartika Putri"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Dokter Richard Lee Girang, Menang Praperadilan Lawan Kartika Putri </div>
                            </a>
                        </li>


                        <li class="suara-populer-feed">
                            <a href="https://sumbar.suara.com/read/2022/11/17/010912/vicky-prasetyo-ternyata-masih-cinta-zaskia-gotik-andai-waktu-bisa-diputar"
                                title="Vicky Prasetyo Ternyata Masih Cinta Zaskia Gotik, Andai Waktu Bisa Diputar...">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/pictures/336x188/2014/11/08/o_1966vduc01gaf18vvmf61v0d7ha.jpg"
                                        alt="Vicky Prasetyo Ternyata Masih Cinta Zaskia Gotik, Andai Waktu Bisa Diputar..."
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Vicky Prasetyo Ternyata Masih Cinta Zaskia Gotik, Andai Waktu Bisa Diputar... </div>
                            </a>
                        </li>






                    </ul>
                </div>

                <div class="amp-ads">
                    <amp-ad width=336 height=280 type="doubleclick" data-slot="/148558260/Suara/AMP"
                        data-multi-size="300x250,336x280,1x1"
                        json='{"targeting":{"pos":["mediumrectangle_1"],"page":["Amp"]}}'>
                    </amp-ad>
                </div>

                <!-- Begin Social Icon -->
                <div class="reset h10"></div>
                <div class="social-icon">
                    <amp-social-share type="facebook" width="30" height="30" data-param-app_id="802054763141639"
                        data-param-href="https://indotnesia.suara.com/read/2022/11/17/125634/richard-lee-bebas-dari-status-tersangka-begini-kronologi-perseteruannya-dengan-kartika-putri">
                    </amp-social-share>
                    <amp-social-share type="twitter" width="30" height="30"
                        data-param-text="Richard Lee Bebas dari Status Tersangka, Begini Kronologi Perseteruannya dengan Kartika Putri"
                        data-param-url="https://indotnesia.suara.com/read/2022/11/17/125634/richard-lee-bebas-dari-status-tersangka-begini-kronologi-perseteruannya-dengan-kartika-putri">
                    </amp-social-share>
                    <amp-social-share type="whatsapp" width="30" height="30"
                        data-param-text="Richard Lee Bebas dari Status Tersangka, Begini Kronologi Perseteruannya dengan Kartika Putri - https://indotnesia.suara.com/read/2022/11/17/125634/richard-lee-bebas-dari-status-tersangka-begini-kronologi-perseteruannya-dengan-kartika-putri">
                    </amp-social-share>
                    <amp-social-share type="line" width="30" height="30"
                        data-param-url="https://indotnesia.suara.com/read/2022/11/17/125634/richard-lee-bebas-dari-status-tersangka-begini-kronologi-perseteruannya-dengan-kartika-putri"
                        data-param-text="Richard Lee Bebas dari Status Tersangka, Begini Kronologi Perseteruannya dengan Kartika Putri">
                    </amp-social-share>
                    <amp-social-share type="linkedin" width="30" height="30"
                        data-param-text="Richard Lee Bebas dari Status Tersangka, Begini Kronologi Perseteruannya dengan Kartika Putri"
                        data-param-url="https://indotnesia.suara.com/read/2022/11/17/125634/richard-lee-bebas-dari-status-tersangka-begini-kronologi-perseteruannya-dengan-kartika-putri">
                    </amp-social-share>
                </div>
                <!-- End Social Icon -->

                <div class="box-wrapper">
                    <div class="wrapper-title">
                        <h2>Tag</h2>
                    </div>
                    <ul class="tag">
                        <li> <a href="https://www.suara.com/tag/kartika-putri" title="kartika putri"># kartika putri</a>
                        </li>
                        <li> <a href="https://www.suara.com/tag/richard-lee" title="richard lee"># richard lee</a></li>
                        <li> <a href="https://www.suara.com/tag/kronologi" title="kronologi"># kronologi</a></li>
                        <li> <a href="https://www.suara.com/tag/krim-wajah" title="krim wajah"># krim wajah</a></li>
                    </ul>
                    <div class="reset"></div>
                </div>


                <div class="list-wrapper">
                    <div class="wrapper-title">
                        <h2>Terpopuler</h2>
                    </div>
                    <ul>


                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/10/24/152441/sinopsis-under-the-queens-umbrella-drakor-kerajaan-dengan-balutan-komedi-menyegarkan"
                                title="Sinopsis Under The Queens Umbrella, Drakor Kerajaan dengan Balutan Komedi Menyegarkan">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/10/24/1-under-the-queens-umbrella-swoon.JPG"
                                        alt="Sinopsis Under The Queens Umbrella, Drakor Kerajaan dengan Balutan Komedi Menyegarkan"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Sinopsis Under The Queens Umbrella, Drakor Kerajaan dengan Balutan Komedi
                                    Menyegarkan </div>
                            </a>
                        </li>


                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/15/202033/disinggung-sebagai-penyebab-kematian-keluarga-kalideres-apa-itu-sekte-apokaliptik"
                                title="Disinggung sebagai Penyebab Kematian Keluarga Kalideres, Apa Itu Sekte Apokaliptik?">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/11/15/1-bunuh-diri-freepik.jpg"
                                        alt="Disinggung sebagai Penyebab Kematian Keluarga Kalideres, Apa Itu Sekte Apokaliptik?"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Disinggung sebagai Penyebab Kematian Keluarga Kalideres, Apa Itu Sekte Apokaliptik?
                                </div>
                            </a>
                        </li>


                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/10/18/141211/pemain-ftv-clerence-chyntia-audry-meninggal-dunia-berikut-profil-dan-sinetron-yang-dibintanginya"
                                title="Pemain FTV Clerence Chyntia Audry Meninggal Dunia, Berikut Profil dan Sinetron yang Dibintanginya">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/10/18/1-clerence-chyntia-instagram-at-clerenceca.jpg"
                                        alt="Pemain FTV Clerence Chyntia Audry Meninggal Dunia, Berikut Profil dan Sinetron yang Dibintanginya"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Pemain FTV Clerence Chyntia Audry Meninggal Dunia, Berikut Profil dan Sinetron yang
                                    Dibintanginya </div>
                            </a>
                        </li>


                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/11/194111/potrobayan-river-camp-spot-kemah-seru-di-jogja-dengan-pemandangan-sungai"
                                title="Potrobayan River Camp, Spot Kemah Seru di Jogja Dengan Pemandangan Sungai">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/11/11/1-img-20221111-193813.jpg"
                                        alt="Potrobayan River Camp, Spot Kemah Seru di Jogja Dengan Pemandangan Sungai"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Potrobayan River Camp, Spot Kemah Seru di Jogja Dengan Pemandangan Sungai </div>
                            </a>
                        </li>




                    </ul>
                </div>

                <div class="amp-ads">
                    <amp-ad width=336 height=280 type="doubleclick" data-slot="/148558260/Suara/AMP"
                        data-multi-size="300x250,336x280,1x1"
                        json='{"targeting":{"pos":["mediumrectangle_2"],"page":["Amp"]}}'>
                    </amp-ad>
                </div>

                <div class="list-wrapper">
                    <div class="wrapper-title">
                        <h2>Pilihan</h2>
                    </div>
                    <ul>


                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/13/112333/berapa-lama-manusia-bertahan-tanpa-makanan-dan-apa-efek-kelaparan-bagi-tubuh"
                                title="Berapa Lama Manusia Bertahan Tanpa Makanan dan Apa Efek Kelaparan bagi Tubuh?">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/11/13/1-makanan-makan-pexels-pixabay.jpg"
                                        alt="Berapa Lama Manusia Bertahan Tanpa Makanan dan Apa Efek Kelaparan bagi Tubuh?"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Berapa Lama Manusia Bertahan Tanpa Makanan dan Apa Efek Kelaparan bagi Tubuh? </div>
                            </a>
                        </li>


                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/10/191716/5-buah-ini-bisa-bantu-menghilangkan-lemak-perut-apa-saja"
                                title="5 Buah Ini Bisa Bantu Menghilangkan Lemak Perut, Apa Saja?">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/11/10/1-lemak-perut-perut-buncit-pexels-towfiqu-barbhuiya.jpg"
                                        alt="5 Buah Ini Bisa Bantu Menghilangkan Lemak Perut, Apa Saja?" width="150"
                                        height="84" layout="responsive" data-component-name="amp:image"></amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    5 Buah Ini Bisa Bantu Menghilangkan Lemak Perut, Apa Saja? </div>
                            </a>
                        </li>


                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/10/163700/jennifer-aniston-blak-blakan-soal-usaha-untuk-hamil-dan-perceraiannya"
                                title="Jennifer Aniston Blak-blakan soal Usaha untuk Hamil dan Perceraiannya">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/11/10/1-jeniffer-aniston-instagram-pribadi.jpg"
                                        alt="Jennifer Aniston Blak-blakan soal Usaha untuk Hamil dan Perceraiannya"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Jennifer Aniston Blak-blakan soal Usaha untuk Hamil dan Perceraiannya </div>
                            </a>
                        </li>


                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/05/184122/inilah-minuman-terburuk-yang-bikin-kulit-wajah-cepat-tua"
                                title="Inilah Minuman Terburuk yang Bikin Kulit Wajah Cepat Tua">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/11/05/1-berbagai-jenis-minuman-pexels-cottonbro-studio.jpg"
                                        alt="Inilah Minuman Terburuk yang Bikin Kulit Wajah Cepat Tua" width="150"
                                        height="84" layout="responsive" data-component-name="amp:image"></amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Inilah Minuman Terburuk yang Bikin Kulit Wajah Cepat Tua </div>
                            </a>
                        </li>


                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/05/174554/7-hal-yang-perlu-diketahui-sebelum-nonton-konser-untuk-pertama-kali"
                                title="7 Hal yang Perlu Diketahui Sebelum Nonton Konser untuk Pertama Kali">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/11/05/1-konser-pexels-josh-sorenson.jpg"
                                        alt="7 Hal yang Perlu Diketahui Sebelum Nonton Konser untuk Pertama Kali"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    7 Hal yang Perlu Diketahui Sebelum Nonton Konser untuk Pertama Kali </div>
                            </a>
                        </li>

                    </ul>
                </div>

                <div class="amp-ads">
                    <amp-ad width=336 height=280 type="doubleclick" data-slot="/148558260/Suara/AMP"
                        data-multi-size="300x250,336x280,1x1"
                        json='{"targeting":{"pos":["mediumrectangle_3"],"page":["Amp"]}}'>
                    </amp-ad>
                </div>

                <div class="list-wrapper">
                    <div class="wrapper-title">
                        <h2>Terkini</h2>
                    </div>
                    <ul>

                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/17/125634/richard-lee-bebas-dari-status-tersangka-begini-kronologi-perseteruannya-dengan-kartika-putri"
                                title="Richard Lee Bebas dari Status Tersangka, Begini Kronologi Perseteruannya dengan Kartika Putri">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/11/17/1-screenshot-20221117-125426.png"
                                        alt="Richard Lee Bebas dari Status Tersangka, Begini Kronologi Perseteruannya dengan Kartika Putri"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Richard Lee Bebas dari Status Tersangka, Begini Kronologi Perseteruannya dengan
                                    Kartika Putri </div>
                            </a>
                        </li>
                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/17/113524/dijual-khusus-berikut-distributor-resmi-penjual-materai-elektronik-dan-cara-pembeliannya"
                                title="Dijual Khusus, Berikut Distributor Resmi Penjual Materai Elektronik dan Cara Pembeliannya">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/11/17/1-materai-elektronik-e-materaicoid.png"
                                        alt="Dijual Khusus, Berikut Distributor Resmi Penjual Materai Elektronik dan Cara Pembeliannya"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Dijual Khusus, Berikut Distributor Resmi Penjual Materai Elektronik dan Cara
                                    Pembeliannya </div>
                            </a>
                        </li>
                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/17/101850/profil-basuki-hadimuljono-menteri-pupr-yang-jadi-fotografer-di-acara-g20"
                                title="Profil Basuki Hadimuljono, Menteri PUPR yang Jadi Fotografer di Acara G20">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/11/17/1-menteri-pupr-instagram-basuki-hadimuljono-2.png"
                                        alt="Profil Basuki Hadimuljono, Menteri PUPR yang Jadi Fotografer di Acara G20"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Profil Basuki Hadimuljono, Menteri PUPR yang Jadi Fotografer di Acara G20 </div>
                            </a>
                        </li>
                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/16/180824/tayang-17-november-besok-pevita-pearce-sebut-90-persen-adegan-laga-dilakukan-tanpa-peran-pengganti"
                                title="Tayang 17 November Besok, Pevita Pearce Sebut 90 Persen Adegan Laga Dilakukan Tanpa Peran Pengganti">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/09/07/1-film-sri-asih-imdb.JPG"
                                        alt="Tayang 17 November Besok, Pevita Pearce Sebut 90 Persen Adegan Laga Dilakukan Tanpa Peran Pengganti"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Tayang 17 November Besok, Pevita Pearce Sebut 90 Persen Adegan Laga Dilakukan Tanpa
                                    Peran Pengganti </div>
                            </a>
                        </li>
                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/16/043135/begini-komentar-warganet-soal-jokowi-ajak-pimpinan-g20-menanam-mangrove-di-bali"
                                title="Begini Komentar Warganet Soal Jokowi Ajak Pimpinan G20 Menanam Mangrove di Bali">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/11/16/1-antara-media-center-g20-indonesia-galih-pradipta.jpg"
                                        alt="Begini Komentar Warganet Soal Jokowi Ajak Pimpinan G20 Menanam Mangrove di Bali"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Begini Komentar Warganet Soal Jokowi Ajak Pimpinan G20 Menanam Mangrove di Bali
                                </div>
                            </a>
                        </li>
                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/16/152910/bts-masuk-2-nominasi-di-grammy-awards-2023-dan-beyonce-mendominasi"
                                title="BTS Masuk 2 Nominasi di Grammy Awards 2023 dan Beyonce Mendominasi">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/10/17/1-boy-band-bts-instagram-btsofficial.png"
                                        alt="BTS Masuk 2 Nominasi di Grammy Awards 2023 dan Beyonce Mendominasi"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    BTS Masuk 2 Nominasi di Grammy Awards 2023 dan Beyonce Mendominasi </div>
                            </a>
                        </li>
                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/16/133925/mengenal-apa-itu-lo-g20-yang-lagi-trending-di-twitter"
                                title="Mengenal Apa itu LO G20 yang Lagi Trending di Twitter">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/11/16/1-ktt-g20-hari-kedua-youtube-sekretariat-presiden.jpg"
                                        alt="Mengenal Apa itu LO G20 yang Lagi Trending di Twitter" width="150"
                                        height="84" layout="responsive" data-component-name="amp:image"></amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Mengenal Apa itu LO G20 yang Lagi Trending di Twitter </div>
                            </a>
                        </li>
                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/16/122500/viral-tukang-fotocopy-keliling-di-sinetron-ternyata-ada-di-depok"
                                title="Viral Tukang Fotocopy Keliling di Sinetron, Ternyata Ada di Depok">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/11/16/1-fotocopy-keliling-twitter-kegblgnunfaedh.jpg"
                                        alt="Viral Tukang Fotocopy Keliling di Sinetron, Ternyata Ada di Depok"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Viral Tukang Fotocopy Keliling di Sinetron, Ternyata Ada di Depok </div>
                            </a>
                        </li>
                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/16/111857/bobby-ikon-dikabarkan-tola-pembaharuan-kontrak-yg-entertainment-masih-ada-waktu"
                                title="Bobby iKON Dikabarkan Tola Pembaharuan Kontrak, YG Entertainment: Masih Ada Waktu">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/11/16/1-bobby-ikon-yg-entertainment-inc.jpg"
                                        alt="Bobby iKON Dikabarkan Tola Pembaharuan Kontrak, YG Entertainment: Masih Ada Waktu"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Bobby iKON Dikabarkan Tola Pembaharuan Kontrak, YG Entertainment: Masih Ada Waktu
                                </div>
                            </a>
                        </li>
                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/16/095512/sejarah-dan-makna-hari-angklung-sedunia-yang-diperingati-setiap-16-november"
                                title="Sejarah dan Makna Hari Angklung Sedunia yang Diperingati Setiap 16 November">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/11/16/1-angklung-google-doodle.JPG"
                                        alt="Sejarah dan Makna Hari Angklung Sedunia yang Diperingati Setiap 16 November"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Sejarah dan Makna Hari Angklung Sedunia yang Diperingati Setiap 16 November </div>
                            </a>
                        </li>
                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/15/202033/disinggung-sebagai-penyebab-kematian-keluarga-kalideres-apa-itu-sekte-apokaliptik"
                                title="Disinggung sebagai Penyebab Kematian Keluarga Kalideres, Apa Itu Sekte Apokaliptik?">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/11/15/1-bunuh-diri-freepik.jpg"
                                        alt="Disinggung sebagai Penyebab Kematian Keluarga Kalideres, Apa Itu Sekte Apokaliptik?"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Disinggung sebagai Penyebab Kematian Keluarga Kalideres, Apa Itu Sekte Apokaliptik?
                                </div>
                            </a>
                        </li>
                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/15/153500/buntut-pernyataan-kontroversial-cristiano-ronaldo-terancam-denda-rp18-miliar"
                                title="Buntut Pernyataan Kontroversial, Cristiano Ronaldo Terancam Denda Rp18 Miliar">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/11/15/1-cristiano-ronaldo-isntagram-cristiano.jpg"
                                        alt="Buntut Pernyataan Kontroversial, Cristiano Ronaldo Terancam Denda Rp18 Miliar"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Buntut Pernyataan Kontroversial, Cristiano Ronaldo Terancam Denda Rp18 Miliar </div>
                            </a>
                        </li>
                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/15/133256/festival-film-dokumenter-2022-resmi-dibuka-berikut-jadwal-lengkap-dan-lokasinya"
                                title="Festival Film Dokumenter 2022 Resmi Dibuka, Berikut Jadwal Lengkap dan Lokasinya">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/11/15/1-ffd-2022-instagram-ffdjogja.jpg"
                                        alt="Festival Film Dokumenter 2022 Resmi Dibuka, Berikut Jadwal Lengkap dan Lokasinya"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Festival Film Dokumenter 2022 Resmi Dibuka, Berikut Jadwal Lengkap dan Lokasinya
                                </div>
                            </a>
                        </li>
                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/15/122500/ngayogjazz-kembali-digelar-mengusung-konsep-pesta-rakyat-meriah"
                                title="Ngayogjazz Kembali Digelar, Mengusung Konsep Pesta Rakyat Meriah">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/11/15/1-ngayogjazz-instagram-ngayogjazz.jpg"
                                        alt="Ngayogjazz Kembali Digelar, Mengusung Konsep Pesta Rakyat Meriah"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Ngayogjazz Kembali Digelar, Mengusung Konsep Pesta Rakyat Meriah </div>
                            </a>
                        </li>
                        <li class="suara-populer-feed">
                            <a href="https://indotnesia.suara.com/read/2022/11/15/111600/gudfest-2022-batal-digelar-honne-ungkap-maaf-ke-penggemar-di-indonesia"
                                title="Gudfest 2022 Batal Digelar, HONNE Ungkap Maaf Ke Penggemar di Indonesia">
                                <div class="suara-populer-thumb">
                                    <amp-img
                                        src="https://media.suara.com/suara-partners/indotnesia/thumbs/336x189/2022/11/15/1-honne-instagram-gudfestival-2.png"
                                        alt="Gudfest 2022 Batal Digelar, HONNE Ungkap Maaf Ke Penggemar di Indonesia"
                                        width="150" height="84" layout="responsive" data-component-name="amp:image">
                                    </amp-img>
                                </div>
                                <div class="suara-populer-title">
                                    Gudfest 2022 Batal Digelar, HONNE Ungkap Maaf Ke Penggemar di Indonesia </div>
                            </a>
                        </li>
                    </ul>
                </div>

                <a href="https://indotnesia.suara.com/indeks" class="btn">
                    Load More
                </a>

                <div class="reset"></div>
            </section>


        </article>
    </main>

    <footer class="footer">
        <div class="site-footer">
            <div class="inner">
                <section class="copyright">&copy; 2022 <a href="mailto:redaksi@suara.com"
                        title="Email Redaksi Suara.com">suara.com</a> - All Rights Reserved.</section>
                <div class="reset h10"></div>
                <a class="back-to-top" href="#" rel="">Back to Top</a>
            </div>
        </div>
    </footer>
</body>

</html>